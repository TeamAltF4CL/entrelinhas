<!DOCTYPE html>
<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="assets/img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>ENTRElinhas</title>
		<link rel="icon" href="assets/img/logo_escola.png" type="image/png">
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="assets/css/linearicons.css">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/bootstrap.css">
		<link rel="stylesheet" href="assets/css/magnific-popup.css">
		<link rel="stylesheet" href="assets/css/nice-select.css">
		<link rel="stylesheet" href="assets/css/animate.min.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.css">
		<link rel="stylesheet" href="assets/css/jquery-ui.css">
		<link rel="stylesheet" href="assets/css/main.css">

	</head>
	<body>

			
    <?php

        require 'includes/navbar.php';

    ?>  

    
		<div class="site-main-container">
            <!-- Start top-post Area -->

                <?php
                        /* Noticias importantes (vars: id_primeira_pub, linha, $linha_final, 
												$linha2, $linha_final2, $linha3, %linha_final3) */
                        include('includes/noticias/noticias_importantes.php');
                ?>

			<!-- Start latest-post Area -->
			<section class="latest-post-area pb-120">
				<div class="container no-padding">
					<div class="row">
						<div class="col-lg-8 post-list">

						<?php
                        	/* Noticias recentes (vars: $linha4) */
                        	include('includes/noticias/noticias_recentes.php');
               			 ?>
							
							<!-- Start banner-ads Area -->
							<div class="col-lg-12">
							<br>
							</div>
							<!-- End banner-ads Area -->

							<?php
								/* Noticias populares (vars: id_primeira_pub2, linha5, $linha_final5, 
														$linha6, $linha_final6, $linha7, %linha_final7) */
								include('includes/noticias/noticias_populares.php');
               			 	?>

							<?php
								/* Noticias populares (vars: ) */
								include('includes/noticias/noticias_relevantes.php');
               			 	?>

						</div>

						<?php
								/*Noticias laterais (Escolha do editor, noticias aleatorias, mais populares etc)
													(vars: $linha8) */
								include('includes/noticias/noticias_laterais_index.php');
               			 	?>


			
					</div>
				</div>
			</section>
			<!-- End latest-post Area -->
		</div>
		
		<?php
				include('includes/footer.php');
				include('includes/botao_topo.php');
		?>	

		<script src="assets/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="assets/js/vendor/bootstrap.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="assets/js/easing.min.js"></script>
		<script src="assets/js/hoverIntent.js"></script>
		<script src="assets/js/superfish.min.js"></script>
		<script src="assets/js/jquery.ajaxchimp.min.js"></script>
		<script src="assets/js/jquery.magnific-popup.min.js"></script>
		<script src="assets/js/mn-accordion.js"></script>
		<script src="assets/js/jquery-ui.js"></script>
		<script src="assets/js/jquery.nice-select.min.js"></script>
		<script src="assets/js/owl.carousel.min.js"></script>
		<script src="assets/js/mail-script.js"></script>
		<script src="assets/js/main.js"></script>
	</body>
</html>