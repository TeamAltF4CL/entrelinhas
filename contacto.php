<!DOCTYPE html>
<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="assets/img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>ENTRElinhas | Contactos</title>
		<link rel="icon" href="assets/img/logo_escola.png" type="image/png">	
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="assets/css/linearicons.css">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/bootstrap.css">
		<link rel="stylesheet" href="assets/css/magnific-popup.css">
		<link rel="stylesheet" href="assets/css/nice-select.css">
		<link rel="stylesheet" href="assets/css/animate.min.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.css">
		<link rel="stylesheet" href="assets/css/jquery-ui.css">
		<link rel="stylesheet" href="assets/css/main.css">
	</head>
	<body>
	<?php 
	require 'includes/navbar.php'; 
	?>
		<div class="site-main-container">
			<!-- Start top-post Area -->
			<section class="top-post-area pt-10">
				<div class="container no-padding">
					<div class="row">
						<div class="col-lg-12">
							<div class="hero-nav-area" style="background-image: url('https://inbest.solutions/wp-content/uploads/2018/09/banner-correo.jpg') !important;">
								<h1 class="text-white">Contactos</h1>
								<p class="text-white link-nav"><a href="index.php">Início </a>  <span class="lnr lnr-arrow-right"></span><a href="contacto.php">Contactos </a></p>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End top-post Area -->
			<!-- Start contact-page Area -->
			<section class="contact-page-area pt-50 pb-120">
				<div class="container">
					<div class="row contact-wrap">
						<div class="map-wrap" style="width:100%; height: 445px;"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3012.078784365917!2d-8.570397284011705!3d40.97975565038355!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9e5e187b4ece85bd!2sCol%C3%A9gio+de+Lamas!5e0!3m2!1spt-PT!2spt!4v1555761168613!5m2!1spt-PT!2spt" width="100%" height="445px" frameborder="0" style="border:0" allowfullscreen></iframe></div>
						<div class="col-lg-3 d-flex flex-column address-wrap">
							<div class="single-contact-address d-flex flex-row">
								<div class="icon">
									<span class="lnr lnr-home"></span>
								</div>
								<div class="contact-details">
									<h5>Rua da Salgueirinha, n.º 325</h5>
									<p>
									4535-368 St.ª .M.ª de Lamas
									</p>
								</div>
							</div>
							<div class="single-contact-address d-flex flex-row">
								<div class="icon">
									<span class="lnr lnr-phone-handset"></span>
								</div>
								<div class="contact-details">
									<h5>227470210</h5>
									<p>Seg a Sex das 9h ás 17h</p>
								</div>
							</div>
							<div class="single-contact-address d-flex flex-row">
								<div class="icon">
									<span class="lnr lnr-envelope"></span>
								</div>
								<div class="contact-details">
									<h5>Email do ENTRElinhas</h5>
									<p>geral@colegiodelamas.com</p>
								</div>
							</div>
						</div>
						<div class="col-lg-9">
							<form class="form-area contact-form text-right" action="includes/processos/proc_contacto.php" method="POST">
								<div class="row">
									<div class="col-lg-6">
										<input name="nome" placeholder="Insira o seu nome" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" class="common-input mb-20 form-control" required="" type="text">
										
										<input name="email" placeholder="Insira o seu email" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Insira o seu email'" class="common-input mb-20 form-control" required="" type="email">
										<input name="assunto" placeholder="Insira o assunto" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Insira um assunto'" class="common-input mb-20 form-control" required="" type="text">
									</div>
									<div class="col-lg-6">
										<textarea class="common-textarea form-control" name="mensagem" placeholder="Insira a sua mensagem" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Insira a sua mensagem'" required=""></textarea>
									</div>
									<div class="col-lg-12">
										<div class="alert-msg" style="text-align: left;"></div>
										<button type="submit" class="primary-btn primary" name="contacto" value="contacto" style="float: right;">Enviar Mensagem</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</section>
			<!-- End contact-page Area -->
		</div>
				<!-- start footer Area -->
<?php 
require 'includes/footer.php'; 

include('includes/botao_topo.php');
?>
		<!-- End footer Area -->
		<script src="assets/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="assets/js/vendor/bootstrap.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="assets/js/easing.min.js"></script>
		<script src="assets/js/hoverIntent.js"></script>
		<script src="assets/js/superfish.min.js"></script>
		<script src="assets/js/jquery.ajaxchimp.min.js"></script>
		<script src="assets/js/jquery.magnific-popup.min.js"></script>
		<script src="assets/js/mn-accordion.js"></script>
		<script src="assets/js/jquery-ui.js"></script>
		<script src="assets/js/jquery.nice-select.min.js"></script>
		<script src="assets/js/owl.carousel.min.js"></script>
		<script src="assets/js/mail-script.js"></script>
		<script src="assets/js/main.js"></script>
	</body>
</html>