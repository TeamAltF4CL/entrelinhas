<!DOCTYPE html>
<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="assets/img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>ENTRElinhas | Notícias</title>
		<link rel="icon" href="assets/img/logo_escola.png" type="image/png">
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="assets/css/linearicons.css">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/bootstrap.css">
		<link rel="stylesheet" href="assets/css/magnific-popup.css">
		<link rel="stylesheet" href="assets/css/nice-select.css">
		<link rel="stylesheet" href="assets/css/animate.min.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.css">
		<link rel="stylesheet" href="assets/css/jquery-ui.css">
		<link rel="stylesheet" href="assets/css/main.css">

		<!--Carregar mais noticias from BD com o botao-->

		<style>
		    html, body {
    max-width: 100%;
    overflow-x: hidden;
}
		</style>
	</head>
	<body>
	
    <?php

        require 'includes/navbar.php';

    ?>  

		<div class="site-main-container">
			<!-- Start top-post Area -->
			<section class="top-post-area pt-10">
				<div class="container no-padding">
					<div class="row">
						<div class="col-lg-12">
							<div class="hero-nav-area" style="background-image: url('https://previews.123rf.com/images/sergiobarrios/sergiobarrios1504/sergiobarrios150400072/38517914-diarios-del-mundo-detalle-de-peri%C3%B3dicos-con-noticias-informaci%C3%B3n-y-lectura.jpg') !important;">
								<h1 class="text-white">Noticias</h1>
								<p class="text-white link-nav"><a href="index.php">Inicio </a>  <span class="lnr lnr-arrow-right"></span><a href="noticias.php">Todas as noticias</a></p>
							</div>
						</div>
							<?php include('includes/noticias/noticia_ultima_hora.php') ; ?>
					</div>
				</div>
			</section>
            <!-- End top-post Area -->
            
			<!-- Start latest-post Area -->
			<section class="latest-post-area pb-120">
				<div class="container no-padding">
					<div class="row">
						<div class="col-lg-8 post-list" >
                            
                        <?php
                        /* Todas as noticias (vars: $linha) */
                        include('includes/noticias/noticias_todas.php');
                        ?>

                        </div>
                        
                        <?php
                                /* Noticias laterais (Escolha do editor, noticias aleatorias, mais populares etc)
                                    (vars: $linha7) */
								include('includes/noticias/noticias_laterais.php');
               			 	?>
					</div>
				</div>
			</section>
			<!-- End latest-post Area -->
		</div>
		
        <?php
				include('includes/footer.php');
				include('includes/botao_topo.php');
        ?>	
        
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">

		<script src="assets/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="assets/js/vendor/bootstrap.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="assets/js/easing.min.js"></script>
		<script src="assets/js/hoverIntent.js"></script>
		<script src="assets/js/superfish.min.js"></script>
		<script src="assets/js/jquery.ajaxchimp.min.js"></script>
		<script src="assets/js/jquery.magnific-popup.min.js"></script>
		<script src="assets/js/mn-accordion.js"></script>
		<script src="assets/js/jquery-ui.js"></script>
		<script src="assets/js/jquery.nice-select.min.js"></script>
		<script src="assets/js/owl.carousel.min.js"></script>
		<script src="assets/js/mail-script.js"></script>
		<script src="assets/js/main.js"></script>

	</body>
</html>