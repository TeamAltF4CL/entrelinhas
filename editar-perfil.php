<?php

include_once('includes/processos/proc_login.php');

if (!isset($_SESSION["user_id"])) //se não existe login 
{
header('Location: index.php');
	exit;
}
else //se existe login
{


	include_once('includes/processos/conexao.php'); 


$sql="Select * From utilizadores where id='".$_SESSION["user_id"]."'";
$result = mysqli_query($conn, $sql);

$linha = mysqli_fetch_array($result, MYSQLI_ASSOC);

?>
<style>
      /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
      @import url(https://fonts.googleapis.com/css?family=Lato:100,300,400,700);
@import url(https://fonts.googleapis.com/css?family=Montez);

body{
  background-color: #f5f5f5;  
  font-family: Lato;
}
.profile,.content{
  -webkit-transition: 0.5s ease;
  -moz-transition: 0.5s ease;
    transition: 0.5s ease;
}
.profile{
  /* position: absolute; */
  top: 100px;
  bottom: auto;
  left: 0;
  right: 0;
  min-height: 100%;
  height: auto;
  width: 75%;
  margin: 20px auto;
  /* margin-bottom: 100px; */
  background-color: #fff;
  border-top: 5px solid #1c24ab;
  border-radius: 0 0 5px 5px;
  box-shadow: 0 2.5px 5px #ccc;
}
.content{
  /* position: absolute; */
  min-height: 100%;
  /* height: 100%; */
  width: 95%;
  margin: 2.5% auto;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  /*border: 1px solid #eee;*/
  /*background-color: yellow;*/
  overflow: hidden;
}
.short-info{
  position: relative;
  min-height: 100px;
  height: auto;
  width: 97.5%;
  margin: 1.5% auto;
  /*background-color: #f5f5f5;*/
  background-color: #fff;
  background-color: #f2ecee;
  /*border-top: 2px solid #1c24ab;*/
  border-radius: 2.5px;
  /*box-shadow: inset 0 -1.5px #ddd;*/
}
.details{
  width: 97.5%;
  /*background-color: red;*/
  margin: 2.5px auto;
}
.tab-heads{
  color: #777;
  margin: 0 2.5px;
}
.tabs{
  height: 200px;
  width: 97.5%;
  margin: 0 auto;
  /* border-top: 2.5px solid #1c24ab; */
  background-color: #f2ecee;
  border-radius: 2.5px;
  /* background-color: #f3f3f3; */
}
span.fa-envelope{
  position: absolute;
  top: 22%;
  left: 56%;
  color: #1c24ab;
}
span.photo{
  position: relative;
  height: 80px;
  width: 80px;
  border-radius: 5px;
  margin: 10px 0 2.5px;
  display: block;
  top: 10%;
  left: 10%;
  overflow: hidden;
  background: #ddd url("https://www.adtechnology.co.uk/images/UGM-default-user.png");
  background-size: 100%;
  border-radius: 100%;
  border: 2px solid #ddd;
}
span.photo:after{
  content: " Add Profile Pic ";
  text-align: center;
  position: absolute;
  left: 0;
  z-index: 2;
  padding: 35% 0 65%;
  height: 0;
  width: 100%;
  opacity: 0;
  color: #222;
  background-color: rgba(0,0,0,0.25);
  background-size: 100%;
  transition: 0.35s ease-in-out;
  overflow: hidden;
  border-radius: 100%;
}
span.photo:hover:after{
  bottom: 0;
  opacity: 1;
  color: #fff;
}
input[type="file"]{
  /* position: absolute; */
  color: #555;
  font-size: 15px;
  top: 25%;
  box-shadow: none !important;
  margin-left:  !important;
  background-color: #ededed;
  width: 200px;
  border: 0;
}

span.name,span.links > h3,h4{
  font-family: Lato;
  /*font-weight: 200;*/
}
span.name{
  position: absolute;
  top: 20%;
  left: 25%;
  color: #555;
  font-size: 18px;
}
label{
  color: #555;
  line-height: 2.1em;
  margin-left: 10px;
}
label[for="avatar"]{
  line-height: 120px;
}
.btn{
  /* position: absolute; */
  top: 45%;
  left: 25%;
  /* color: #fff; */
  /* background-color: ligth#1c24ab; */
  background-color: #ddd;
  padding: 10px 15px;
  border-radius: 3px;
  cursor: pointer;
  transition: 0.5s ease;
}
span.btn:hover{
  opacity: 1;
  color: #fff;
  background-color: rgba(57,203,88,1);
  /* background-color: #1c24ab; */
}
.profile:hover .btn{
    opacity: 1;
}
.short-info span h3,h4{
  display: inline-block;
  margin: 0;
}

div.circles{
  width: 100%;
  margin: 0 auto;
}
span.fa-users{
  color: #777;
  margin-left: 1px;
  margin: 7px 7px 2.5px;
}
span.fa-users:after{
  content: "Circles";
  font-family: Lato;
  margin-left: 3px;
  color: #777;
}
.myCircle{
  height: 200px;
  width: 97.5%;
  margin: 0 auto;
  background-color: #f2ecee;
  border-radius: 2.5px;
}

/*Circles Styles
=========================*/
.myCircle #tabs ul li:nth-child(1){
  margin: 0;
  padding: 0px 5px 10px;
  /*margin-top: 5px;*/
  background-color: white;
  border-radius: 0 0 0 30px;
}
.myCircle #tabs ul li:nth-child(2){
  margin-left: -3px;
  padding: 0 5px 10px;
  border-radius: 0 0 30px 0;
  background-color: white;
}
.myCircle #tabs .active{
  border-bottom: none;
  padding-left: 10px;
}
.myCircle #tabs{
  padding: 0;
  margin: 0;
}
.fa-twitter,.fa-facebook,.fa-github{
  height: 20px;
  width: 20px;
  text-align: center;
  line-height: 20px;
  padding: 2.5px;
  margin-left: -15px;
}

/*Input Fields Styles
=========================*/
fieldset textarea,input{
  font-family: Open Sans;
  font-size: 15px;
  color: #333;
  background-color: #f7f7f7;
  box-shadow: 0 0 0 1px #1c24ab;
  padding: 5px;
  width: 75%;
  margin: 5px auto;
  border: 0;
  border-radius: 2.5px;
  outline: 0;
  transition: 0.3s ease;
}
fieldset textarea{
  min-height: 40px;
  max-height: 60px;
}
fieldset textarea:hover,input:hover{
  
  background-color: #fff;
  color: #1c24ab !important;
}
fieldset textarea:hover,label:hover{

  box-shadow: none;
  background-color: #fff;
  color: #1c24ab !important;
  
}
fieldset textarea:focus,input:focus{
  box-shadow: 0 0 0 2px #1c24ab,
              inset 0 2px 5px -1px rgba(0,0,0,0.25);
}

.grid-35{
  width: 35%;
  float: left;
  font-weight: 500;
  color: #333;
  /* text-align: left; */
}
.grid-50{
  width: 50%;
  float: left;
  font-weight: 500;
  color: #333;
  /* text-align: left; */
  margin-top: -20px;
}
.grid-65{
  position: relative;
  width: 65%;
  float: right;
  font-family: Source Sans Pro;
  font-weight: 300;
  font-size: 17px;
}
#tabs-1 div,#tabs-2 div,#tabs-3 div{
  border-bottom: 1px solid #ddd;
}

/*Tabs Styles
=========================*/
#tabs {
  width: 97.5%;
  margin: 0 auto;
  position: relative;
  -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
    transition: all 0.5s ease;
}
#tabs-1,#tabs-2,#tabs-3{
  width: 95%;
  margin: 0 auto;
  /*margin-top: 5px;*/
  padding: 5px 10px;
  line-height: 1.3em;
  padding-bottom: 10px;
  font-family: Open Sans;
  background-color: #f2ecee;
  border-radius: 0 2.5px 2.5px 2.5px;
  -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
  transition: all 0.5s ease;
} 
#tabs ul{
  margin: 0 auto;
  padding: 0;
}
#tabs ul li{
  display: inline-block;
  margin: 0;
  padding: 0 7px;
  width: 65px;
  text-align: center;
  padding-bottom: 5px;
  -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
     transition: all 0.5s ease;  
}
#tabs ul li a{
  outline: 0;
  text-decoration: none;
  padding: 0 3px 0 0;
  /* background-color: #222; */
  font-family: Open Sans;
  margin: 0;
  -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
     transition: all 0.5s ease;
}

.ui-state-hover{
  border-bottom: 2.5px solid #aaa;
}
.ui-state-active{
  color: #38cc85;
  border-bottom: 2.5px solid #1c24ab;
}

/* #CLEARFIX HACK
=========================*/
.clear:after{
  content: " ";
  display: table;
  clear: both;
}

/* Edit View
======================*/
.content h1{
  text-align: center;
  color: #555;
  font-family: Lato;
  font-size: 40px;
  font-weight: 200;
  margin: 5px auto 15px auto;
}
select{
  width: 80%;
  padding: 7px 10px;
  background-color: #f5f5f5;
  border: 1px solid #1c24ab;
  border-radius: 2.5px;
  outline: 0;
}
select option{
  padding: 5px;
}
fieldset{
    text-align: center;
    /* background-color: rgba(0,0,0,0.01); */
    margin-bottom: 5px !important;
    padding: 5px !important;
    box-sizing: border-box;
    border-bottom: 1px solid rgba(0,0,0,0.07) !important;
}
fieldset:last-child{
  border-bottom: none;
}
input.Btn{
  width: 48%;
  float: left;
  display: block;
  margin: 40px auto;
  margin-left: 1%;
  padding: 15px 0;
  font-size: 16px;
  color: #fff;
  cursor: pointer;
  transition: 0.5s ease;
  border: 1px solid #1c24ab;
  background-color: #1c24ab;
}
label.Btn{
  width: 48%;
  float: left;
  display: block;
  margin: 40px auto;
  margin-left: 1%;
  padding: 15px 0;
  font-size: 16px;
  color: #fff;
  cursor: pointer;
  transition: 0.5s ease;
  border: 1px solid #1c24ab;
  background-color: #1c24ab;
}
label.btn{
  width: 48%;
  float: left;
  display: block;
  margin: 40px auto;
  margin-left: 1%;
  padding: 15px 0;
  font-size: 16px;
  color: #fff;
  cursor: pointer;
  transition: 0.5s ease;
  border: 1px solid #1c24ab;
  background-color: #1c24ab;
}
input.Btn:hover{
  background-color: white;
  color: #1c24ab;
  border: 1px solid #1c24ab;
}
label.Btn:hover{
  background-color: white;
  color: #1c24ab;
  border: 2px solid #1c24ab;
}
/* Header Bar 
===========================*/
.navbar{
  padding: 10px;
  box-shadow: 0 2.5px 10px rgba(0,0,0,0.5);
}
div.logo{
/*    margin: 0 auto;*/
/*    text-align: center;*/
    font-size: 30px;
    font-family:"Source Sans Pro";
    color:#1c24ab;
    width: 100%;
/*    display: inline-block;*/
    /*    position: absolute;*/
/*    top:0px;*/
/*    left:0;*/
    right: 0;
    z-index: 2;
}
span.logo-part{
    color:#fff;
    background-color: #1c24ab;
    padding: 2px 5.5px 0px 10px;
    border-radius:100%;
    margin-bottom:15px;
    font-family: "Montez";
}
.search{
/*    margin-top:5px;
/*    border:0;*/
    float: left;
    position: relative;
    top:4px;
    right: 100px;
    padding-bottom: 0;
}
.search-bar{
/*    border:0;*/
    width:220px;
    color:white;
    background-color:#eee;
    font-family: "Lato","Open Sans",Helvetica,Arial;
    border-top: 2px solid black;
    padding: 5px;
    padding-left: 15px;
    display: inline;
    border-radius: 50px;
    -webkit-transition: 0.4s ease;
                transition: 0.4s ease;
}
.search-icon{
    color:gray;
    margin-left:-27.5px;
}
.search-bar:focus{
    width:255px;
    outline:none;
}
.navbar-nav li a{
  padding-left: 8.5px;
  padding-right: 8.5px;
  font-size: 12px;
  transition: 0.5s ease;
}
.navbar-nav li a > span.fa:before{
  margin-right: 5px;
  /* background-color: #ddd; */
}



/*Media Queries
=========================*/
@media only screen and (min-width: 768px){ /*Desktop*/
  .profile{ 
    width: 100%;
  }
  .search{
    float: none;
    /* top: 0; */
    left: 0;
    right: 0;
    margin: 0 auto;
  }
 
}
@media only screen and (max-width: 768px){ /*Tablet*/
  .profile{ 
    width: 100%;
  }
  .search{
    top: 0;
    left: 0;
    right: 0;
    display: block;
    margin: 0 auto;
  } 
  .margem1{
    margin-left:20px !important;
  }
  .margem2{
    margin-left:20px !important;
  }
}

@media only screen and (max-width: 400px){ /*Phone*/
  .margem1{
    margin-left:0px !important;
    font-size:12px !important;
  }
  .margem2{
    margin-left:5px !important;
    font-size:12px !important;
  }
}

@media only screen and (min-width: 320px) and (max-width: 520px){ /*Phone*/
  .profile{
    width: 90%;
  }
}












.emp-profile{
    /* padding: 3%; */
    margin-top: 3%;
    margin-bottom: 3%;
    border-radius: 0.5rem;
    background: #fff;
}
.profile-img{
    text-align: center;
}
.profile-img img{
    width: 225px;
    height: 225px;
}
.profile-img .file {
    position: relative;
    overflow: hidden;
    width: 70%;
    border: none;
    border-radius: 0;
    font-size: 15px;
    background:  #1c24ab;
}
.profile-img .file input {
  position: absolute;
  opacity: 0;
  right: 0;
  top: 0;
  width: 100%;
  height: 100%;
  cursor: pointer;
}
.profile-head h5{
    color: #333;
}

.profile-edit-btn{
    border: none;
    border-radius: 1.5rem;
    width: 70%;
    padding: 2%;
    font-weight: 600;
    color: #6c757d;
    cursor: pointer;
}


.profile-head .nav-tabs{
    margin-bottom:5%;
}
.profile-head .nav-tabs .nav-link{
    font-weight:600;
    border: none;
}
.profile-head .nav-tabs .nav-link.active{
    border: none;
    border-bottom:2px solid  #1c24ab;
}
.profile-work{
    padding: 14%;
    margin-top: -15%;
}
.profile-work p{
    font-size: 12px;
    color: #ff6666;
    font-weight: 600;
    margin-top: 10%;
}
.profile-work a{
    text-decoration: none;
    color: #ff6666;
    font-weight: 600;
    font-size: 14px;
}
.profile-work ul{
    list-style: none;
}
.profile-tab label{
    font-weight: 600;
}
.profile-tab p{
    font-weight: 600;
    color: black;
}
.eliminar{
    margin: 0px !important;
    float: none !important;
    display: inline-flex !important;
    width: 100px !important;
    font-size:12px !important;
    height: 30px !important;
    padding-bottom: 5px !important;
    padding-top: 5px !important;
}

@media only screen and (min-width: 991.5px) and (max-width: 1199.5px){ /*Phone*/
  .margem3{
    width: 85% !important;
  }
}

@media only screen and (min-width: 767.5px) and (max-width: 991px){ /*Phone*/
  .margem3{
    width: 100% !important;
  }
}

@media only screen and (min-width: 575.5px) and (max-width: 767px){ /*Phone*/
  .margem3{
    width: 50% !important;
  }
}

@media only screen and (min-width: 200.5px) and (max-width: 575px){ /*Phone*/
  .margem3{
    width: 53% !important;
    top:0;
  }
}
@media only screen and (min-width: 490.5px) and (max-width: 520px){ /*Phone*/
  .margem3{
    width: 58% !important;
    top:0;
  }
}
@media only screen and (min-width: 200px) and (max-width: 490px){ /*Phone*/
  .margem3{
    width: 100% !important;
    top:0;
  }
}

i{
  margin-top: 10px;
}
    </style>

<!DOCTYPE html>
<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="assets/img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>ENTRElinhas | Perfil </title>
		<link rel="icon" href="assets/img/logo_escola.png" type="image/png">
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="assets/css/linearicons.css">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/bootstrap.css">
		<link rel="stylesheet" href="assets/css/magnific-popup.css">
		<link rel="stylesheet" href="assets/css/nice-select.css">
		<link rel="stylesheet" href="assets/css/animate.min.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.css">
		<link rel="stylesheet" href="assets/css/jquery-ui.css">
		<link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	
		<script>
        function preview(input) {
        if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
            $('#preview_image')
        .attr('src', e.target.result);
          };
          reader.readAsDataURL(input.files[0]);
        }
        }
</script>
	
	</head>
	<body>


	<?php

require 'includes/navbar.php';

?> 

		<div class="site-main-container">
			<!-- Start top-post Area -->
			<section class="top-post-area pt-10">
				<div class="container no-padding">
					<div class="row">
						<div class="col-lg-12">
							<div class="hero-nav-area" style="background-image: url('http://demo.geekslabs.com/materialize/v2.3/layout03/images/user-profile-bg.jpg') !important;">
								<h1 class="text-white">Editar Perfil</h1>
								<p class="text-white link-nav"><a href="index.html">Home </a>  <span class="lnr lnr-arrow-right"></span><a href="editar-perfil.php">Editar Perfil </a></p>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End top-post Area -->
			<!-- Start contact-page Area -->
			<section class="contact-page-area pt-50 pb-120">
				<div class="container">
					<div style="padding:0px;" class=" contact-wrap">

					<form method="POST" action="includes/processos/proc_editar.php" enctype="multipart/form-data">

	
					<input type="hidden" value="<?php echo $linha['id']; ?>" name="id" id="id">
                  <input type="hidden" value="<?php echo $linha['nome_imagem']; ?>" name="nome_imagem1" id="nome_imagem1">
                  <input type="hidden" value="<?php echo $linha['password']; ?>" name="password2" id="password2">
		
   
    <div class="wrapper">
  <div class="profile">
    <div class="content">
<!--     
      <h1 style="font-weight: 500;color: #1c24ab;margin-bottom: 20px;">Editar Perfil</h1> -->
      
        <!-- Photo -->
        <!-- <fieldset style="border-bottom:none !important;">
          <div class="grid-50">
            <label for="avatar">Sua Foto</label>
          </div>
          <div class="grid-50">
         
                  
          </div>
        </fieldset>
        <fieldset>
          <div class="grid-50">
          <input type="submit" class="btn btn-danger margem1" name="eliminar" id="eliminar" value="Eliminar Foto" style="margin-top: 0px;margin-left: 300px;padding: 0.7rem 10px;width: 118px; margin-bottom:20px;font-size: 15px;">
          </div>
          <div class="grid-50" style="float:right;">

          <input style="display:none" type="file" id="nome_imagem" name="nome_imagem" onchange="preview(this);" />
            <label for="nome_imagem" name="nome_imagem" class="btn btn-danger margem2" style="width:118px;margin-top: 0px; padding: 0.7rem 10px; margin-bottom:5px; margin-left:40px; font-size: 15px;"/>Selecionar</label>
          </div>
          
        </fieldset> -->

        <div class="container emp-profile">
            <!-- <form method="POST" action="proc_editar.php" enctype="multipart/form-data"> -->
                <div class="row d-flex justify-content-start" >
                    <div class="col-md-4">
                                      <div class="profile-img">
                                           
                                                  <img src="assets/fotos_user/<?php echo $linha['nome_imagem'];?>" id="preview_image"/>
                                                  
                                                        <div class="file btn btn-lg btn-primary margem3" style="left: 0;" >                                                          
                                                            <input style=" cursor:pointer;" type="file" id="nome_imagem" name="nome_imagem" onchange="preview(this);"/>
                                                        Alterar Foto
                                                        </div>

                                                          <div class="ml-auto" style="margin-top:5px;">
                                                                  <input type="submit" class="btn btn-primary eliminar" id="eliminar" name="eliminar" value="Eliminar foto" style="font-size:12px"/>
                                                          </div>
                                          </div>
                              </div>
                    <div class="col-md-6">
                          <div class="profile-head">
                                            
                                            <h4 style="font-weight: bold;" id="ancora1">
                                            <?php echo $linha['nome'];?>
                                            </h4>
                        <hr>
                        
                                            <h5>
                                            <i class="fas fa-envelope" style="color: #1c24ab"></i>
                                             <?php echo $linha['email'];?>
                                            </h5>
                                            <h5>
                                            <i class="fas fa-calendar-alt" style="color: #1c24ab"></i>
                                            <?php echo $linha['data_nasc'];?>
                                            </h5>
                                            <br>
                                            <br>
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" style="color: #1c24ab">Editar</a>
                                        </li>
                                      
                                    </ul>
                          </div>

                    </div>
                    
                    <!-- <div class="col-md-2" style="margin-left: -200px;">
                        <input type="submit" class="profile-edit-btn" name="btnAddMore" value="Edit Profile"/>
                    </div> -->
                </div>
                
                </div>
        <!-- Nome -->
        <fieldset>
          <div class="grid-35">
            <label for="fname">Nome</label>
          </div>
          <div class="grid-65">
            <input type="text" name="nome" tabindex="1" value="<?php echo $linha['nome']; ?>" pattern="[a-zA-Z0-9\sàáèéìíòóùú\?\d-_]+" title="Por favor não insira caracteres especiais!" required />
          </div>
        </fieldset>
        <!-- Email -->
        <fieldset>
          <div class="grid-35">
            <label for="lname">Email</label>
          </div>
          <div class="grid-65">
            <input type="text" name="email" tabindex="2" value="<?php echo $linha['email']; ?>" required pattern="[^@]+@[^@]+.[a-zA-Z\d-_]{2,6}" title="Por favor não insira caracteres especiais!"/>
          </div>
        </fieldset>
         <!-- Data Nascimento -->
         <fieldset>
          <div class="grid-35">
            <label for="lname">Data Nascimento</label>
          </div>
          <div class="grid-65">
            <input type="date" name="data_nasc" tabindex="2" value="<?php echo $linha['data_nasc']; ?>" >
          </div>
        </fieldset>
        <!-- Alterar Password -->
        <fieldset>
          <div class="grid-35">
            <label for="description">Alterar Password:</label>
          </div>
          <div class="grid-65">
          <input type="password" name="password" type="password" tabindex="2" pattern="[a-zA-Z0-9\sàáèéìíòóùú\?\d-_]+" title="Por favor não insira caracteres especiais!" />
          </div>
        </fieldset>
        <!-- Confirmar Password -->
        <fieldset>
          <div class="grid-35">
            <label for="location">Confirmar password:</label>
          </div>
          <div class="grid-65">
            <input type="password" name="password1" type="password" tabindex="4" pattern="[a-zA-Z0-9\sàáèéìíòóùú\?\d-_]+" title="Por favor não insira caracteres especiais!"/>
          </div>
        </fieldset>
        

        <fieldset>
          <a href="index.php"><input type="button" class="Btn cancel" value="Cancelar"/></a>
          <input type="submit" class="btn" name="gravar" id="gravar" value="Salvar Alterações" />
        </fieldset>

      
    </div>
  </div>
</div>
         
         
          
        
      

</form>
 <?php
				
				
					mysqli_free_result($result);
					mysqli_close($conn);

				?>
						
					</div>
				</div>
			</section>
			<!-- End contact-page Area -->
		</div>
		<!-- start footer Area -->
		<?php
        include('includes/footer.php');
				include('includes/botao_topo.php');
		?>	
		<!-- End footer Area -->
		<script src="assets/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="assets/js/vendor/bootstrap.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="assets/js/easing.min.js"></script>
		<script src="assets/js/hoverIntent.js"></script>
		<script src="assets/js/superfish.min.js"></script>
		<script src="assets/js/jquery.ajaxchimp.min.js"></script>
		<script src="assets/js/jquery.magnific-popup.min.js"></script>
		<script src="assets/js/mn-accordion.js"></script>
		<script src="assets/js/jquery-ui.js"></script>
		<script src="assets/js/jquery.nice-select.min.js"></script>
		<script src="assets/js/owl.carousel.min.js"></script>
		<script src="assets/js/mail-script.js"></script>
		<script src="assets/js/main.js"></script>
	</body>
</html>

<?php
				}
				?>