<?php
//iniciar a sessão
session_start();

include_once('conexao.php');



 if (isset($_POST['gravar'])) {

  $id_utilizador=$_POST['id'];
  $imagem_antiga=$_POST['nome_imagem1'];
  $passatual=$_POST['password2'];


//criar variáveis temporarias
      $nome_temp=$_POST['nome'];
      $email_temp=$_POST['email'];
      $data_nascimento_temp=$_POST['data_nasc'];
      $password_temp=$_POST['password'];
      $password1_temp=$_POST['password1'];
      $imagem_nova = $_FILES['nome_imagem']['name'];

      //Pasta onde o ficheiro vai ser salvo
			$_UP['pasta'] = '../../assets/fotos_user/';
			
			//Tamanho máximo do ficheiro em Bytes
			$_UP['tamanho'] = 1024*1024*100; //5mb
			
			//Array com a extensões permitidas
			$_UP['extensoes'] = array('png', 'jpg', 'jpeg');
			
			//Renomear
			$_UP['renomeia'] = false;
			
			//Array com os tipos de erros de upload do PHP
			$_UP['erros'][0] = 'Não houve erro';
			$_UP['erros'][1] = 'O ficheiro no upload é maior que o limite do PHP';
			$_UP['erros'][2] = 'O ficheiro ultrapassa o limite de tamanho especificado no HTML';
			$_UP['erros'][3] = 'O upload do ficheiro foi feito parcialmente';
      $_UP['erros'][4] = 'Não foi feito o upload do ficheiro';

      

        //verificar se o email já está registado
        $sql="Select * From utilizadores where email='".$_POST['email']."' AND id <>'".$id_utilizador."'";
        $result = mysqli_query($conn, $sql);

        $nr_linhas = mysqli_num_rows($result);

          /*se for encontrado um registo com o mesmo email */
        if($nr_linhas!=0){
          echo"<script language='javascript' type='text/javascript'>alert('Erro! Esse Email já está registado');window.location.href='../../editar-perfil.php';</script>";
          die();
        }

        //confirmar a password
        if((strcmp($_POST['password'],$_POST['password1'])!=0)){
          echo"<script language='javascript' type='text/javascript'>alert('Erro! Digite de novo a password');window.location.href='../../editar-perfil.php';</script>";
          die();
          }
          //password estiver vazia
          else if(empty($_POST['password'])){
            $_POST['password']=$passatual;
          }
          //se mudar a password
          else{
            $_POST['password'] = hash('sha512', $_POST['password']);
          }

        if($imagem_nova != NULL) {

      //Verifica se houve algum erro com o upload. Sem sim, exibe a mensagem do erro
			if($_FILES['nome_imagem']['error'] != 0){
				die("Não foi possivel fazer o upload, erro: <br />". $_UP['erros'][$_FILES['nome_imagem']['error']]);
				exit; //Para a execução do script
			}
			
			//Faz a verificação da extensao do ficheiro
			$img_separador = explode('.',$imagem_nova );
			$extensao = strtolower(end($img_separador));
			
			if(array_search($extensao, $_UP['extensoes'])=== false){		
				echo"<script language='javascript' type='text/javascript'>alert('Extesão inválida.');window.location.href='../../editar-perfil.php#ancora1';</script>";
        exit; //Para a execução do script
    }
			
			//Faz a verificação do tamanho do ficheiro
			if ($_UP['tamanho'] < $_FILES['nome_imagem']['size']){
				echo"<script language='javascript' type='text/javascript'>alert('Ficheiro muito grande.');window.location.href='../../editar-perfil.php#ancora1';</script>";
        exit; //Para a execução do script
      }

      
      //Primeiro verifica se deve trocar o nome do ficheiro
				if($_UP['renomeia'] == false){
					//Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg
					$nome_final = time().'.jpg';
				}else{
					//mantem o nome original do arquivo
					$nome_final = $_FILES['nome_imagem']['name'];
        }

        move_uploaded_file($_FILES['nome_imagem']['tmp_name'], $_UP['pasta']. $nome_final);

        //gravar o registo *************************************
        $sql="UPDATE utilizadores SET nome= '".$_POST['nome']."', email ='".$_POST['email']."', data_nasc ='".$_POST['data_nasc']."', password ='".$_POST['password']."', nome_imagem ='".$nome_final."'WHERE id='".$id_utilizador."'";
          $result = mysqli_query($conn, $sql);
            if ($result){
              echo"<script language='javascript' type='text/javascript'>window.location.href='../../editar-perfil.php#ancora1';</script>";
          die();
          } else
          {
            echo "Erro ".mysqli_error($conn);
            header('Location: ../../editar_perfil.php');
            }
      }
      else{

          //gravar o registo *************************************
          $sql="UPDATE utilizadores SET nome= '".$_POST['nome']."', email ='".$_POST['email']."', data_nasc ='".$_POST['data_nasc']."', password ='".$_POST['password']."', nome_imagem ='".$imagem_antiga."'WHERE id='".$id_utilizador."'";
          $result = mysqli_query($conn, $sql);
            if ($result){
          echo"<script language='javascript' type='text/javascript'>window.location.href='../../editar-perfil.php#ancora1';</script>";
          exit; //Para a execução do script
          } else
          {
            echo "Erro ".mysqli_error($conn);
            header('Location: ../../editar-perfil.php');
            }
          }
        }

        if (isset($_POST['eliminar'])) {

          $id_utilizador=$_POST['id'];

          $fotopred = "user.png";
          
          $sql="UPDATE utilizadores SET nome_imagem ='".$fotopred."'WHERE id='".$id_utilizador."'";
          $result = mysqli_query($conn, $sql);
          echo"<script language='javascript' type='text/javascript'>window.location.href='../../editar-perfil.php#ancora1';</script>";

        }
