
<?php

if(!isset($_GET['email']) || !isset($_GET['token'])){
    echo"<script>window.location.href='login.php';</script>";
}else{

    include_once('conexao.php');

    $email = $conn->real_escape_string($_GET['email']);
    $token = $conn->real_escape_string($_GET['token']);


                
  ?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>ENTRElinhas | Repor Password</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
<link rel="icon" href="../../assets/img/logo_escola.png" type="image/png">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../../assets/login_registo/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../../assets/login_registo/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../../assets/login_registo/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../../assets/login_registo/vendor/animate/animate.css">
<!--===============================================================================================-->	
<link rel="stylesheet" type="text/css" href="../../assets/login_registo/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../../assets/login_registo/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../../assets/login_registo/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
<link rel="stylesheet" type="text/css" href="../../assets/login_registo/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../../assets/login_registo/css/util.css">
<link rel="stylesheet" type="text/css" href="../../assets/login_registo/css/main.css">
<!--===============================================================================================-->



<!-- Mobile Specific Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="assets/img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="colorlib">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>ENTRElinhas | Repor password</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
<!--
CSS
    ============================================= -->
    <link rel="stylesheet" href="../../assets/css/linearicons.css">
    <link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../assets/css/magnific-popup.css">
    <link rel="stylesheet" href="../../assets/css/nice-select.css">
    <link rel="stylesheet" href="../../assets/css/animate.min.css">
    <link rel="stylesheet" href="../../assets/css/owl.carousel.css">
    <link rel="stylesheet" href="../../assets/css/jquery-ui.css">
    <link rel="stylesheet" href="../../assets/css/main.css">
</head>
<body>

<div class="limiter">
    <div class="container-login100" style="background-image: url('../../assets/login_registo/images/colegio.jpg');">
        <div class="wrap-login100">
            <form class="login100-form validate-form" method="POST" action="proc_reset_pass2.php"> 
                <span class="login100-form-logo">
                    <img src="../../assets/login_registo/images/logo_escola.png" style="width:70%;">
                </span>

                <span class="login100-form-title p-b-34 p-t-27">
                    Repor password
                </span>

                <input type="hidden" name="email" value="<?php echo $email;?>">
                <input  type="hidden" name="token" value="<?php echo $token;?>">

                <div class="wrap-input100 validate-input" data-validate = "Insira a password">
                    <input class="input100" type="password" name="password" id="password" placeholder="Nova password.." pattern="[a-zA-Z0-9\sàáèéìíòóùú\?\d-_]+" title="Por favor não insira caracteres especiais!">
                    <span class="focus-input100" data-placeholder="&#xf191;"></span>
                </div>
                <div class="wrap-input100 validate-input" data-validate = "Repita a password">
                    <input class="input100" type="password" name="password1" id="password1" placeholder="Repita a nova password.." pattern="[a-zA-Z0-9\sàáèéìíòóùú\?\d-_]+" title="Por favor não insira caracteres especiais!">
                    <span class="focus-input100" data-placeholder="&#xf191;"></span>
                </div>
                <div class="container-login100-form-btn">
                    <button class="login100-form-btn" id="reset" name="reset">
                        Repor
                    </button>
                </div>

                <div>
                    <a href="login.php"> <p style="color:white;text-align:center;margin-top:20px;font-size:13px;"> Sair  </p> </a>
                </div>
            </form>
        </div>
    </div>
</div>




<!--===============================================================================================-->
<script src="../../assets/login_registo/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../../assets/login_registo/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../../assets/login_registo/vendor/bootstrap/js/popper.js"></script>
<script src="../../assets/login_registo/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../../assets/login_registo/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../../assets/login_registo/vendor/daterangepicker/moment.min.js"></script>
<script src="../../assets/login_registo/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../../assets/login_registo/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../../assets/login_registo/js/main.js"></script>

</body>
</html>

<?php

    }

?>