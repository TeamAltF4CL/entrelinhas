<!DOCTYPE html>
<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="assets/img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>ENTRElinhas</title>
		<link rel="icon" href="../../assets/img/logo_escola.png" type="image/png">
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="../../assets/css/linearicons.css">
		<link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="../../assets/css/bootstrap.css">
		<link rel="stylesheet" href="../../assets/css/magnific-popup.css">
		<link rel="stylesheet" href="../../assets/css/nice-select.css">
		<link rel="stylesheet" href="../../assets/css/animate.min.css">
		<link rel="stylesheet" href="../../assets/css/owl.carousel.css">
		<link rel="stylesheet" href="../../assets/css/jquery-ui.css">
		<link rel="stylesheet" href="../../assets/css/main.css">
	</head>
	<body>


<?php

  // Import PHPMailer classes into the global namespace
  // These must be at the top of your script, not inside a function
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;
  /*
  $nome_para = $_POST['name'];
  $email_para = $_POST['email'];
  $assunto_para = $_POST['subject'];
  $mensagem_para = $_POST['msg'];
  */



  include_once('proc_login.php');	



        if (isset($_POST['submeter'])) {


          date_default_timezone_set('Europe/Lisbon');
          $data = date('Y-m-d H:i:s');

          //Infs do aritgo
          $titulo = $_POST['titulo'];
          $assunto = $_POST['assunto'];
          $texto = $_POST['texto'];
          
          $imagem1 = $_FILES['imagem1']['name'];
          $imagem2 = $_FILES['imagem2']['name'];
          $imagem3 = $_FILES['imagem3']['name'];

              //User infs
              $user_infs= "Select * From utilizadores where id='".$_SESSION['user_id']."' ";
              $user_infs_result =mysqli_query($conn, $user_infs);
              $linha = mysqli_fetch_array($user_infs_result, MYSQLI_ASSOC);

              $email = $linha['email'];
              $nome = $linha['nome'];

               //Pasta onde o ficheiro vai ser salvo
							$_UP['pasta'] = '../../assets/img/noticias/';
			
							//Tamanho máximo do ficheiro em Bytes
							$_UP['tamanho'] = 1024*1024*1024*1024*1024*1024*1024*1024*1024*1024*1024*100; //  (capacidade do tamanho do ficheiro)
							
							//Array com a extensões permitidas
							$_UP['extensoes'] = array('png', 'jpg', 'jpeg', '');
							
							//Renomear
              $_UP['renomeia'] = false;
              
              //Array com os tipos de erros de upload do PHP
							$_UP['erros'][0] = 'Não houve erro';
							$_UP['erros'][1] = 'O ficheiro no upload é maior que o limite do PHP';
							$_UP['erros'][2] = 'O ficheiro ultrapassa o limite de tamanho especificado no HTML';
							$_UP['erros'][3] = 'O upload do ficheiro foi feito parcialmente';
							$_UP['erros'][4] = 'Não foi feito o upload do ficheiro';

          // VERIFICAÇÃO DAS IMAGENS!!

            //Faz a verificação da extensao do ficheiro
            $separar = explode('.',$imagem1 );
            $extensao = strtolower(end($separar));

            $separar2 = explode('.',$imagem2 );
            $extensao2 = strtolower(end($separar2));

            $separar3 = explode('.',$imagem3 );
            $extensao3 = strtolower(end($separar3));

            if(array_search($extensao, $_UP['extensoes'])=== false){		
              echo"<script language='javascript' type='text/javascript'>alert('Extensão inválida.');window.location.href='../../partilha.php';</script>";
              exit; //Para a execução do script
          }elseif(array_search($extensao2, $_UP['extensoes'])=== false){
            echo"<script language='javascript' type='text/javascript'>alert('Extensão inválida.');window.location.href='../../partilhaphp';</script>";
            exit; //Para a execução do script
          }elseif(array_search($extensao3, $_UP['extensoes'])=== false){
            echo"<script language='javascript' type='text/javascript'>alert('Extensão inválida.');window.location.href='../../partilha.php';</script>";
            exit;  //Para a execução do script

          }
  
    //Primeiro verifica se deve trocar o nome do ficheiro
    if($_UP['renomeia'] == false){
      //Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg

      //Verifica se é video ou imagem

      //PARA O 1 CONTEUDO
      if($extensao == 'png'){

            $nome_final = uniqid('entrelinhas', true).'.png';;

          }if($extensao == 'gif'){

            $nome_final = uniqid('entrelinhas', true).'.gif';

          }if($extensao == 'jpg'){

            $nome_final = uniqid('entrelinhas', true).'.jpg';
        }

          if($extensao == 'jpeg'){

          $nome_final = uniqid('entrelinhas', true).'.jpeg';

      }

      if($extensao == ''){

        $nome_final = '';

    }
    //FIM DO 1 CONTEUDO



                        //PARA O 2 CONTEUDO
                        if($extensao2 == 'png'){

                          $nome_final2 = uniqid('entrelinhas', true).'.png';

                        }if($extensao2 == 'jpg'){

                          $nome_final2 = uniqid('entrelinhas', true).'.jpg';
                      }

                        if($extensao2 == 'jpeg'){

                        $nome_final2 = uniqid('entrelinhas', true).'.jpeg';

                    }

                    if($extensao2 == ''){

                      $nome_final2 = '';

                  }
                  //FIM DO 2 CONTEUDO



                            //PARA O 3 CONTEUDO
                            if($extensao3 == 'png'){

                              $nome_final3 = uniqid('entrelinhas', true).'.png';

                            }if($extensao3 == 'jpg'){

                              $nome_final3 = uniqid('entrelinhas', true).'.jpg';
                          }

                            if($extensao3 == 'jpeg'){

                            $nome_final3 = uniqid('entrelinhas', true).'.jpeg';

                        }

                        if($extensao3 == ''){

                          $nome_final3 = '';
    
                      }
                      //FIM DO 3 CONTEUDO


              //Fim da verificação da extensao do ficheiro




             //inserir o registo *************************************
          $submeter="INSERT INTO pub_temp (titulo,assunto,texto,img1,img2,img3,dados,data) VALUES ('$titulo','$assunto','$texto','$nome_final','$nome_final2','$nome_final3','".$_SESSION['user_id']."','$data')";
          $result_submeter = mysqli_query($conn, $submeter);

              if ($result_submeter){

                //Se submeter a pub, move as fotos pra pasta
                move_uploaded_file($_FILES['imagem1']['tmp_name'], $_UP['pasta']. $nome_final);
                move_uploaded_file($_FILES['imagem2']['tmp_name'], $_UP['pasta']. $nome_final2);
                move_uploaded_file($_FILES['imagem3']['tmp_name'], $_UP['pasta']. $nome_final3);

                  //Se submter a pub, manda email de confirmação pra ele

                    // Load Composer's autoloader
                    require '../vendor/autoload.php';

                    // Instantiation and passing `true` enables exceptions
                    $mail = new PHPMailer(true);

                    try {
                        //Server settings
                        $mail->SMTPDebug = 0;                                       // Enable verbose debug output
                        $mail->isSMTP();                                            // Set mailer to use SMTP
                        $mail->Host       = 'smtp.gmail.com';  // Specify main and backup SMTP servers
                        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                        $mail->Username   = 'teamaltf4cl@gmail.com';                     // SMTP username
                        $mail->Password   = 'Colegio2019';                               // SMTP password
                        $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
                        $mail->Port       = 587;                                    // TCP port to connect to

                        //Recipients
                        $mail->setFrom('teamaltf4cl@gmail.com', 'ENTRElinhas');
                        $mail->addAddress($email, utf8_decode($nome));     // Add a recipient        


                        // Content
                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->Subject = utf8_decode('Publicação submetida para aprovação');
                        $mail->Body    = utf8_decode("Olá $nome, venho por este meio lhe informar que a sua publicação foi submetida para avaliação de administradores. Receberá mais informações, refrente à mesma, em breve.
                                             <br>
                                             
                                             A sua publicação submetida, segue-se asseguir: <br><br>
                                             
                                            Título: ''$titulo'' <br>
                                             
                                             Assunto: ''$assunto'' <br><br>
                                             
                                             Publicação: ''$texto''
                        
                       ");



                        $mail->send();
                       
                    } catch (Exception $e) {
                        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
                    }

                  ?>

									<!-- Modal -->
									<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h4 class="modal-title" id="myModalLabel">Sucesso!</h4>
											</div>
											<div class="modal-body">
											  A sua publicação foi submetida com sucesso!
											</div>
											<div class="modal-footer">
												<a href="../../partilha.php"><button type="button" class="btn btn-primary">Ok</button></a>
											</div>
										</div>
									</div>
								</div>

							<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
									<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
								<!-- Include all compiled plugins (below), or include individual files as needed -->
								<script src="js/bootstrap.min.js"></script>
								
								<?php
									$situacao_usuario = "pendente";
									if($situacao_usuario == "pendente"){ ?>
										<script>
											$(document).ready(function(){
												$('#myModal').modal('show');
											});
										</script>
							<?php } ?>

                  <?php


            }else{
             
              ?>

									<!-- Modal -->
									<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h4 class="modal-title" id="myModalLabel">Falhou!</h4>
											</div>
											<div class="modal-body">
												Não foi possível de submeter a sua publicação.
											</div>
											<div class="modal-footer">
												<a href="../../partilha.php"><button type="button" class="btn btn-primary">Ok</button></a>
											</div>
										</div>
									</div>
								</div>

							<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
									<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
								<!-- Include all compiled plugins (below), or include individual files as needed -->
								<script src="js/bootstrap.min.js"></script>
								
								<?php
									$situacao_usuario = "pendente";
									if($situacao_usuario == "pendente"){ ?>
										<script>
											$(document).ready(function(){
												$('#myModal').modal('show');
											});
										</script>
							<?php } ?>

              <?php
              }


            } //Se o remane for igual a false


        }else{ //Se nao houver clique no botao submeter
          echo"<script language='javascript' type='text/javascript'>window.location.href='../../partilha.php';</script>";
        }
?>


<script src="../../assets/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="../../assets/js/vendor/bootstrap.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="../../assets/js/easing.min.js"></script>
		<script src="../../assets/js/hoverIntent.js"></script>
		<script src="../../assets/js/superfish.min.js"></script>
		<script src="../../assets/js/jquery.ajaxchimp.min.js"></script>
		<script src="../../assets/js/jquery.magnific-popup.min.js"></script>
		<script src="../../assets/js/mn-accordion.js"></script>
		<script src="../../assets/js/jquery-ui.js"></script>
		<script src="../../assets/js/jquery.nice-select.min.js"></script>
		<script src="../../assets/js/owl.carousel.min.js"></script>
		<script src="../../assets/js/mail-script.js"></script>
		<script src="../../assets/js/main.js"></script>

		</body>

	</html>