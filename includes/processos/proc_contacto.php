<!DOCTYPE html>
<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="assets/img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>ENTRElinhas</title>
		<link rel="icon" href="../../assets/img/logo_escola.png" type="image/png">
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="../../assets/css/linearicons.css">
		<link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="../../assets/css/bootstrap.css">
		<link rel="stylesheet" href="../../assets/css/magnific-popup.css">
		<link rel="stylesheet" href="../../assets/css/nice-select.css">
		<link rel="stylesheet" href="../../assets/css/animate.min.css">
		<link rel="stylesheet" href="../../assets/css/owl.carousel.css">
		<link rel="stylesheet" href="../../assets/css/jquery-ui.css">
		<link rel="stylesheet" href="../../assets/css/main.css">
	</head>
	<body>

<?php

  // Import PHPMailer classes into the global namespace
  // These must be at the top of your script, not inside a function
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;
  /*
  $nome_para = $_POST['name'];
  $email_para = $_POST['email'];
  $assunto_para = $_POST['subject'];
  $mensagem_para = $_POST['msg'];
  */

if (isset($_POST['contacto'])) {

    $nome= $_POST['nome'];
    $email= $_POST['email'];
    $assunto= $_POST['assunto'];
    $mensagem= $_POST['mensagem'];

                //Se inserir o user, manda email de confirmação pra ele

                  // Load Composer's autoloader
                  require '../vendor/autoload.php';

                  // Instantiation and passing `true` enables exceptions
                  $mail = new PHPMailer(true);

                  try {
                      //Server settings
                      $mail->SMTPDebug = 0;                                       // Enable verbose debug output
                      $mail->isSMTP();                                            // Set mailer to use SMTP
                      $mail->Host       = 'smtp.gmail.com';  // Specify main and backup SMTP servers
                      $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                      $mail->Username   = 'teamaltf4cl@gmail.com';                     // SMTP username
                      $mail->Password   = 'Colegio2019';                               // SMTP password
                      $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
                      $mail->Port       = 587;                                    // TCP port to connect to

                      //Recipients
                      $mail->setFrom($email, utf8_decode($nome));
                      $mail->addAddress('teamaltf4cl@gmail.com', 'ENTRElinhas'); 
                      $mail->addReplyTo($email, utf8_decode($nome));    // Add a recipient        


                      // Content
                      $mail->isHTML(true);                                  // Set email format to HTML
                      $mail->Subject = utf8_decode("$assunto");
                      $mail->Body    = utf8_decode(" De $nome, $email
                      
                                                    <br>$mensagem");


                                                    $mail->send();
                                                  ?>

									<!-- Modal -->
									<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h4 class="modal-title" id="myModalLabel">Sucesso!</h4>
											</div>
											<div class="modal-body">
											  A sua mensagem foi enviada com sucesso!
											</div>
											<div class="modal-footer">
												<a href="../../contacto.php"><button type="button" class="btn btn-primary">Ok</button></a>
											</div>
										</div>
									</div>
								</div>

							<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
									<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
								<!-- Include all compiled plugins (below), or include individual files as needed -->
								<script src="js/bootstrap.min.js"></script>
								
								<?php
									$situacao_usuario = "pendente";
									if($situacao_usuario == "pendente"){ ?>
										<script>
											$(document).ready(function(){
												$('#myModal').modal('show');
											});
										</script>
			<?php } ?>

                                              <?php
                                                } catch (Exception $e) {
                                                    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
                                                }
       }

  
    

          ?>

<script src="../../assets/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="../../assets/js/vendor/bootstrap.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="../../assets/js/easing.min.js"></script>
		<script src="../../assets/js/hoverIntent.js"></script>
		<script src="../../assets/js/superfish.min.js"></script>
		<script src="../../assets/js/jquery.ajaxchimp.min.js"></script>
		<script src="../../assets/js/jquery.magnific-popup.min.js"></script>
		<script src="../../assets/js/mn-accordion.js"></script>
		<script src="../../assets/js/jquery-ui.js"></script>
		<script src="../../assets/js/jquery.nice-select.min.js"></script>
		<script src="../../assets/js/owl.carousel.min.js"></script>
		<script src="../../assets/js/mail-script.js"></script>
		<script src="../../assets/js/main.js"></script>

		</body>

	</html>