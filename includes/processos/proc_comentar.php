<!DOCTYPE html>
<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="assets/img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>ENTRElinhas</title>
		<link rel="icon" href="../../assets/img/logo_escola.png" type="image/png">
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="../../assets/css/linearicons.css">
		<link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="../../assets/css/bootstrap.css">
		<link rel="stylesheet" href="../../assets/css/magnific-popup.css">
		<link rel="stylesheet" href="../../assets/css/nice-select.css">
		<link rel="stylesheet" href="../../assets/css/animate.min.css">
		<link rel="stylesheet" href="../../assets/css/owl.carousel.css">
		<link rel="stylesheet" href="../../assets/css/jquery-ui.css">
		<link rel="stylesheet" href="../../assets/css/main.css">
	</head>
	<body>

	<?php
			include_once('proc_login.php');	

			if(isset($_SESSION['user_id'])){

						if(isset($_POST['comentar'])){

							date_default_timezone_set('Europe/Lisbon');
								$data_hora = date('Y-m-d H:i:s');
								$comentario = $_POST['comentario'];
								$id_pub = $_POST['id_pub'];
								$titulo_pub = $_POST['titulo_pub'];
								$user_id = $_SESSION['user_id'];
									

								$conn = new PDO ("mysql:host=127.0.0.1;dbname=entrelin_entrelinhas", "root", "");
								
								$conteudo = [
									'id_publicacao' => $id_pub,
									'id_utilizador' => $id_pub,
									'comentario' => utf8_encode($comentario),
									'data_hora' => $data_hora,
									'estado' => 0,
								];
								

						try{


							//inserir o registo *************************************
							$submeter_comentario=$conn->prepare("INSERT INTO comentarios (id_publicacao,id_utilizador,comentario,data_hora, estado) VALUES ('$id_pub','$user_id','$comentario','$data_hora','0')");
						

							if ($submeter_comentario->execute($conteudo)){
								
									?>
							

									<!-- Modal -->
									<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h4 class="modal-title" id="myModalLabel">Sucesso!</h4>
											</div>
											<div class="modal-body">
												Comentário submetido para aprovação de admistradores.
											</div>
											<div class="modal-footer">
												<a href="../../noticia.php?id=<?php echo $id_pub; ?>&titulo=<?php echo $titulo_pub ;?>#likes"><button type="button" class="btn btn-primary">Ok</button></a>
											</div>
										</div>
									</div>
								</div>

							<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
									<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
								<!-- Include all compiled plugins (below), or include individual files as needed -->
								<script src="js/bootstrap.min.js"></script>
								
								<?php
									$situacao_usuario = "pendente";
									if($situacao_usuario == "pendente"){ ?>
										<script>
											$(document).ready(function(){
												$('#myModal').modal('show');
											});
										</script>
			<?php } ?>

							<?php
							}else{
									?>

									<!-- Modal -->
									<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h4 class="modal-title" id="myModalLabel">Falhou!</h4>
											</div>
											<div class="modal-body">
												Não foi possível de submeter o seu comentário.
											</div>
											<div class="modal-footer">
												<a href="../../noticia.php?id=<?php echo $id_pub; ?>&titulo=<?php echo $titulo_pub ;?>#likes"><button type="button" class="btn btn-primary">Ok</button></a>
											</div>
										</div>
									</div>
								</div>

							<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
									<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
								<!-- Include all compiled plugins (below), or include individual files as needed -->
								<script src="js/bootstrap.min.js"></script>
								
								<?php
									$situacao_usuario = "pendente";
									if($situacao_usuario == "pendente"){ ?>
										<script>
											$(document).ready(function(){
												$('#myModal').modal('show');
											});
										</script>
							<?php } ?>



									<?php
											}

											
										}catch(PDOException $e) { // casso retorne erro
											echo"<script language='javascript' type='text/javascript'>location='../../noticia.php?id=$id_pub&titulo=$titulo_pub#likes';</script>";
											echo('Erro: ' . $e->getMessage()); 
										  
										  }

									}else{

										}

										
				}else{

					echo"<script>location='../../index.php'</script>";
				}


	?>

		<script src="../../assets/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="../../assets/js/vendor/bootstrap.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="../../assets/js/easing.min.js"></script>
		<script src="../../assets/js/hoverIntent.js"></script>
		<script src="../../assets/js/superfish.min.js"></script>
		<script src="../../assets/js/jquery.ajaxchimp.min.js"></script>
		<script src="../../assets/js/jquery.magnific-popup.min.js"></script>
		<script src="../../assets/js/mn-accordion.js"></script>
		<script src="../../assets/js/jquery-ui.js"></script>
		<script src="../../assets/js/jquery.nice-select.min.js"></script>
		<script src="../../assets/js/owl.carousel.min.js"></script>
		<script src="../../assets/js/mail-script.js"></script>
		<script src="../../assets/js/main.js"></script>

		</body>

	</html>