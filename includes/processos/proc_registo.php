<!DOCTYPE html>
<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="assets/img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>ENTRElinhas</title>
		<link rel="icon" href="../../assets/img/logo_escola.png" type="image/png">
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="../../assets/css/linearicons.css">
		<link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="../../assets/css/bootstrap.css">
		<link rel="stylesheet" href="../../assets/css/magnific-popup.css">
		<link rel="stylesheet" href="../../assets/css/nice-select.css">
		<link rel="stylesheet" href="../../assets/css/animate.min.css">
		<link rel="stylesheet" href="../../assets/css/owl.carousel.css">
		<link rel="stylesheet" href="../../assets/css/jquery-ui.css">
		<link rel="stylesheet" href="../../assets/css/main.css">
	</head>
	<body>

<?php

  // Import PHPMailer classes into the global namespace
  // These must be at the top of your script, not inside a function
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;
  /*
  $nome_para = $_POST['name'];
  $email_para = $_POST['email'];
  $assunto_para = $_POST['subject'];
  $mensagem_para = $_POST['msg'];
  */

//fazer conexao com a base de dados
  $conn = new PDO ("mysql:host=127.0.0.1;dbname=entrelin_entrelinhas", "root", "");

  try{

  if (isset($_POST['registar'])) {

        
          // obter os dados 
          $nome= utf8_decode($_POST['nome']); 
          $email=$_POST['email'];
          $password=$_POST['password'];
          $password1=$_POST['password1'];
          $imagem = $_FILES['nome_imagem']['name'];
          $estado=0;

          $token = 'qwertzuiopasdfghjklyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM0123456789!$/()*';
          $token = str_shuffle($token);
          $token = substr($token, 0, 10);

          //Pasta onde o ficheiro vai ser salvo
        $_UP['pasta'] = '../../assets/fotos_user/';
        
        //Tamanho máximo do ficheiro em Bytes
        $_UP['tamanho'] = 1024*1024*100; //5mb
        
        //Array com a extensões permitidas
        $_UP['extensoes'] = array('png', 'jpg', 'jpeg');
        
        //Renomear
        $_UP['renomeia'] = false;
        
        //Array com os tipos de erros de upload do PHP
        $_UP['erros'][0] = 'Não houve erro';
        $_UP['erros'][1] = 'O ficheiro no upload é maior que o limite do PHP';
        $_UP['erros'][2] = 'O ficheiro ultrapassa o limite de tamanho especificado no HTML';
        $_UP['erros'][3] = 'O upload do ficheiro foi feito parcialmente';
        $_UP['erros'][4] = 'Não foi feito o upload do ficheiro';

              //verificar se o email está registado
              $result_utilizador = $conn->prepare("SELECT email FROM utilizadores WHERE email = '$email'");
              $result_utilizador->execute();

              $nr_linhas = $result_utilizador->rowCount();

            /*se for encontrado um registo com o mesmo email */
          if($nr_linhas<>0){
            
            
            ?>

                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Falhou!</h4>
                                    </div>
                                    <div class="modal-body">
                                      Esse email já existe!
                                    </div>
                                    <div class="modal-footer">
                                      <a href="../../registo.php"><button type="button" class="btn btn-primary">Ok</button></a>
                                    </div>
                                  </div>
                                </div>
                              </div>

                            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                              <!-- Include all compiled plugins (below), or include individual files as needed -->
                              <script src="js/bootstrap.min.js"></script>
                              
                              <?php
                                $situacao_usuario = "pendente";
                                if($situacao_usuario == "pendente"){ ?>
                                  <script>
                                    $(document).ready(function(){
                                      $('#myModal').modal('show');
                                    });
                                  </script>
                            <?php } ?>

            <?php
          }else{

          //confirmar a password
          if((strcmp($_POST['password'],$_POST['password1'])!=0)){
            
            ?>

                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Falhou!</h4>
                                    </div>
                                    <div class="modal-body">
                                    Certifique-se que as passwords coincidem!
                                    </div>
                                    <div class="modal-footer">
                                      <a href="../../registo.php"><button type="button" class="btn btn-primary">Ok</button></a>
                                    </div>
                                  </div>
                                </div>
                              </div>

                            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                              <!-- Include all compiled plugins (below), or include individual files as needed -->
                              <script src="js/bootstrap.min.js"></script>
                              
                              <?php
                                $situacao_usuario = "pendente";
                                if($situacao_usuario == "pendente"){ ?>
                                  <script>
                                    $(document).ready(function(){
                                      $('#myModal').modal('show');
                                    });
                                  </script>
                            <?php } ?>

            <?php

            }
            //se mudar a password
            else{
              $password = hash('sha512', $_POST['password']);
            

          if($imagem != NULL) {

        //Verifica se houve algum erro com o upload. Sem sim, exibe a mensagem do erro
        if($_FILES['nome_imagem']['error'] != 0){
          die("Não foi possivel fazer o upload, erro: <br />". $_UP['erros'][$_FILES['nome_imagem']['error']]);
          exit; //Para a execução do script
        }else{

        
        
        //Faz a verificação da extensao do ficheiro
        $img_separador = explode('.',$imagem );
        $extensao = strtolower(end($img_separador));
        
        if(array_search($extensao, $_UP['extensoes'])=== false){		
          
        ?>

                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Falhou!</h4>
                                    </div>
                                    <div class="modal-body">
                                    Extesão inválida!
                                    </div>
                                    <div class="modal-footer">
                                      <a href="../../registo.php"><button type="button" class="btn btn-primary">Ok</button></a>
                                    </div>
                                  </div>
                                </div>
                              </div>

                            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                              <!-- Include all compiled plugins (below), or include individual files as needed -->
                              <script src="js/bootstrap.min.js"></script>
                              
                              <?php
                                $situacao_usuario = "pendente";
                                if($situacao_usuario == "pendente"){ ?>
                                  <script>
                                    $(document).ready(function(){
                                      $('#myModal').modal('show');
                                    });
                                  </script>
                            <?php } ?>

        <?php

      }else{

      
        
        //Faz a verificação do tamanho do ficheiro
        if ($_UP['tamanho'] < $_FILES['nome_imagem']['size']){
          
         ?>

                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Falhou!</h4>
                                    </div>
                                    <div class="modal-body">
                                    Ficheiro muito grande!
                                    </div>
                                    <div class="modal-footer">
                                      <a href="../../registo.php"><button type="button" class="btn btn-primary">Ok</button></a>
                                    </div>
                                  </div>
                                </div>
                              </div>

                            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                              <!-- Include all compiled plugins (below), or include individual files as needed -->
                              <script src="js/bootstrap.min.js"></script>
                              
                              <?php
                                $situacao_usuario = "pendente";
                                if($situacao_usuario == "pendente"){ ?>
                                  <script>
                                    $(document).ready(function(){
                                      $('#myModal').modal('show');
                                    });
                                  </script>
                            <?php } ?>

         <?php
        }else{

        
      
        
        //Primeiro verifica se deve trocar o nome do ficheiro
          if($_UP['renomeia'] == false){
            //Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg
            $nome_final = time().'.jpg';
          }else{
            //mantem o nome original do arquivo
            $nome_final = $_FILES['nome_imagem']['name'];
          }

          move_uploaded_file($_FILES['nome_imagem']['tmp_name'], $_UP['pasta']. $nome_final);


          
          //inserir o registo *************************************
          $sql=("INSERT INTO utilizadores (nome,email,password,nome_imagem, token, estado) VALUES ('$nome','$email','$password','$nome_final','$token','$estado')");
          $result = $conn->prepare($sql); 
          $result->execute(array($nome, $email, $password, $nome_final, $token, 0));

              if ($sql){

                  //Se inserir o user, manda email de confirmação pra ele

                    // Load Composer's autoloader
                    require '../vendor/autoload.php';

                    // Instantiation and passing `true` enables exceptions
                    $mail = new PHPMailer(true);

                    try {
                        //Server settings
                        $mail->SMTPDebug = 0;                                       // Enable verbose debug output
                        $mail->isSMTP();                                            // Set mailer to use SMTP
                        $mail->Host       = 'smtp.gmail.com';  // Specify main and backup SMTP servers
                        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                        $mail->Username   = 'teamaltf4cl@gmail.com';                     // SMTP username
                        $mail->Password   = 'Colegio2019';                               // SMTP password
                        $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
                        $mail->Port       = 587;                                    // TCP port to connect to

                        //Recipients
                        $mail->setFrom('teamaltf4cl@gmail.com', 'ENTRElinhas');
                        $mail->addAddress($email, utf8_decode($nome));     // Add a recipient        


                        // Content
                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->Subject = utf8_decode('Validacao de email');
                        $mail->Body    = utf8_decode("Olá $nome, venho por este meio pedir que confirme o seu email. Clique o link abaixo.<br><br>
                        
                        <a href='http://localhost/entrelinhas/includes/processos/valida.php?email=$email&token=$token'>Clique aqui para confirmar</a>");


                        $mail->send();
                       
                    } catch (Exception $e) {
                        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
                    }

              ?>
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Sucesso!</h4>
                                    </div>
                                    <div class="modal-body">
                                      Foi registado com sucesso!
                                      Verifique o seu email!
                                    </div>
                                    <div class="modal-footer">
                                      <a href="../../login.php"><button type="button" class="btn btn-primary">Ok</button></a>
                                    </div>
                                  </div>
                                </div>
                              </div>

                            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                              <!-- Include all compiled plugins (below), or include individual files as needed -->
                              <script src="js/bootstrap.min.js"></script>
                              
                              <?php
                                $situacao_usuario = "pendente";
                                if($situacao_usuario == "pendente"){ ?>
                                  <script>
                                    $(document).ready(function(){
                                      $('#myModal').modal('show');
                                    });
                                  </script>
                            <?php } ?>

              <?php

            } else
            {
              echo "Erro ".mysqli_error($conn);
              }
            }
        }
      }
    }
        else{

          $imagem="user.png";

            //inserir o registo *************************************
            $sql=("INSERT INTO utilizadores (nome,email,password,nome_imagem, token, estado) VALUES ('$nome','$email','$password','$imagem','$token','$estado')");
            $result = $conn->prepare($sql); 
            $result->execute(array($nome, $email, $password, $imagem, $token, 0));

              if ($sql){


                  //Se inserir o user, manda email de confirmação pra ele

                    // Load Composer's autoloader
                    require '../vendor/autoload.php';

                    // Instantiation and passing `true` enables exceptions
                    $mail = new PHPMailer(true);

                    try {
                        //Server settings
                        $mail->SMTPDebug = 0;                                       // Enable verbose debug output
                        $mail->isSMTP();                                            // Set mailer to use SMTP
                        $mail->Host       = 'smtp.gmail.com';  // Specify main and backup SMTP servers
                        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                        $mail->Username   = 'teamaltf4cl@gmail.com';                     // SMTP username
                        $mail->Password   = 'Colegio2019';                               // SMTP password
                        $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
                        $mail->Port       = 587;                                    // TCP port to connect to

                        //Recipients
                        $mail->setFrom('teamaltf4cl@gmail.com', 'ENTRElinhas');
                        $mail->addAddress($email, utf8_decode($nome));     // Add a recipient        


                        // Content
                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->Subject = utf8_decode('Validacao de email');
                        $mail->Body    = utf8_decode("Olá $nome, venho por este meio pedir que confirme o seu email. Clique o link abaixo.<br><br>
                        
                        <a href='http://localhost/entrelinhas/includes/processos/valida.php?email=$email&token=$token'>Clique aqui para confirmar</a>");


                        $mail->send();
                        
                    } catch (Exception $e) {
                        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
                    }

                    ?>
									<!-- Modal -->
									<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h4 class="modal-title" id="myModalLabel">Sucesso!</h4>
											</div>
											<div class="modal-body">
												Foi registado com sucesso!
                        Verifique o seu email!
											</div>
											<div class="modal-footer">
												<a href="../../login.php"><button type="button" class="btn btn-primary">Ok</button></a>
											</div>
										</div>
									</div>
								</div>

							<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
									<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
								<!-- Include all compiled plugins (below), or include individual files as needed -->
								<script src="js/bootstrap.min.js"></script>
								
								<?php
									$situacao_usuario = "pendente";
									if($situacao_usuario == "pendente"){ ?>
										<script>
											$(document).ready(function(){
												$('#myModal').modal('show');
											});
										</script>
							<?php } ?>
        <?php
           

            } else
            {
              echo "Erro ".mysqli_error($conn);
              }
            }
          }
        }
        
        }

        }catch(PDOException $e) { // casso retorne erro
          echo"<script></script>";
          echo('Erro: ' . $e->getMessage()); 
        
        }
        

        

          ?>

          		<script src="../../assets/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="../../assets/js/vendor/bootstrap.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="../../assets/js/easing.min.js"></script>
		<script src="../../assets/js/hoverIntent.js"></script>
		<script src="../../assets/js/superfish.min.js"></script>
		<script src="../../assets/js/jquery.ajaxchimp.min.js"></script>
		<script src="../../assets/js/jquery.magnific-popup.min.js"></script>
		<script src="../../assets/js/mn-accordion.js"></script>
		<script src="../../assets/js/jquery-ui.js"></script>
		<script src="../../assets/js/jquery.nice-select.min.js"></script>
		<script src="../../assets/js/owl.carousel.min.js"></script>
		<script src="../../assets/js/mail-script.js"></script>
		<script src="../../assets/js/main.js"></script>

		</body>

	</html>