<style>
  
/* Scroll up */
.scrollToTop {
  bottom: 60px;
  color: #fff;
  background-color: #1c24ab;
  display: none;
  font-size: 23px;
  height: 50px;
  line-height: 45px;
  position: fixed;
  right: 20px;
  text-align: center;
  text-decoration: none;
  -webkit-transition: all 0.5s ease 0s;
  -moz-transition: all 0.5s ease 0s;
  -ms-transition: all 0.5s ease 0s;
  -o-transition: all 0.5s ease 0s;
  transition: all 0.5s ease 0s;
  width: 50px;
  z-index: 999;
  margin-bottom: 50px;
  border: solid 2px #fff;
}

.scrollToTop:hover,
.scrollToTop:focus {
  background-color: #FFF;
  border: solid 2px #1c24ab;
  color: #1c24ab;
  text-decoration: none;
  outline: none;
}
</style>

	<!-- SCROLL TOP BUTTON -->
	<a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->


  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
      
  <script >  //Check to see if the window is top if not then display button

    jQuery(window).scroll(function(){
      if ($(this).scrollTop() > 300) {
        $('.scrollToTop').fadeIn();
      } else {
        $('.scrollToTop').fadeOut();
      }
    });
     
    //Click event to scroll to top

    jQuery('.scrollToTop').click(function(){
      $('html, body').animate({scrollTop : 0},800);
      return false;
    });
 </script>