$(document).ready(function(){
  $('#login').on('submit',function(e) {  //Don't foget to change the id form
  $.ajax({
      url:'includes/processos/proc_login.php', //===PHP file name====
      data:$(this).serialize(),
      type:'POST',
      success:function(data){
        console.log(data);
        //Success Message == 'Title', 'Message body', Last one leave as it is
	    window.location.href='index.php';
      },
      error:function(data){
        //Error Message == 'Title', 'Message body', Last one leave as it is
	    swal("Oops...", "Password/Email errados :(", "error");
      }
    });
    e.preventDefault(); //This is to Avoid Page Refresh and Fire the Event "Click"
  });
});