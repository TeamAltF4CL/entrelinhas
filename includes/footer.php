<script src="//platform-api.sharethis.com/js/sharethis.js#property=5ca216ad9b272f00119aba4a&product=inline-follow-buttons"></script>
<!-- start footer Area -->
		<footer class="footer-area section-gap">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-6 single-footer-widget">
						<h4>Contactos</h4>
						<ul>
							<li><a>Rua da Salgueirinha, n.º 325, 4535-368 St.ª .M.ª de Lamas</a></li>
							<li><a>227470210</a></li>
							<li><a href="mailto:geral@colegiodelamas.com ">geral@colegiodelamas.com </a></li>
						</ul>
					</div>
					<div class="col-lg-2 col-md-6 single-footer-widget">
						<h4>Parcerias</h4>
						<ul>
							<li><a href="https://www.colegiodelamas.com/index.php/blog/atividades/entrelinhas"  target="_blank">ENTRElinhas</a></li>
							<li><a href="https://complexo.colegiodelamas.com/"  target="_blank">Complexo Desportivo</a></li>
							<li><a href="https://www.colegiodelamas.com/index.php/inovacao/cwm" target="_blank">Coding</a></li>
							<li><a href="http://office365.colegiodelamas.com/" tagert="_blank">Office 365</a></li>
						</ul>
					</div>
					<div class="col-lg-2 col-md-6 single-footer-widget">
						<h4>Links</h4>
						<ul>
						<li><a href="https://www.colegiodelamas.com" target="_blank">Colégio de Lamas</a></li>
							<li><a href="http://62.48.253.34:8080/PortalSIGE/" tagert="_blank">Portal Sige</a></li>
							<li><a href="https://museu.colegiodelamas.com/" target="_blank">Museu de Lamas</a></li>
							<li><a href="http://www.colegiodelamas.com/moodle2015/moodle/login/index.php" tagert="_blank">Moodle</a></li>
						</ul>
					</div>
					<div class="col-lg-2 col-md-3 single-footer-widget">
						<h4>Apoio</h4>
						<ul>
							<li><a href="https://decojovem.pt/" target="_blank">DECO Jovem</a></li>
							<li><a href="https://www.dns.pt/pt/" target="_blank">.pt</a></li>
						</ul>
					</div>
					<div class="col-lg-3 col-md-6 single-footer-widget">
						<h4>Segue-nos</h4>
						<div class="sharethis-inline-follow-buttons"></div>
					</div>
				</div>
				<div class="footer-bottom row align-items-center">
					<p class="footer-text m-0 col-lg-8 col-md-12"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <span class="lnr lnr-heart" style="color:white;"></span> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
					<div class="col-lg-4 col-md-12 footer-social">
						
					</div>
				</div>
			</div>
		</footer>
		<!-- End footer Area -->