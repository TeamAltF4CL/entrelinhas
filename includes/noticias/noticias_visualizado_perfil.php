<style>

	/*SELECT CSS*/
	.select_categoria {
	margin: 0;
	padding: 0;
	background-color: #004882;
	}

	.select_categoria .box {
	position: absolute;
	margin-bottom:25px;
	}

	.select_categoria .box select {
	background-color: #0563af;
	color: white;
	padding: 12px;
	width: 250px;
	border: none;
	box-shadow: 0 5px 25px rgba(0, 0, 0, 0.2);
	-webkit-appearance: button;
	appearance: button;
	outline: none;
	height:50px;
	border-radius: 5px 5px 5px 5px;
	}

	.select_categoria .box::before {
	content: "\f13a";
	font-family: FontAwesome;
	position: absolute;
	top: 0;
	right: 0;
	width: 20%;
	height: 100%;
	text-align: center;
	font-size: 28px;
	line-height: 45px;
	color: rgba(255, 255, 255, 0.5);
	background-color: rgba(255, 255, 255, 0.1);
	pointer-events: none;
	}

	.select_categoria .box:hover::before {
	color: rgba(255, 255, 255, 0.6);
	background-color: rgba(255, 255, 255, 0.2);
	}

	.select_categoria .box select option {
	padding: 30px;
	}



	/*SEARCH CSS*/

	@import url(https://fonts.googleapis.com/css?family=Open+Sans);

	pesquisar_categoria {
	background: #f2f2f2;
	font-family: 'Open Sans', sans-serif;
	}

	.pesquisar_categoria .search {
	width: 70%;
	position: relative;
	display: flex;
	float: right !important;
	margin-bottom: 25px;
	}

	.pesquisar_categoria .searchTerm {
	width: 100%;
	border: 3px solid #0563af;
	border-right: none;
	padding: 5px;
	height: 50px;
	border-radius: 5px 0 0 5px;
	outline: none;
	color: #9DBFAF;
	}

	.pesquisar_categoria .searchTerm:focus{
	color: #0563af;
	}

	.pesquisar_categoria .searchButton {
	width: 40px;
	height: 50px;
	border: 1px solid #0563af;
	background: #0563af;
	text-align: center;
	color: #fff;
	border-radius: 0 5px 5px 0;
	cursor: pointer;
	font-size: 20px;
	}

	/*Resize the wrap to see the search bar change!*/
	/* .pesquisar_categoria .wrap{
	width: 60%;
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
	} */






	
</style>
  
<?php

    error_reporting(0);
    
    include_once('includes/processos/proc_login.php');

	date_default_timezone_set('Europe/Lisbon');
	$fuso_horario = date_default_timezone_get();

	$data_corrente = date('Y/m/d H:i:s');	



	/* obter as categorias */
    $sql_c="SELECT * FROM categorias ORDER BY categoria";
    $result_c = mysqli_query($conn, $sql_c);

    	/* obter visualizadas */
		$sql_visualizado="SELECT * FROM visualizado WHERE visualizado = 1 and id_utilizador = '".$_SESSION['user_id']."' ORDER BY data_visualizada ";
        $result_visualizado = mysqli_query($conn, $sql_visualizado);
        
        $nr_linha_visualizado = mysqli_num_rows($result_visualizado);



?>

<div class="container-fluid">

<form class="Search" method="post">
<div class="row" id="custom-search-input" style="flex-wrap:nowrap;">


</div>	

</form>
</div>	


<!-- Start latest-post Area -->
<?php
							if($nr_linha_visualizado == 0){
								$mensagem_pra_0_resutlados = "Você ainda não tem <b>histórico</b>";
								$default = "Sem resultados"
						?>


			<div class="latest-post-wrap" id="result_para">
				<h4 class="cat-title"><?php echo $default;?> </h4>
				
				<br><br>
				
				<div class="col-lg-12 post-center">
						<p class="excert">
							<?php echo $mensagem_pra_0_resutlados ;?>
						</p>
				</div>
	

						<?php

								}else{ //Se encontrar linha_perfils na pesquisa
						?>


			<div class="latest-post-wrap" id="result_para">
				<h4 class="cat-title">Foram encontrados <b><?php echo $nr_linha_visualizado ;?> resultados no seu histórico de visualizações</b></h4>
				<?php

					//Pesquisa
				
						while($linha_visualizado = mysqli_fetch_array($result_visualizado, MYSQLI_ASSOC)){


                        /* obter as publicacoes visualizadas */
                        $sql_visualizado_publicacao="SELECT * FROM utilizadores,publicacoes,categorias WHERE publicacoes.dados=utilizadores.id and publicacoes.id_categoria=categorias.id_categoria  and publicacoes.id = '".$linha_visualizado['id_publicacao']."'";
                        $result_visualizado_publicacao = mysqli_query($conn, $sql_visualizado_publicacao);
                        
                        $linha_visualizado_publicacao = mysqli_fetch_array($result_visualizado_publicacao, MYSQLI_ASSOC);

                        /* obter as infs do user */
                        $sql_visualizado_user="SELECT * FROM utilizadores WHERE id = '".$linha_visualizado['id_utilizador']."' ";
                        $result_visualizado_user = mysqli_query($conn, $sql_visualizado_user);
                        
                        $linha_visualizado_user = mysqli_fetch_array($result_visualizado_user, MYSQLI_ASSOC);

				?>

				<div class="single-latest-post row align-items-center">
					<div class="col-lg-5 post-left">
					
						<div class="feature-img relative">
							<div class="overlay overlay-bg"></div>
							<a href="noticia.php?id=<?php echo $linha_visualizado['id_publicacao'];?>&titulo=<?php echo $linha_visualizado_publicacao['titulo'];?>"><img class="img-fluid" src="assets/img/noticias/<?php echo $linha_visualizado_publicacao['imgnome'];?>" alt=""></a>
						</div>
						<ul class="tags">
							<li><a href="perfil.php?categ=<?php echo $linha_visualizado_publicacao['id_categoria'];?>"><?php echo $linha_visualizado_publicacao['categoria'] ;?></a></li>
						</ul>
					</div>
					<div class="col-lg-7 post-right">
						<a href="noticia.php?id=<?php echo $linha_visualizado['id_publicacao'];?>&titulo=<?php echo $linha_visualizado_publicacao['titulo'];?>">
							<h4><?php echo $linha_visualizado_publicacao['titulo'] ;?></h4>
						</a>
						<ul class="meta">
							<li><a href="noticia.php?id=<?php echo $linha_visualizado['id_publicacao'];?>&titulo=<?php echo $linha_visualizado_publicacao['titulo'];?>"><span class="lnr lnr-user"></span><?php echo  $linha_visualizado_publicacao['nome'] ;?></a></li>
							<li><a href="noticia.php?id=<?php echo $linha_visualizado['id_publicacao'];?>&titulo=<?php echo $linha_visualizado_publicacao['titulo'];?>"><span class="lnr lnr-calendar-full"></span><?php $datanormal=$linha_visualizado_publicacao['data']; $dataformatada=date("d-m-Y H:i",strtotime($datanormal)); echo $dataformatada;?></a></li>
							<li><a href="noticia.php?id=<?php echo $linha_visualizado['id_publicacao'];?>&titulo=<?php echo $linha_visualizado_publicacao['titulo'];?>"><span class="lnr lnr-heart"></span><?php echo $linha_visualizado_publicacao['likes'] ;?> Likes</a></li>
						
							<?php

							//O user ja viu a publicacao?

							$visualizado_ou_nao = "SELECT * FROM visualizado WHERE id_publicacao = '".$linha_visualizado['id_publicacao']."' and id_utilizador= '".$linha_visualizado['id_utilizador']."' ";

							$result_visualizado_ou_nao = mysqli_query($conn, $visualizado_ou_nao);

							$nr_linha_visualizado = mysqli_num_rows($result_visualizado_ou_nao);

								if($nr_linha_visualizado == 0){

								}else{

							?>
							<li><a><span class='lnr lnr-checkmark-circle'></span>Já Visualizada</a></li>

							<?php
								}
							?>
						
						</ul>
						<p class="excert">
							<?php echo $linha_visualizado_publicacao['assunto'] ;?>
						</p>
					</div>
				</div>

					<?php
					
						}
					?>


				<?php

					}

					?>

			
			</div>
			<!-- End latest-post Area -->
