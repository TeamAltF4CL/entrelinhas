			<?php

			error_reporting(0);

			/*IMPORTANTE:

			Index das categorias (possiveis alterações, consultar a BD categorias pra mais inf):

			categoria 1: Todos
			categoria 2: Escola
			categoria 3: Inoformnática
			categoria 4: Desporto

			*/

			//declarar algumas coisas importantes

			date_default_timezone_set('Europe/Lisbon');
			$fuso_horario = date_default_timezone_get();

				$data_corrente = date('Y/m/d H:i:s');
			?>		
			
							<!-- Start relavent-story-post Area -->
							<div class="relavent-story-post-wrap mt-30">
								<h4 class="title">Notícias Relevantes</h4>

								<div class="relavent-story-list-wrap">

								<?php

									//Pesquisa
									$noticias_relevantes= "SELECT * FROM publicacoes,categorias WHERE publicacoes.id_categoria = categorias.id_categoria ORDER BY likes DESC LIMIT 4";
									$resultados_noticias_relevantes= mysqli_query($conn, $noticias_relevantes);
										while($linha8 = mysqli_fetch_array($resultados_noticias_relevantes, MYSQLI_ASSOC)){

											$user_infs_revelante="SELECT * From utilizadores WHERE id = '" .$linha8['dados']. "' ";

												$result_para_o_user_revelante = mysqli_query($conn, $user_infs_revelante);

												$user_revelante = mysqli_fetch_array($result_para_o_user_revelante, MYSQLI_ASSOC);

								?>

									<div class="single-relavent-post row align-items-center">
										<div class="col-lg-5 post-left">
											<div class="feature-img relative">
												<div class="overlay overlay-bg"></div>
												<a href="noticia.php?id=<?php echo $linha8['id'];?>&titulo=<?php echo $linha8['titulo'];?>"><img class="img-fluid" src="assets/img/noticias/<?php echo $linha8['imgnome'];?>" alt=""></a>
											</div>
											<ul class="tags">
												<li><a href="noticias.php?categ=<?php echo $linha8['id_categoria'];?>"><?php echo $linha8['categoria'] ;?></a></li>
											</ul>
										</div>
										<div class="col-lg-7 post-right">
										<a href="noticia.php?id=<?php echo $linha8['id'];?>&titulo=<?php echo $linha8['titulo'];?>">
												<h4><?php echo $linha8['titulo'] ;?></h4>
											</a>
											<ul class="meta">
												<li><a href="ver-perfil.php?id=<?php echo $user_revelante['id'];?>&nome=<?php echo $user_revelante['nome'];?>"><span class="lnr lnr-user"></span><?php echo $user_revelante['nome'] ;?></a></li>
												<li><a href="noticia.php?id=<?php echo $linha8['id'];?>&titulo=<?php echo $linha8['titulo'];?>"><span class="lnr lnr-calendar-full"></span><?php $datanormal=$linha8['data']; $dataformatada=date("d-m-Y H:i",strtotime($datanormal)); echo $dataformatada;?></a></li>
												<li><a href="noticia.php?id=<?php echo $linha8['id'];?>&titulo=<?php echo $linha8['titulo'];?>"><span class="lnr lnr-heart"></span><?php echo $linha8['likes'] ;?> Likes</a></li>
											
												<?php

												//O user ja viu a publicacao?

												$id_pub5 = $linha8['id'];
												$user_id_para_o_visualizado5 = $_SESSION['user_id'];

												$visualizado_ou_nao5 = "SELECT * FROM visualizado WHERE id_publicacao = $id_pub5 AND id_utilizador= $user_id_para_o_visualizado5 ";

												$result_visualizado_ou_nao5 = mysqli_query($conn, $visualizado_ou_nao5);

												$nr_linha_visualizado5 = mysqli_num_rows($result_visualizado_ou_nao5);

													if($nr_linha_visualizado5 == 0){

													}else{

												?>
												<li><a><span class='lnr lnr-checkmark-circle'></span>Já Visualizada</a></li>

												<?php
													}
												?>
											
											</ul>
											<p class="excert">
													<?php echo $linha8['assunto'] ;?>
											</p>
										</div>
									</div>
									<?php
										}
									?>


								</div>
							</div>
							<!-- End relavent-story-post Area -->