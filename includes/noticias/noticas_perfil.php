<style>

	/*SELECT CSS*/
	.select_categoria {
	margin: 0;
	padding: 0;
	background-color: #004882;
	}

	.select_categoria .box {
	position: absolute;
	margin-bottom:25px;
	}

	.select_categoria .box select {
	background-color: #0563af;
	color: white;
	padding: 12px;
	border: none;
	width: 250px;
	box-shadow: 0 5px 25px rgba(0, 0, 0, 0.2);
	-webkit-appearance: button;
	appearance: button;
	outline: none;
	height:50px;
	border-radius: 5px 5px 5px 5px;
	}

	.select_categoria .box::before {
	content: "\f13a";
	font-family: FontAwesome;
	position: absolute;
	top: 0;
	right: 0;
	width: 20%;
	height: 100%;
	text-align: center;
	font-size: 28px;
	line-height: 45px;
	color: rgba(255, 255, 255, 0.5);
	background-color: rgba(255, 255, 255, 0.1);
	pointer-events: none;
	}

	.select_categoria .box:hover::before {
	color: rgba(255, 255, 255, 0.6);
	background-color: rgba(255, 255, 255, 0.2);
	}

	.select_categoria .box select option {
	padding: 30px;
	}



	/*SEARCH CSS*/

	@import url(https://fonts.googleapis.com/css?family=Open+Sans);

	pesquisar_categoria {
	background: #f2f2f2;
	font-family: 'Open Sans', sans-serif;
	}

	.pesquisar_categoria .search {
	width: auto;
	position: relative;
	display: flex;
	float: right !important;
	margin-bottom: 25px;
	}

	.pesquisar_categoria .searchTerm {
	width: 70%;
	border: 3px solid #0563af;
	border-right: none;
	padding: 5px;
	height: 50px;
	border-radius: 5px 0 0 5px;
	outline: none;
	color: #9DBFAF;
	}

	.pesquisar_categoria .searchTerm:focus{
	color: #0563af;
	}

	.pesquisar_categoria .searchButton {
	width: 40px;
	height: 50px;
	border: 1px solid #0563af;
	background: #0563af;
	text-align: center;
	color: #fff;
	border-radius: 0 5px 5px 0;
	cursor: pointer;
	font-size: 20px;
	}

	/*Resize the wrap to see the search bar change!*/
	/* .pesquisar_categoria .wrap{
	width: 60%;
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
	} */
	
	
</style>
  
<?php

    error_reporting(0);
    
    include_once('includes/processos/proc_login.php');

	/*IMPORTANTE:

	Index das categorias (possiveis alterações, consultar a BD categorias pra mais inf):

	categoria 1: Todos
	categoria 2: Escola
	categoria 3: Inoformnática
	categoria 4: Desporto

	*/

	//declarar algumas coisas importantes

date_default_timezone_set('Europe/Lisbon');
$fuso_horario = date_default_timezone_get();

	$data_corrente = date('Y/m/d H:i:s');	



	/* obter as categorias */
    $sql_c="SELECT * FROM categorias ORDER BY categoria";
    $result_c = mysqli_query($conn, $sql_c);

    
			// fazer a procura
			if($_POST[bt_search]=='search')
			{
			$designacao_temp=utf8_decode($_POST[srch_term]);
			/* procura */
			}
			else{
			
			}
			if(isset($_GET[categ])){
				$id_categ=$_GET[categ];
                $noticias_recentes="SELECT * From publicacoes,categorias Where publicacoes.id_categoria=categorias.id_categoria and publicacoes.id_categoria='".$id_categ."' and publicacoes.titulo LIKE '%".$designacao_temp."%' and publicacoes.dados = '" .$_SESSION['user_id']. "' ORDER BY data DESC";
                $user_infs="SELECT * From utilizadores WHERE id = '" .$_SESSION['user_id']. "'";
			}
				else
			{
				/* consulta */
				$noticias_recentes="SELECT * From publicacoes,categorias Where publicacoes.id_categoria=categorias.id_categoria and publicacoes.titulo LIKE '%".$designacao_temp."%' and publicacoes.dados = '" .$_SESSION['user_id']. "' ORDER BY data DESC" ;
                $user_infs="SELECT * From utilizadores WHERE id = '" .$_SESSION['user_id']. "' ";
            }

			/* executar a consulta */
            $resultados_noticias_recentes = mysqli_query($conn, $noticias_recentes);
            $result_para_o_user = mysqli_query($conn, $user_infs);

            $user = mysqli_fetch_array($result_para_o_user, MYSQLI_ASSOC);

			/* contar o número de linha_perfils da comsulta */
			$nr_linha_perfils = mysqli_num_rows($resultados_noticias_recentes);
?>

<div class="container">

<form class="" method="post">
<div class="row" id="custom-search-input" style="flex-wrap:nowrap;">


<div class="col-sm-6">

<div class="select_categoria">

	<div class="box">

	<?php 
/*
	//Detetar que categoria selecionou
		if($_GET[categ] ==  2){
		 	$categoria_atual =  "Escola";
				}
				if($_GET[categ] ==  3){
					$categoria_atual = "Informatica";
							}
							if($_GET[categ] ==  4){
								$categoria_atual = "Desporto";
										}else{
											$categoria_atual = "Selecione uma categoria";
					}
	*/
		?>

		<select name="categ" onChange="javascript:location.href = this.value;">
		<option value="" >Selecione uma categoria</option>
		<option value="perfil.php" >Todas</option>
		<?php while($linha_perfil_c = mysqli_fetch_array($result_c, MYSQLI_ASSOC)){ ?>
		<option value= "perfil.php?categ=<?php echo $linha_perfil_c[id_categoria];?>" ><?php echo $linha_perfil_c[categoria];?>
		</option>
		<?php } ?>
		</select>
	</div>
</div>
</div>

<?php 
	//Detetar o que pesquisou
		if($designacao_temp ==  ""){
				$default_pesquisa = "";
						}else{
							$default_pesquisa = $designacao_temp;
							}
		?>

<div class="col-sm-6 pesquisar_categoria" style="padding-right:0;">
		<div class="search" style="margin-right: -40px;">
			<input type="text" class="searchTerm" name="srch_term" id="srch_term" placeholder="Pesquisar.." value="<?php echo $default_pesquisa;?>" pattern="[a-zA-Z0-9\sàáèéìíòóùú\?]+" title="Por favor não insira caracteres especiais!">
			<button class="searchButton" type="submit"  id="bt_search" name="bt_search" value="search" >		
			<i class="fa fa-search"></i>
			</button>
	</div>
</div>


</div>	

</form>
</div>	


	<?php 
	//Detetar em que categoria esta
		if($_GET[categ] == 2){
				$default = "Escola";
					}elseif($_GET[categ] == 3){
					$default = "Informatica";
						}elseif($_GET[categ] == 4){
						$default = "Desporto";
						}elseif($_GET[categ] == NULL){
							$default = "Todas as noticias";
							}
		?>

<!-- Start latest-post Area -->


				<?php
							if($nr_linha_perfils == 0){
								$mensagem_pra_0_resutlados = "Nao foram encontrados resultados para <b><i> '$designacao_temp' </i></b>";
								$default = "Sem resultados"
						?>

			<div class="latest-post-wrap" id="result_para">
				<h4 class="cat-title"><?php echo $default;?> </h4>
				
				<br><br>
				
				<div class="col-lg-12 post-center">
						<p class="excert">
							<?php echo $mensagem_pra_0_resutlados ;?>
						</p>
				</div>
	

						<?php

								}else{ //Se encontrar linha_perfils na pesquisa
						?>


			<div class="latest-post-wrap" id="result_para">
				<h4 class="cat-title">
					
				<?php 
					//Mostrar no titulo da form o que a pessoa pesquisou
					if($default == "Todas as noticias" AND $default_pesquisa = $designacao_temp){
							$default = "Resultados encontrados para <b><i> '$designacao_temp' </i></b>";
						}else{
							$default = $default;	
					}
						echo $default;?> </h4>
				<?php

					//Pesquisa
				
					$resultados_noticias_recentes= mysqli_query($conn, $noticias_recentes);
						while($linha_perfil = mysqli_fetch_array($resultados_noticias_recentes, MYSQLI_ASSOC)){


				?>

				<div class="single-latest-post row align-items-center">
					<div class="col-lg-5 post-left">
					
						<div class="feature-img relative">
							<div class="overlay overlay-bg"></div>
							<a href="noticia.php?id=<?php echo $linha_perfil['id'];?>&titulo=<?php echo $linha_perfil['titulo'];?>"><img class="img-fluid" src="assets/img/noticias/<?php echo $linha_perfil['imgnome'];?>" alt=""></a>
						</div>
						<ul class="tags">
							<li><a href="perfil.php?categ=<?php echo $linha_perfil['id_categoria'];?>"><?php echo $linha_perfil['categoria'] ;?></a></li>
						</ul>
					</div>
					<div class="col-lg-7 post-right">
						<a href="noticia.php?id=<?php echo $linha_perfil['id'];?>&titulo=<?php echo $linha_perfil['titulo'];?>">
							<h4><?php echo $linha_perfil['titulo'] ;?></h4>
						</a>
						<ul class="meta">
							<li><a href="noticia.php?id=<?php echo $linha_perfil['id'];?>&titulo=<?php echo $linha_perfil['titulo'];?>"><span class="lnr lnr-user"></span><?php echo $user['nome'] ;?></a></li>
							<li><a href="noticia.php?id=<?php echo $linha_perfil['id'];?>&titulo=<?php echo $linha_perfil['titulo'];?>"><span class="lnr lnr-calendar-full"></span><?php $datanormal=$linha_perfil['data']; $dataformatada=date("d-m-Y H:i",strtotime($datanormal)); echo $dataformatada;?></a></li>
							<li><a href="noticia.php?id=<?php echo $linha_perfil['id'];?>&titulo=<?php echo $linha_perfil['titulo'];?>"><span class="lnr lnr-heart"></span><?php echo $linha_perfil['likes'] ;?> Likes</a></li>
						
							<?php

						//O user ja viu a publicacao?

						$id_pub5 = $linha_perfil['id'];
						$user_id_para_o_visualizado5 = $_SESSION['user_id'];

						$visualizado_ou_nao5 = "SELECT * FROM visualizado WHERE id_publicacao = $id_pub5 AND id_utilizador= $user_id_para_o_visualizado5 ";

						$result_visualizado_ou_nao5 = mysqli_query($conn, $visualizado_ou_nao5);

						$nr_linha_visualizado5 = mysqli_num_rows($result_visualizado_ou_nao5);

							if($nr_linha_visualizado5 == 0){

							}else{

						?>
						<li><a><span class='lnr lnr-checkmark-circle'></span>Já Visualizada</a></li>

						<?php
							}
						?>
						
						</ul>
						<p class="excert">
							<?php echo $linha_perfil['assunto'] ;?>
						</p>
					</div>
				</div>

					<?php
					
						}
					?>

				

				<?php

					}

					?>

			
			</div>
			<!-- End latest-post Area -->
