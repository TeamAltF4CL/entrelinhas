<style>

@media (min-width:992px){
.img1responsive{
    height:450px;
}
.img2responsive{
    height:220px;
}
.img3responsive{
    height:220px;
}

}

@media (min-width:1200px){
.img1responsive{
    height:510px;
}
.img2responsive{
    height:250px;
}
.img3responsive{
    height:250px;
}

}





</style>

<?php

        error_reporting(0);
    
        /*IMPORTANTE:

        Index das categorias (possiveis alterações, consultar a BD categorias pra mais inf):

        categoria 1: Todos
        categoria 2: Escola
        categoria 3: Inoformnática
        categoria 4: Desporto

     */

         //declarar algumas coisas importantes

         date_default_timezone_set('Europe/Lisbon');
         $fuso_horario = date_default_timezone_get();
    
                $data_corrente = date('Y/m/d H:i:s');

?>

        <section class="top-post-area pt-10">
                        <div class="container no-padding">
                            <div class="row small-gutters">

                            <?php
                            //PESQUISA DAS NOTICIAS

                                  //Pesquisa
                                $noticias_importantes= "SELECT * FROM publicacoes,categorias WHERE publicacoes.id_categoria = categorias.id_categoria AND categoria='Escola' ORDER BY data DESC LIMIT 1";
                                $resultados_noticias_importantes= mysqli_query($conn, $noticias_importantes);
                                     $linha = mysqli_fetch_array($resultados_noticias_importantes, MYSQLI_ASSOC);
                               

                                             //1 noticia importante mais recente 
                                                if(strtotime($linha['data']) <= strtotime($data_corrente)){
                                                    $noticia_imp_recent = "SELECT * FROM publicacoes,categorias WHERE publicacoes.id = '" .$linha['id']. "' AND categoria='Escola' LIMIT 1";
                                                    $resultados_noticia_imp_recent= mysqli_query($conn, $noticia_imp_recent);
                                                   
                                         while($linha_final = mysqli_fetch_array($resultados_noticia_imp_recent, MYSQLI_ASSOC)){
                                                    
                                                //Algumas vars importantes a se passarem pra usar fora do ciclo While
                                                    $id_primeira_pub = $linha_final['id'];
                                                    $data_mais_recente_das_pub = $linha_final['data'];

                                                    $user_infs_imp="SELECT * From utilizadores WHERE id = '" .$linha_final['dados']. "' ";

                                                    $result_para_o_user_imp = mysqli_query($conn, $user_infs_imp);

                                                    $user_imp = mysqli_fetch_array($result_para_o_user_imp, MYSQLI_ASSOC);
                    ?>

                                <div class="col-lg-8 top-post-left">
                                    <div class="feature-image-thumb relative">
                                        <div class="overlay overlay-bg"></div>
                                        <a href="noticia.php?id=<?php echo $linha_final['id'];?>&titulo=<?php echo $linha_final['titulo'];?>"><img class="img-fluid img1responsive" src="assets/img/noticias/<?php echo $linha_final['imgnome'];?>" alt=""></a>
                                    </div>
                                    <div class="top-post-details">
                                        <ul class="tags">
                                            <li><a href="noticias.php?categ=<?php echo $linha_final['id_categoria'];?>"><?php echo $linha_final['categoria'] ;?></a></li>
                                        </ul>
                                        <a href="noticia.php?id=<?php echo $linha_final['id'];?>&titulo=<?php echo $linha_final['titulo'];?>">
                                            <h3><?php echo $linha['titulo'] ;?></h3>
                                        </a>
                                        <ul class="meta">
                                            <li><a href="ver-perfil.php?id=<?php echo $user_imp['id'];?>&nome=<?php echo $user_imp['nome'];?>"><span class="lnr lnr-user"></span><?php echo $user_imp['nome'] ;?></a></li>
                                            <li><a href="noticia.php?id=<?php echo $linha_final['id'];?>&titulo=<?php echo $linha_final['titulo'];?>"><span class="lnr lnr-calendar-full"></span><?php $datanormal=$linha_final['data']; $dataformatada=date("d-m-Y H:i",strtotime($datanormal)); echo $dataformatada;?></a></li>
                                            <li><a href="noticia.php?id=<?php echo $linha_final['id'];?>&titulo=<?php echo $linha_final['titulo'];?>"><span class="lnr lnr-heart"></span><?php echo $linha_final['likes'] ;?> Likes</a></li>
                                        </ul>
                                    </div>
                                </div>
                    
                                <?php
                                            } //fecha o cilo while

                                 /*termina o statment if e abre o else*/   }

                                  //2 noticia importante mais recente 

                                //Pesquisa 2
                                $noticias_importantes2= "SELECT * FROM publicacoes,categorias WHERE publicacoes.id_categoria = categorias.id_categoria AND categoria='Escola' AND publicacoes.id <> $id_primeira_pub ORDER BY data DESC LIMIT 1";
                                    $resultados_noticias_importantes2 = mysqli_query($conn, $noticias_importantes2);
                                         $linha2 = mysqli_fetch_array($resultados_noticias_importantes2, MYSQLI_ASSOC);

                                                        if(strtotime($linha2['data']) <= strtotime($data_mais_recente_das_pub)){
                                                            $noticia_imp_recent2 = "SELECT * FROM publicacoes,categorias WHERE publicacoes.id = '" .$linha2['id']. "' AND categoria='Escola' LIMIT 1";
                                                                $resultados_noticia_imp_recent2= mysqli_query($conn, $noticia_imp_recent2);
            
                                                     while($linha_final2 = mysqli_fetch_array($resultados_noticia_imp_recent2, MYSQLI_ASSOC)){

                                                   //Algumas vars importantes a se passarem pra usar fora do ciclo While
                                                    $id_segunda_pub = $linha_final2['id'];
                                                    $segunda_data_mais_recente_das_pub = $linha_final2['data'];

                                                    $user_infs_imp2="SELECT * From utilizadores WHERE id = '" .$linha_final2['dados']. "' ";

                                                    $result_para_o_user_imp2 = mysqli_query($conn, $user_infs_imp2);

                                                    $user_imp2 = mysqli_fetch_array($result_para_o_user_imp2, MYSQLI_ASSOC);
                                ?>
                               
                                <div class="col-lg-4 top-post-right">
                                    <div class="single-top-post">
                                        <div class="feature-image-thumb relative">
                                            <div class="overlay overlay-bg"></div>
                                            <a href="noticia.php?id=<?php echo $linha_final2['id'];?>&titulo=<?php echo $linha_final2['titulo'];?>"><img class="img-fluid img2responsive" src="assets/img/noticias/<?php echo $linha_final2['imgnome'] ;?>" alt=""></a>
                                        </div>
                                        <div class="top-post-details">
                                            <ul class="tags">
                                                <li><a href="noticias.php?categ=<?php echo $linha_final2['id_categoria'];?>"><?php echo $linha_final2['categoria'] ;?></a></li>
                                            </ul>
                                            <a href="noticia.php?id=<?php echo $linha_final2['id'];?>&titulo=<?php echo $linha_final2['titulo'];?>">
                                                <h4><?php echo $linha_final2['titulo'] ;?></h4>
                                            </a>
                                            <ul class="meta">
                                                <li><a href="ver-perfil.php?id=<?php echo $user_imp2['id'];?>&nome=<?php echo $user_imp2['nome'];?>"><span class="lnr lnr-user"></span><?php echo $user_imp2['nome'] ;?></a></li>
                                                <li><a href="noticia.php?id=<?php echo $linha_final2['id'];?>&titulo=<?php echo $linha_final2['titulo'];?>"><span class="lnr lnr-calendar-full"></span><?php $datanormal=$linha_final2['data']; $dataformatada=date("d-m-Y H:i",strtotime($datanormal)); echo $dataformatada;?></a></li>
                                                <li><a href="noticia.php?id=<?php echo $linha_final2['id'];?>&titulo=<?php echo $linha_final2['titulo'];?>"><span class="lnr lnr-heart"></span><?php echo $linha_final2['likes'] ;?> Likes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    
                                    <?php
                                            }//fecha o cilo while

                                                            }//termina o if statment

                                //3 noticia importante mais recente 

                                //Pesquisa 3
                                $noticias_importantes3= "SELECT * FROM publicacoes,categorias WHERE publicacoes.id_categoria = categorias.id_categoria AND categoria='Escola' AND publicacoes.id <> $id_primeira_pub AND publicacoes.id <> $id_segunda_pub ORDER BY data DESC LIMIT 1";
                                $resultados_noticias_importantes3 = mysqli_query($conn, $noticias_importantes3);
                                     $linha3 = mysqli_fetch_array($resultados_noticias_importantes3, MYSQLI_ASSOC);

                                                    if(strtotime($linha3['data']) <= strtotime($segunda_data_mais_recente_das_pub)){
                                                        $noticia_imp_recent3 = "SELECT * FROM publicacoes,categorias WHERE publicacoes.id = '" .$linha3['id']. "' AND categoria='Escola' LIMIT 1";
                                                            $resultados_noticia_imp_recent3= mysqli_query($conn, $noticia_imp_recent3);
        
                                                 while($linha_final3 = mysqli_fetch_array($resultados_noticia_imp_recent3, MYSQLI_ASSOC)){

                                                    $user_infs_imp3="SELECT * From utilizadores WHERE id = '" .$linha_final3['dados']. "' ";

                                                    $result_para_o_user_imp3 = mysqli_query($conn, $user_infs_imp3);

                                                    $user_imp3 = mysqli_fetch_array($result_para_o_user_imp3, MYSQLI_ASSOC);

                                    ?>
                                    <div class="single-top-post mt-10">
                                        <div class="feature-image-thumb relative">
                                            <div class="overlay overlay-bg"></div>
                                            <a href="noticia.php?id=<?php echo $linha_final3['id'];?>&titulo=<?php echo $linha_final3['titulo'];?>"><img class="img-fluid img3responsive" src="assets/img/noticias/<?php echo $linha_final3['imgnome'] ;?>" alt=""></a>
                                        </div>
                                        <div class="top-post-details">
                                            <ul class="tags">
                                                <li><a href="noticias.php?categ=<?php echo $linha_final3['id_categoria'];?>"><?php echo $linha_final3['categoria'] ;?></a></li>
                                            </ul>
                                            <a href="noticia.php?id=<?php echo $linha_final3['id'];?>&titulo=<?php echo $linha_final3['titulo'];?>">
                                                <h4><?php echo $linha_final3['titulo'] ;?></h4>
                                            </a>
                                            <ul class="meta">
                                                <li><a href="ver-perfil.php?id=<?php echo $user_imp3['id'];?>&nome=<?php echo $user_imp3['nome'];?>"><span class="lnr lnr-user"></span><?php echo $user_imp3['nome'] ;?></a></li>
                                                <li><a href="noticia.php?id=<?php echo $linha_final3['id'];?>&titulo=<?php echo $linha_final3['titulo'];?>"><span class="lnr lnr-calendar-full"></span><?php $datanormal=$linha_final3['data']; $dataformatada=date("d-m-Y H:i",strtotime($datanormal)); echo $dataformatada;?></a></li>
                                                <li><a href="noticia.php?id=<?php echo $linha_final3['id'];?>&titulo=<?php echo $linha_final3['titulo'];?>"><span class="lnr lnr-heart"></span><?php echo $linha_final3['likes'] ;?> Likes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    
                                <?php
                                                                        }//fecha o cilo while

                                                            }//termina o if statment   
                                ?>

                                </div>
                                                                    
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- End top-post Area -->