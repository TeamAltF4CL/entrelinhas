-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 10-Maio-2019 às 20:30
-- Versão do servidor: 10.1.40-MariaDB
-- versão do PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `entrelin_entrelinhas`
--
CREATE DATABASE IF NOT EXISTS `entrelin_entrelinhas` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `entrelin_entrelinhas`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

DROP TABLE IF EXISTS `categorias`;
CREATE TABLE IF NOT EXISTS `categorias` (
  `id_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(100) NOT NULL,
  PRIMARY KEY (`id_categoria`),
  KEY `categoria` (`categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id_categoria`, `categoria`) VALUES
(4, 'Desporto'),
(2, 'Escola'),
(3, 'Informática');

-- --------------------------------------------------------

--
-- Estrutura da tabela `comentarios`
--

DROP TABLE IF EXISTS `comentarios`;
CREATE TABLE IF NOT EXISTS `comentarios` (
  `id_comentario` int(11) NOT NULL AUTO_INCREMENT,
  `id_publicacao` int(11) NOT NULL,
  `id_utilizador` int(11) NOT NULL,
  `comentario` varchar(180) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `data_hora` datetime NOT NULL,
  `estado` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_comentario`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `comentarios`
--

INSERT INTO `comentarios` (`id_comentario`, `id_publicacao`, `id_utilizador`, `comentario`, `data_hora`, `estado`) VALUES
(3, 10, 38, 'Adoro o Sarau!', '2019-04-29 13:16:32', 1),
(20, 3, 63, 'O nosso colÃ©gio tem um ambiente muito acolhedor e familiar ', '2019-04-29 16:39:14', 1),
(28, 2, 67, 'Meu deus, que site incrÃ­vel', '2019-04-29 16:43:05', 1),
(35, 14, 27, 'Sem dÃºvida uma experiÃªncia muito enriquecedora!\r\n&Eacute; o 2Âº ano consecutivo que o faÃ§o e aconselho a todos esta experiÃªncia inexplicÃ¡vel. ', '2019-05-03 22:51:32', 1),
(36, 13, 10, 'Foi uma visita de estudo muito boa!', '2019-05-04 01:29:58', 1),
(37, 15, 17, 'NÃ£o escrevo', '2019-05-04 10:21:07', 0),
(38, 15, 28, 'Boa notÃ­cia! ', '2019-05-06 13:35:06', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `edicao`
--

DROP TABLE IF EXISTS `edicao`;
CREATE TABLE IF NOT EXISTS `edicao` (
  `id_edicao` int(11) NOT NULL AUTO_INCREMENT,
  `nome_edicao` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pdf_edicao` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_edicao` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_edicao`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `edicao`
--

INSERT INTO `edicao` (`id_edicao`, `nome_edicao`, `pdf_edicao`, `imagem_edicao`) VALUES
(1, 'Edição de janeiro de 2018', 'janeiro2018.pdf', 'capaentrelinhas_janeiro2018.jpg'),
(2, 'Edição de junho de 2018', 'junho2018.pdf', 'capaentrelinhas_junho2018.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `likes`
--

DROP TABLE IF EXISTS `likes`;
CREATE TABLE IF NOT EXISTS `likes` (
  `id_like` int(11) NOT NULL AUTO_INCREMENT,
  `id_publicacao` int(11) NOT NULL,
  `id_utilizador` int(11) NOT NULL,
  UNIQUE KEY `id_like` (`id_like`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `likes`
--

INSERT INTO `likes` (`id_like`, `id_publicacao`, `id_utilizador`) VALUES
(1, 2, 24),
(2, 3, 24),
(3, 10, 24),
(4, 2, 25),
(5, 2, 26),
(6, 3, 26),
(8, 2, 28),
(9, 2, 27),
(10, 3, 27),
(11, 10, 27),
(12, 2, 17),
(13, 1, 27),
(14, 10, 10),
(15, 3, 10),
(16, 10, 38),
(18, 2, 38),
(19, 1, 38),
(21, 2, 52),
(22, 10, 52),
(23, 1, 52),
(24, 3, 52),
(25, 2, 53),
(26, 3, 54),
(27, 3, 53),
(28, 10, 53),
(29, 10, 17),
(30, 1, 17),
(31, 2, 57),
(32, 2, 56),
(33, 2, 63),
(34, 2, 58),
(35, 2, 60),
(36, 3, 63),
(37, 2, 59),
(38, 2, 62),
(39, 2, 69),
(40, 2, 68),
(41, 3, 68),
(42, 10, 68),
(43, 2, 67),
(44, 1, 68),
(45, 2, 61),
(46, 2, 65),
(50, 1, 16),
(51, 10, 16),
(52, 3, 65),
(53, 10, 60),
(54, 3, 28),
(59, 2, 16),
(60, 2, 10),
(62, 2, 77),
(63, 11, 16),
(64, 12, 16),
(65, 13, 16),
(66, 13, 10),
(67, 14, 79),
(68, 14, 27),
(69, 13, 27),
(70, 15, 27),
(71, 15, 16),
(72, 15, 10),
(73, 14, 10),
(74, 12, 10),
(75, 11, 10),
(76, 15, 17),
(77, 15, 28);

-- --------------------------------------------------------

--
-- Estrutura da tabela `preferencias`
--

DROP TABLE IF EXISTS `preferencias`;
CREATE TABLE IF NOT EXISTS `preferencias` (
  `id_preferencia` int(11) NOT NULL AUTO_INCREMENT,
  `preferencia` varchar(100) NOT NULL,
  PRIMARY KEY (`id_preferencia`),
  KEY `preferencia` (`preferencia`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `preferencias`
--

INSERT INTO `preferencias` (`id_preferencia`, `preferencia`) VALUES
(1, 'Escolha do Editor'),
(2, 'Normal');

-- --------------------------------------------------------

--
-- Estrutura da tabela `publicacoes`
--

DROP TABLE IF EXISTS `publicacoes`;
CREATE TABLE IF NOT EXISTS `publicacoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(75) NOT NULL,
  `assunto` varchar(100) NOT NULL,
  `texto` text NOT NULL,
  `imgnome` varchar(100) NOT NULL,
  `imgnome2` varchar(500) NOT NULL,
  `imgnome3` varchar(500) NOT NULL,
  `dados` int(100) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `preferencia` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `likes` int(11) NOT NULL,
  `views` int(11) NOT NULL,
  `estado` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `categoria` (`id_categoria`),
  KEY `preferencia` (`preferencia`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `publicacoes`
--

INSERT INTO `publicacoes` (`id`, `titulo`, `assunto`, `texto`, `imgnome`, `imgnome2`, `imgnome3`, `dados`, `id_categoria`, `preferencia`, `data`, `likes`, `views`, `estado`) VALUES
(1, 'DECORAÇÕES DE NATAL', 'Natal', '<p>&nbsp;&nbsp;&nbsp; Para assinalar a &eacute;poca festiva de Natal, os nossos dedicados alunos, dos mais pequenos aos mais crescidos, participaram na execu&ccedil;&atilde;o das decora&ccedil;&otilde;es e dos enfeites que deram mais cor e brilho aos principais espa&ccedil;os de circula&ccedil;&atilde;o do Col&eacute;gio, lembrando a todos como &eacute; especial o m&ecirc;s de dezembro, m&ecirc;s da uni&atilde;o, da fraternidade e da partilha.</p><p>Os nossos alunos expuseram os trabalhos que realizaram nas disciplinas de Educa&ccedil;&atilde;o Visual, Educa&ccedil;&atilde;o Tecnol&oacute;gica e Oficina de Artes. Do 5&ordm; ao 9&ordm; ano, e com a importante interven&ccedil;&atilde;o da Maria Gomes, aluna do Curso de Artes, todos participaram nesta iniciativa. Deste modo, os diversos edif&iacute;cios da nossa escola foram invadidos pelo esp&iacute;rito natal&iacute;cio dos nossos atarefados duendes.</p>\r\nOs nossos alunos expuseram os trabalhos que realizaram nas disciplinas de Educação Visual, Educação Tecnológica e Oficina de Artes. Do 5º ao 9º ano, e com a importante intervenção da Maria Gomes, aluna do Curso de Artes, todos participaram nesta iniciativa. Deste modo, os diversos edifícios da nossa escola foram invadidos pelo espírito natalício dos nossos atarefados duendes.', 'decor_natal.jpg', 'decor_natal1.jpg', '', 79, 2, 2, '2019-03-26 08:43:00', 6, 131, 1),
(2, 'EVENTO PROMOVIDO PELA MICROSOFT', 'Skype', '<p>O Col&eacute;gio, nos &uacute;ltimos anos, tem feito um enorme investimento no sentido de levar a que os alunos, para al&eacute;m das aprendizagens essenciais mais convencionais, aprendam a ter uma atitude mais colaborativa e desenvolvam a autonomia e a criatividade, exercitando m&uacute;ltiplas dimens&otilde;es que ser&atilde;o fundamentais numa sociedade cada vez mais global.</p><p>Gra&ccedil;as ao car&aacute;ter inovador que define o seu projeto educativo, o Col&eacute;gio de Lamas foi a &uacute;nica escola portuguesa selecionada para participar numa sess&atilde;o interativa com Anthony Salcito, Vice-Presidente da Microsoft para a &aacute;rea da educa&ccedil;&atilde;o. Apenas 30 estabelecimentos de ensino em todo o mundo tiveram esta oportunidade &uacute;nica, o que confirma o privil&eacute;gio que constituiu esta participa&ccedil;&atilde;o. O convite surgiu no &acirc;mbito do &laquo;Microsoft Skype-a-Thon&raquo;, um evento educativo que convida professores e estudantes do mundo inteiro a envolverem-se numa aula aberta, via Skype, partilhando compet&ecirc;ncias adquiridas no desenvolvimento de projetos.</p><p>No dia 14 de novembro, logo ao in&iacute;cio da tarde, os nossos alunos abordaram temas relacionados com a utiliza&ccedil;&atilde;o da tecnologia em sala de aula e aprofundaram conhecimentos sobre as aplica&ccedil;&otilde;es Microsoft associadas &agrave; &aacute;rea da educa&ccedil;&atilde;o, sobre a gamifica&ccedil;&atilde;o (utiliza&ccedil;&atilde;o de t&eacute;cnicas e estrat&eacute;gias de jogos em outras atividades, nomeadamente no ensino) e ainda sobre aquilo que ser&aacute; o futuro dos atuais nativos digitais, a chamada gera&ccedil;&atilde;o Z.</p><p>A&nbsp;videochamada com Anthony Salcito, que envolveu os alunos do 5&ordm; ao 12&ordm; ano durante cerca de 40 minutos, contou com a media&ccedil;&atilde;o da professora Maria Jos&eacute; Oliveira e a monitoriza&ccedil;&atilde;o do professor Samuel Reis. No final, os nossos alunos mostraram-se muito entusiasmados com a possibilidade de terem dialogado com t&atilde;o importante personalidade mundial sobre assuntos que envolvem a tecnologia e a educa&ccedil;&atilde;o.</p><p>Este convite foi, sem d&uacute;vida, um reconhecimento da inova&ccedil;&atilde;o que define o projeto educativo do Col&eacute;gio.</p>\r\n\r\nGraças ao caráter inovador que define o seu projeto educativo, o Colégio de Lamas foi a única escola portuguesa selecionada para participar numa sessão interativa com Anthony Salcito, Vice-Presidente da Microsoft para a área da educação. Apenas 30 estabelecimentos de ensino em todo o mundo tiveram esta oportunidade única, o que confirma o privilégio que constituiu esta participação. O convite surgiu no âmbito do «Microsoft Skype-a-Thon», um evento educativo que convida professores e estudantes do mundo inteiro a envolverem-se numa aula aberta, via Skype, partilhando competências adquiridas no desenvolvimento de projetos.\r\n\r\nNo dia 14 de novembro, logo ao início da tarde, os nossos alunos abordaram temas relacionados com a utilização da tecnologia em sala de aula e aprofundaram conhecimentos sobre as aplicações Microsoft associadas à área da educação, sobre a gamificação (utilização de técnicas e estratégias de jogos em outras atividades, nomeadamente no ensino) e ainda sobre aquilo que será o futuro dos atuais nativos digitais, a chamada geração Z.\r\n\r\nA videochamada com Anthony Salcito, que envolveu os alunos do 5º ao 12º ano durante cerca de 40 minutos, contou com a mediação da professora Maria José Oliveira e a monitorização do professor Samuel Reis. No final, os nossos alunos mostraram-se muito entusiasmados com a possibilidade de terem dialogado com tão importante personalidade mundial sobre assuntos que envolvem a tecnologia e a educação.\r\n\r\nEste convite foi, sem dúvida, um reconhecimento da inovação que define o projeto educativo do Colégio. ', 'skype.jpg', '', '', 79, 3, 2, '2019-04-23 07:15:15', 24, 280, 1),
(3, 'O AMBIENTE CALOROSO DO DIA DE ACOLHIMENTO', 'Dia do Acolhimento', '<p>No dia 14 de setembro teve lugar o primeiro grande e significativo encontro dos alunos do Col&eacute;gio de Lamas. Este primeiro evento do ano foi marcado pela celebra&ccedil;&atilde;o dos 50 anos da nossa institui&ccedil;&atilde;o.</p><p>Ao som de m&uacute;sica ambiente, a rece&ccedil;&atilde;o aos alunos e encarregados de educa&ccedil;&atilde;o decorreu junto &agrave; portaria, no espa&ccedil;o exterior do edif&iacute;cio administrativo. Ap&oacute;s o per&iacute;odo de f&eacute;rias, foi enorme o entusiasmo de todos ao reverem-se para uma nova etapa de estudos. Essa foi tamb&eacute;m uma oportunidade para dar as boas-vindas aos novos alunos, que procuravam a melhor forma de se integrarem numa nova escola.</p><p>Sob a orienta&ccedil;&atilde;o dos Professores de EMRC/Interioridade, todos os presentes foram convidados a participar em duas tarefas distintas. A primeira relacionava-se com a assinatura de cada um numa t-shirt do uniforme do Col&eacute;gio, de modo a que todos se sentissem membros da mesma equipa. No fundo, cada um, ao vestir a mesma camisola, sentiria que &eacute; importante e que &eacute; membro ativo da comunidade que constitui o Col&eacute;gio de Lamas. A segunda era a escolha de uma pedra branca da praia que se encontrava dentro de um ba&uacute;, tendo em vista uma atividade que se iria realizar posteriormente.</p><p>De seguida, todos os alunos e professores foram convidados a deslocar-se para o campo central, para darem forma &agrave; constru&ccedil;&atilde;o do logotipo humano do Col&eacute;gio, na evoca&ccedil;&atilde;o dos seus 50 anos de vida ao servi&ccedil;o da educa&ccedil;&atilde;o. Nesse momento, marcando uma data t&atilde;o significativa, foram cantados os parab&eacute;ns &agrave; institui&ccedil;&atilde;o. Toda a atividade foi filmada por um drone, para a posterior edi&ccedil;&atilde;o de um v&iacute;deo. O resultado final foi sublime.</p><p>Depois, teve lugar uma sess&atilde;o de boas-vindas no Audit&oacute;rio Ant&oacute;nio Joaquim Vieira. Um dos pontos altos foi a inscri&ccedil;&atilde;o, nas pedras brancas anteriormente escolhidas, de uma palavra que expressasse um compromisso para o novo ano letivo. A pedra foi o s&iacute;mbolo eleito para evocar a educa&ccedil;&atilde;o como a tarefa de criar alicerces s&oacute;lidos e firmes para uma vida plena e com sentido, j&aacute; que todos somos pedras vivas na edifica&ccedil;&atilde;o do nosso Col&eacute;gio e da pr&oacute;pria sociedade.</p><p>De seguida, a can&ccedil;&atilde;o &laquo;Voar&raquo;, de Tim e Rui Veloso, desafiou os presentes a terem persist&ecirc;ncia e vontade de querer mais e melhor, na linha do lema do Col&eacute;gio &laquo;Semper Ascendens&raquo;. Depois, dois alunos leram a &laquo;Carta de Compromisso do Aluno do Col&eacute;gio de Lamas&raquo;, assumindo simbolicamente a vontade de todos os que iriam inicias as aulas.</p><p>Ap&oacute;s a visualiza&ccedil;&atilde;o de um v&iacute;deo que fazia a retrospetiva do ano letivo 2017/2018, a Diretora Pedag&oacute;gica do Col&eacute;gio, doutora Joana Vieira, tomou a palavra, saudando alunos e professores, e convidou a que todos se empenhassem na antecipa&ccedil;&atilde;o dessa escola do futuro que todos anseiam contruir. Num discurso pleno de significado, a doutora Joana ainda se comprometeu garantir as condi&ccedil;&otilde;es que concorressem para o sucesso pessoal e escolar de docentes, alunos e encarregados de educa&ccedil;&atilde;o que confiam no Col&eacute;gio de Lamas e no seu Projeto Educativo.</p><p>Posteriormente, a doutora Joana apresentou a equipa de assessores, que t&ecirc;m a responsabilidade de acompanhar a vida quotidiana dos alunos, e os diretores de turma, que t&ecirc;m a miss&atilde;o &iacute;mpar de garantir todas as condi&ccedil;&otilde;es pedag&oacute;gicas para o &ecirc;xito de todos.</p><p>Finalmente, as turmas acompanharam os diretores de turma &agrave;s respetivas salas de aulas para uma primeira conversa, seguindo-se um lanche na cantina.</p><p>Esta foi uma tarde memor&aacute;vel e rica em atividades significativas.</p>\r\n\r\nAo som de música ambiente, a receção aos alunos e encarregados de educação decorreu junto à portaria, no espaço exterior do edifício administrativo. Após o período de férias, foi enorme o entusiasmo de todos ao reverem-se para uma nova etapa de estudos. Essa foi também uma oportunidade para dar as boas-vindas aos novos alunos, que procuravam a melhor forma de se integrarem numa nova escola.\r\n\r\nSob a orientação dos Professores de EMRC/Interioridade, todos os presentes foram convidados a participar em duas tarefas distintas. A primeira relacionava-se com a assinatura de cada um numa t-shirt do uniforme do Colégio, de modo a que todos se sentissem membros da mesma equipa. No fundo, cada um, ao vestir a mesma camisola, sentiria que é importante e que é membro ativo da comunidade que constitui o Colégio de Lamas. A segunda era a escolha de uma pedra branca da praia que se encontrava dentro de um baú, tendo em vista uma atividade que se iria realizar posteriormente.\r\n\r\nDe seguida, todos os alunos e professores foram convidados a deslocar-se para o campo central, para darem forma à construção do logotipo humano do Colégio, na evocação dos seus 50 anos de vida ao serviço da educação. Nesse momento, marcando uma data tão significativa, foram cantados os parabéns à instituição. Toda a atividade foi filmada por um drone, para a posterior edição de um vídeo. O resultado final foi sublime.\r\n\r\nDepois, teve lugar uma sessão de boas-vindas no Auditório António Joaquim Vieira. Um dos pontos altos foi a inscrição, nas pedras brancas anteriormente escolhidas, de uma palavra que expressasse um compromisso para o novo ano letivo. A pedra foi o símbolo eleito para evocar a educação como a tarefa de criar alicerces sólidos e firmes para uma vida plena e com sentido, já que todos somos pedras vivas na edificação do nosso Colégio e da própria sociedade.\r\n\r\nDe seguida, a canção «Voar», de Tim e Rui Veloso, desafiou os presentes a terem persistência e vontade de querer mais e melhor, na linha do lema do Colégio «Semper Ascendens». Depois, dois alunos leram a «Carta de Compromisso do Aluno do Colégio de Lamas», assumindo simbolicamente a vontade de todos os que iriam inicias as aulas.\r\n\r\nApós a visualização de um vídeo que fazia a retrospetiva do ano letivo 2017/2018, a Diretora Pedagógica do Colégio, doutora Joana Vieira, tomou a palavra, saudando alunos e professores, e convidou a que todos se empenhassem na antecipação dessa escola do futuro que todos anseiam contruir. Num discurso pleno de significado, a doutora Joana ainda se comprometeu garantir as condições que concorressem para o sucesso pessoal e escolar de docentes, alunos e encarregados de educação que confiam no Colégio de Lamas e no seu Projeto Educativo.\r\n\r\nPosteriormente, a doutora Joana apresentou a equipa de assessores, que têm a responsabilidade de acompanhar a vida quotidiana dos alunos, e os diretores de turma, que têm a missão ímpar de garantir todas as condições pedagógicas para o êxito de todos.\r\n\r\nFinalmente, as turmas acompanharam os diretores de turma às respetivas salas de aulas para uma primeira conversa, seguindo-se um lanche na cantina.\r\n\r\nEsta foi uma tarde memorável e rica em atividades significativas.', 'acolhimento.jpg', '', '', 79, 2, 2, '2019-04-17 03:37:21', 11, 99, 1),
(10, 'A MAGIA DO SARAU DE NATAL', 'Natal', '<p>O sarau de Natal &eacute; uma festa que tem gerado sempre muita ansiedade e interesse em todos os que constituem a comunidade educativa do Col&eacute;gio, correspondendo ao resultado final de muitas horas de ensaios por arte de professores e de alunos. Este ano, que voltou a contar com algumas novidades, n&atilde;o foi exce&ccedil;&atilde;o.</p><p>O espet&aacute;culo ficou marcado por momentos de muita emo&ccedil;&atilde;o e despertou em todos o verdadeiro sentido do Natal. Perante um audit&oacute;rio repleto, os alunos dos diversos anos de escolaridade, concretamente desde o pr&eacute;-escolar ao 12&ordm; ano, apresentaram n&uacute;meros musicais, coreografias e pequenas dramatiza&ccedil;&otilde;es. Os momentos mais especiais e enternecedores foram protagonizados pelos alunos mais novos do nosso Col&eacute;gio, que, em apresenta&ccedil;&otilde;es marcada pela alegria, pela inoc&ecirc;ncia e pela espontaneidade, tiveram a capacidade de encantar e de impressionar todos aqueles que encheram o Grande Audit&oacute;rio.</p><p>A cor e a m&uacute;sica foram uma constante desta festa, deixando perceber o muito talento que existe nos nossos alunos.</p>\r\n\r\nO espetáculo ficou marcado por momentos de muita emoção e despertou em todos o verdadeiro sentido do Natal. Perante um auditório repleto, os alunos dos diversos anos de escolaridade, concretamente desde o pré-escolar ao 12º ano, apresentaram números musicais, coreografias e pequenas dramatizações. Os momentos mais especiais e enternecedores foram protagonizados pelos alunos mais novos do nosso Colégio, que, em apresentações marcada pela alegria, pela inocência e pela espontaneidade, tiveram a capacidade de encantar e de impressionar todos aqueles que encheram o Grande Auditório.\r\n\r\nA cor e a música foram uma constante desta festa, deixando perceber o muito talento que existe nos nossos alunos.', 'sarau.jpg', '', '', 79, 2, 2, '2019-04-01 09:08:00', 10, 75, 1),
(11, 'TECNOLOGIAS NA SALA DE AULA', 'Utilização do ipad e das aplicações associadas', '<p>Na sequ&ecirc;ncia da implementa&ccedil;&atilde;o de v&aacute;rias din&acirc;micas inovadoras e da parceria estabelecida com a <em>Apple Education,</em> o Col&eacute;gio de Lamas est&aacute; a desenvolver, em diferentes n&iacute;veis de ensino, um projeto de utiliza&ccedil;&atilde;o do <em>ipad</em> e das aplica&ccedil;&otilde;es espec&iacute;ficas para a &aacute;rea da educa&ccedil;&atilde;o, iniciando-se a sua implementa&ccedil;&atilde;o no primeiro ciclo e no 10&ordm; de escolaridade. Todos os alunos destes n&iacute;veis de ensino possuem um dispositivo que &eacute; utilizado em contexto de sala de aula e nas diferentes &aacute;reas disciplinares. Esta utiliza&ccedil;&atilde;o faz-se de acordo com as necessidades dos alunos em cada uma das disciplinas e a especificidade dos conte&uacute;dos abordados, sendo as aplica&ccedil;&otilde;es selecionadas pelos professores mais um recurso para o desenvolvimento das atividades.</p><p>O primeiro balan&ccedil;o &eacute; muito positivo. A utiliza&ccedil;&atilde;o <em>one to one</em> permite ao aluno um maior aproveitamento das potencialidades das aplica&ccedil;&otilde;es, ao n&iacute;vel da organiza&ccedil;&atilde;o dos conte&uacute;dos e tamb&eacute;m da estrutura&ccedil;&atilde;o dos seus pr&oacute;prios apontamentos. A utiliza&ccedil;&atilde;o do <em>ipad</em>, por exemplo, tem facilitado aos alunos a organiza&ccedil;&atilde;o das suas notas de aula no dispositivo, desenvolvendo esquemas e s&iacute;nteses dos conte&uacute;dos, realizando v&iacute;deos de atividades laboratoriais que poder&atilde;o consultar em qualquer altura e concretizando avalia&ccedil;&otilde;es, recorrendo a aplica&ccedil;&otilde;es desenvolvidas para esse efeito. Num tempo marcado pelo dom&iacute;nio do digital e da imagem, os alunos sentem-se muito mais motivados pela versatilidade e a qualidade deste tipo de equipamentos.</p><p>Este processo implica uma nova gest&atilde;o da din&acirc;mica de sala de aula, para a qual as ferramentas disponibilizadas pela <em>Apple Education</em> se t&ecirc;m revelado muito &uacute;teis, uma vez que o professor pode monitorizar em cada instante toda a atividade que &eacute; desenvolvida pelos alunos e gerir, de acordo com as necessidades, as aplica&ccedil;&otilde;es a disponibilizar aos alunos. Num pequeno inqu&eacute;rito relativo &agrave; utiliza&ccedil;&atilde;o do <em>ipad</em> e das aplica&ccedil;&otilde;es associadas, os alunos do 10&ordm; ano revelaram uma grande satisfa&ccedil;&atilde;o pelo modo como o processo est&aacute; a ser implementado e gerido, confirmando que o recurso &agrave; tecnologia na educa&ccedil;&atilde;o das novas gera&ccedil;&otilde;es &eacute; uma necessidade inadi&aacute;vel.</p>', 'sala_aula.jpg', '', '', 79, 3, 2, '2019-05-03 10:00:00', 2, 32, 1),
(12, 'REALIZAÇÃO DA FEIRA DO LIVRO NA BIBLIOTECA', 'Feira do Livro', '<p>A Feira do Livro de Natal, uma parceria entre o Col&eacute;gio e as editoras associadas ao grupo Leya, esteve patente na nossa biblioteca de 10 a 14 de dezembro.</p><p>A realiza&ccedil;&atilde;o deste evento, que fez da biblioteca o centro das aten&ccedil;&otilde;es do nosso Col&eacute;gio, teve como principais objetivos motivar os alunos para a leitura e divulgar autores portugueses e estrangeiros. &nbsp;Com uma &oacute;tima sele&ccedil;&atilde;o de livros para todas as idades e para todos os gostos, a feira apresentou tamb&eacute;m excelentes propostas para prendas de Natal.</p><p>Foram muitos os alunos que visitaram o espa&ccedil;o com o intuito de encontrar obras do seu agrado, o que mostrou que as novas gera&ccedil;&otilde;es ainda valorizam os livros e a leitura. Enquanto educadores, a recetividade e o entusiasmo dos alunos mostram que estamos a formar cidad&atilde;os cultos, cr&iacute;ticos e informados.</p><p>Dada a magia da feira do livro, este evento, para al&eacute;m da nossa comunidade escolar, tamb&eacute;m conseguiu conquistar os pais dos nossos alunos, o que comprova que o gosto pela leitura atravessa gera&ccedil;&otilde;es.</p>', 'feira_livro.png', '', '', 79, 2, 2, '2019-05-03 10:10:00', 2, 18, 1),
(13, 'CURSOS PROFISSIONAIS REALIZAM VISITA DE ESTUDO A LISBOA', 'LGW', '<p>No dia 16 de novembro, algumas turmas dos cursos profissionais de Inform&aacute;tica, de Informa&ccedil;&atilde;o e Anima&ccedil;&atilde;o Tur&iacute;stica e de Multim&eacute;dia efetuaram uma animada visita de estudo a Lisboa. Os alunos de Inform&aacute;tica e de Multim&eacute;dia deslocaram-se Feira Internacional de Lisboa, com o intuito de participarem na &laquo;Lisboa Games Week&raquo;, enquanto os de Informa&ccedil;&atilde;o e Anima&ccedil;&atilde;o Tur&iacute;stica fizeram uma interessante visita guiada ao Pal&aacute;cio Nacional da Ajuda e ao Mosteiro dos Jer&oacute;nimos.</p><p>Os dois autocarros que transportaram os alunos, ao chegarem &agrave; capital, seguiram caminhos diferentes, porque estava prevista a realiza&ccedil;&atilde;o das duas atividades em simult&acirc;neo.</p><p>N&oacute;s, alunos de Inform&aacute;tica, ap&oacute;s a entrada no evento, percorremos os muitos standes que ocupavam dois dos pavilh&otilde;es da Feira Internacional de Lisboa. Tivemos a oportunidade de experimentar as novidades e lan&ccedil;amentos das ind&uacute;strias de jogos e os equipamentos mais recentes do mercado. Ao longo da tarde, em pequenos grupos, convivemos com os nossos youtubers favoritos, compr&aacute;mos acess&oacute;rios e lembran&ccedil;as e tir&aacute;mos imensas fotografias. Com sa&iacute;da de Lisboa &agrave;s dezasseis horas, cheg&aacute;mos ao Col&eacute;gio j&aacute; por volta das oito.</p><p>&nbsp;Foi uma experi&ecirc;ncia que adorar&iacute;amos repetir, pois as atividades l&uacute;dicas e interativas nas quais particip&aacute;mos despertaram ainda mais o nosso interesse pelas &aacute;reas da programa&ccedil;&atilde;o, rob&oacute;tica e design de imagem, al&eacute;m de termos conhecido dispositivos de &uacute;ltima gera&ccedil;&atilde;o.</p>', 'LGW.JPG', '', '', 79, 3, 2, '2019-05-03 10:21:00', 3, 37, 1),
(14, 'CAMINHO DE SANTIAGO 2019 SEMPRE ASCENDENS', 'O Caminho da União', '<p>Pelo s&eacute;timo ano consecutivo, os professores de EMRC organizaram e levaram a cabo o projeto &laquo;Caminho de Santiago&raquo;. Este ano, de 24 a 28 de abril, um grupo de 32 alunos e 11 professores fizeram a p&eacute; os &uacute;ltimos 113 quil&oacute;metros do caminho franc&ecirc;s, desde Sarria at&eacute; Santiago de Compostela.</p><p>Foram cinco dias nos quais nem a chuva e granizo nem as dificuldades das subidas e descidas ensombraram a felicidade mais genu&iacute;na. Regress&aacute;mos com o cora&ccedil;&atilde;o cheio e, como se dizia dos peregrinos medievais, com &laquo;um canto nos l&aacute;bios e uma estrela na fronte&raquo;. Mitigadas as dores do corpo, sentimos que o caminho &eacute; a met&aacute;fora perfeita da vida, pois os obst&aacute;culos da exist&ecirc;ncia real e concreta s&atilde;o apenas desafios que, com entusiasmo e determina&ccedil;&atilde;o, s&atilde;o vencidos, rumo &agrave;s metas que nos propomos atingir.</p>', 'santiago.jpg', '', '', 79, 2, 2, '2019-05-03 18:08:00', 3, 79, 1),
(15, 'Semana Aberta de 5 a 10 de maio de 2019', 'Semana Aberta', '<p>Este domingo, 5 de maio de 2019, come&ccedil;a a nossa Semana Aberta.</p><p>&nbsp;</p><p>Programa da Semana Aberta:</p><p>&nbsp;</p><p><strong>5 de maio &ndash; domingo</strong></p><p>17:00</p><p>Celebra&ccedil;&atilde;o Eucar&iacute;stica em mem&oacute;ria do Dr. Ant&oacute;nio Joaquim Vieira, Fundador do Col&eacute;gio de Lamas&nbsp;</p><p>Igreja Matriz de Santa Maria de Lamas</p><p>&nbsp;</p><p><strong>6 de maio &ndash; 2&ordf; feira</strong></p><p>09:00 - 11:00</p><p>Decora&ccedil;&atilde;o dos Espa&ccedil;os - Educa&ccedil;&atilde;o Visual</p><p>&nbsp;</p><p>09:30 - 10:30</p><p>Hospital dos Bonecos do Pr&eacute;-escolar</p><p>Edif&iacute;cio II</p><p>&nbsp;</p><p>11:00 &ndash; 12:00</p><p>Mostra de Piano e Guitarra &ndash; Pr&eacute;-escolar e 1&ordm; ciclo</p><p>Edif&iacute;cio II</p><p>&nbsp;</p><p>11:00 - 12:45</p><p>Confer&ecirc;ncia: &Agrave; conversa com Antigos Alunos Not&aacute;veis&nbsp;</p><p>Audit&oacute;rio Dr. Joaquim Ant&oacute;nio Vieira</p><p>&nbsp;</p><p>14:00 - 17:30</p><p>Concurso Liter&aacute;rio</p><p>Salas de Aula</p><p>&nbsp;</p><p><strong>7 de maio &ndash; 3&ordf; feira</strong></p><p>09:30 - 11:30</p><p>&ldquo;Anna, Elsa e Olaf: A Aventura&rdquo; - Espet&aacute;culo infantil Pr&eacute;-Escolar, 1&ordm; e 2&ordm; ciclos</p><p>Audit&oacute;rio Dr. Joaquim Ant&oacute;nio Vieira</p><p>&nbsp;</p><p>14:30 - 15:10</p><p>&ldquo;Banda do Mickey&rdquo; pela Academia de M&uacute;sica de Pa&ccedil;os Brand&atilde;o&nbsp;</p><p>Audit&oacute;rio Dr. Joaquim Ant&oacute;nio Vieira</p><p>&nbsp;</p><p><strong>8 de maio &ndash; 4&ordf; feira</strong></p><p>09:00 - 11:30 &ndash; Mundo da Ci&ecirc;ncia - Pr&eacute;-escolar e 1&ordm; ciclo&nbsp;</p><p>Edif&iacute;cio II</p><p>&nbsp;</p><p>16:30 - 18:30 &ndash; Exposi&ccedil;&atilde;o de trabalhos - Pr&eacute;-escolar</p><p>Sal&atilde;o de Conv&iacute;vio</p><p>&nbsp;</p><p>16:30 - 18:30</p><p>Mostra de Inova&ccedil;&atilde;o: Apresenta&ccedil;&atilde;o dos projetos do 1&ordm; ciclo e Secund&aacute;rio</p><p>Sal&atilde;o de Conv&iacute;vio e Sala OTO</p><p>&nbsp;</p><p>18:30 - 19:15</p><p>Concerto pela Orquestra de Cordas - Academia de M&uacute;sica de Pa&ccedil;os de Brand&atilde;o, dire&ccedil;&atilde;o de H&eacute;lder Tavares&nbsp;</p><p>Audit&oacute;rio Dr. Joaquim Ant&oacute;nio Vieira</p><p>&nbsp;</p><p><strong>9 de maio &ndash; 5&ordf; feira&nbsp;</strong></p><p>09:00 - 16:00</p><p>Seguran&ccedil;a Rodovi&aacute;ria - Pr&eacute;-escolar&nbsp;</p><p>Campos de Jogos&nbsp;</p><p>&nbsp;</p><p>10:30 - 10:45</p><p>Desfile de Figuras Liter&aacute;rias</p><p>Espa&ccedil;o exterior</p><p>&nbsp;</p><p>16:30 - 18:30&nbsp;</p><p>Mostra de Inova&ccedil;&atilde;o: Apresenta&ccedil;&atilde;o dos projetos do 2&ordm; e 3&ordm; ciclos&nbsp;</p><p>Sal&atilde;o de conv&iacute;vio e Sala OTO</p><p>&nbsp;</p><p><strong>10 de maio &ndash; 6&ordf; feira&nbsp;</strong></p><p>09:00 - 09:30</p><p>Atua&ccedil;&atilde;o dos RITMARE e Grupo de Dan&ccedil;a Moderna</p><p>Pavilh&atilde;o Gimnodesportivo</p><p>&nbsp;</p><p>09:30 - 12:30</p><p>Torneio de Voleibol - 3&ordm; ciclo e Secund&aacute;rio</p><p>Pavilh&atilde;o Gimnodesportivo</p><p>&nbsp;</p><p>09:30 - 12:30</p><p>Torneio de T&eacute;nis de Mesa - todos os alunos</p><p>Pavilh&atilde;o gimnodesportivo</p><p>&nbsp;</p><p>09:30 - 12:30</p><p>Odisseia das L&iacute;nguas &ndash; Karaoke</p><p>Sal&atilde;o de conv&iacute;vio</p><p>&nbsp;</p><p>09:30 - 12:30</p><p>Ci&ecirc;ncia Divertida &ndash; Experi&ecirc;ncias de Biologia e F&iacute;sico-Qu&iacute;mica</p><p>Sal&atilde;o de Conv&iacute;vio</p><p>&nbsp;</p><p>09:30 - 12:30</p><p>Gincana de Jogos de Tabuleiro e de Matem&aacute;tica</p><p>Sal&atilde;o de Conv&iacute;vio</p><p>&nbsp;</p><p>09:30 - 12:30</p><p>Volta ao Mundo Virtual - Fotos e postais do mundo</p><p>Espa&ccedil;o Green Screen</p><p>&nbsp;</p><p>09:30 - 17:30</p><p>Roadshow Dragon Force - 1&ordm; e 2&ordm; ciclos</p><p>Relvado sint&eacute;tico</p><p>&nbsp;</p><p>09:30 - 17:30</p><p>Mesas gastron&oacute;micas do mundo</p><p>Espa&ccedil;o Exterior, junto ao bar</p><p>&nbsp;</p><p>09:30 - 17:30</p><p>Proje&ccedil;&atilde;o multim&eacute;dia Mem&oacute;rias do Col&eacute;gio&nbsp;</p><p>Espa&ccedil;o Green Screen</p><p>&nbsp;</p><p>09:30 - 10:15</p><p>Mexe-te - Aula de dan&ccedil;a - Pr&eacute;-escolar e 1&ordm; ciclo</p><p>Campos de Jogos</p><p>&nbsp;</p><p>10:30 - 12:30</p><p>Jogo do Sem&aacute;foro - Matem&aacute;tica</p><p>Campos de Jogos</p><p>&nbsp;</p><p>20:00</p><p>Jantar &ldquo;Amigos do Col&eacute;gio&rdquo;</p><p>Cantina</p>', 'entrelinhas5cccbcc133c648.27112135.jpg', '', '', 27, 2, 1, '2019-05-03 23:18:00', 5, 73, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pub_temp`
--

DROP TABLE IF EXISTS `pub_temp`;
CREATE TABLE IF NOT EXISTS `pub_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `assunto` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `img1` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `img2` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `img3` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `dados` int(11) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `utilizadores`
--

DROP TABLE IF EXISTS `utilizadores`;
CREATE TABLE IF NOT EXISTS `utilizadores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `email` varchar(100) NOT NULL,
  `data_nasc` date NOT NULL,
  `password` varchar(140) NOT NULL,
  `nome_imagem` varchar(50) NOT NULL,
  `likes_tot` int(11) NOT NULL,
  `views_tot` int(11) NOT NULL,
  `token` varchar(500) NOT NULL,
  `token2` varchar(500) NOT NULL,
  `estado` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `utilizadores`
--

INSERT INTO `utilizadores` (`id`, `nome`, `email`, `data_nasc`, `password`, `nome_imagem`, `likes_tot`, `views_tot`, `token`, `token2`, `estado`) VALUES
(10, 'André Reis', 'gato5andre26@gmail.com', '0000-00-00', '87e3ae8e7b1e864cf48f069b5b1062a0f0370b26ffa42e66e9dbff74d38d3d5e62a40ecae3302c916b2872af450d2f1cc3325efdd4d83ac5f6e05b42c8360e08', '1556209078.jpg', 0, 0, '', 'JwXCNYPs9d', 1),
(16, 'Bruno Fernandes', 'bdof2001@gmail.com', '2001-02-16', '87e3ae8e7b1e864cf48f069b5b1062a0f0370b26ffa42e66e9dbff74d38d3d5e62a40ecae3302c916b2872af450d2f1cc3325efdd4d83ac5f6e05b42c8360e08', '1556116632.jpg', 0, 0, '', '0', 1),
(17, 'João Oliveira', 'joaosilvajoaooliveira@hotmail.com', '2001-03-25', '87e3ae8e7b1e864cf48f069b5b1062a0f0370b26ffa42e66e9dbff74d38d3d5e62a40ecae3302c916b2872af450d2f1cc3325efdd4d83ac5f6e05b42c8360e08', '1556115891.jpg', 0, 0, '', '0', 1),
(24, 'Isaura Espinheira ', 'isauraespinheira@gmail.com', '0000-00-00', '74f33e8ccb0a73ed3b12f796a9c44603ef1d422e17280e0c6341d40e1ca9c6366339025a9f8cf4bd2de2989b69ce321c18c088b758947497a437c5a2ccb711e4', 'user.png', 0, 0, '', '0', 1),
(25, 'David Marques', 'davidmiguelbanana@gmail.com', '0000-00-00', 'd62769cd8d73e0fdd9bbf94cc56657b32119c3062a52326062252762349c266c3d7d9bf064c1bd014bc6a1719e4ead10e063cffd4f80ed6f58daae2a43b24d23', 'user.png', 0, 0, '', '0', 1),
(26, 'Tozé Gato', 'gatotoze50@googlemail.com', '0000-00-00', '9332ea5303d536665faf93f47d319a87fa4ee967ee7451e643c2e94693b36df03315b844144d1db1233e2ea222f8928ddd4e7b5ca4d37b0fd453cb82f0990b3c', '1556364519.jpg', 0, 0, '', '0', 1),
(27, 'Sandra Tavares', 'sandrapaulatavares@colegiodelamas.com', '0000-00-00', 'ec2b4dcab6aee9ea806f14c3a1a858184e0c3d41ebc9627e7aa05a21a64d826fa8a22c5241bafe57fa6abfb7c332068a8f3449768603d475e0a8ae5308c3f7e6', '1556523517.jpg', 5, 73, '', '0', 1),
(28, 'Reis', 'brunofreis5@hotmail.com', '0000-00-00', 'ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413', 'user.png', 0, 0, '', '0', 1),
(35, 'Rodrigo Silva ', 'a19423@colegiodelamas.com', '0000-00-00', 'a329f96328647a8aead18e953cd9d358f1ab4d2fee9ce0f42701554d918304f7053127017e1f3f918d541dc1edb70f40fd7bc63eb0f5f26fedf9cde0b32b7de1', 'user.png', 0, 0, '', '0', 1),
(34, 'Gabriel Costa', 'a19239@colegiodelamas.com', '0000-00-00', '5ab28bc2e794a0adc3ef80bde1032feeaaec711d1054e5005d13afbac2561d6652dc5fdf83fd0e946a15fb601cca5585a9788b94390ecb831edee49eaa4fe191', 'user.png', 0, 0, '', '0', 1),
(33, 'Luis', 'a19800@colegiodelamas.com', '0000-00-00', 'b8fd9f1e42ebe771ae8d97adf946c032ed0217c94b62dfeedbeefb453c2ed7b9b87f852dae0ec0710e2b0335aa41e9813e31647d66cfc45d26acb94a93083484', 'user.png', 0, 0, '', '0', 1),
(32, 'Sousa', 'sousa@ola.com', '0000-00-00', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2', 'user.png', 0, 0, '!PHnB/ZQbO', '0', 0),
(36, 'Abel Pinho', 'a19240@colegiodelamas.com', '0000-00-00', 'c874d0702eaa2a1969645de278e780407967c1a967614f8fcdb43f4343f3d6cb3355be49b1afbb71471566188559ea675997ea7e636d0faef7be7b9e8dc7e3a4', 'user.png', 0, 0, '37bca0fSnB', '0', 0),
(37, 'António Cordeiro', 'eduardocordeiro045@gmail.com', '0000-00-00', 'd17f1a5c6be51e2ebc9edd3c0876ddaac7ac2db9b793349da4287099e252db0376f28ad1f49cb630aa205620109399c2f55c1bec8e7e0e5d42b90f24ddd048c0', 'user.png', 0, 0, '', '0', 1),
(38, 'João Silva', 'a19336@colegiodelamas.com', '0000-00-00', '268b453e844441c3ec5bc148eba93293e556561c2cb410f62121f571d961582ff167cd6cb40100c2da912b36d9254d05cf2ab87e27aed7d69fc94ac6c5519119', 'user.png', 0, 0, '', '0', 1),
(39, 'Ivo', 'ivo.valente.2003@gmail.com', '0000-00-00', '666d7d6350f90acde44093109541116d1d1c8f5e00c42bdcc10eb6708d03855e22610291dfa56c9364c560ae989ad475a9646db1c46d0a1be1a737fa53c2938d', 'user.png', 0, 0, '', '0', 1),
(40, 'Joao', 'a19494@colegiodelamas.com', '0000-00-00', 'dfd62ab17d2c3266b65c2088f430ba295c8d3825d81c2a3931343d168c5c1d2dca7a61457f213e46728494b67057daa81f6f5f3596024ceb5e273107ae671a3d', 'user.png', 0, 0, 'Pvh(QwXBu*', '0', 0),
(41, 'Diogo Rocha', 'a19286@colegiodelamas.com', '0000-00-00', '92a798490686456624b137e24e75cbf33c78cc63b530e39562b9e43485bdd6ce1a267449adef287306b5fbfa8573cc54b58e7e106f8cafd042d1398a779a7d98', 'user.png', 0, 0, '', '0', 1),
(42, 'Gonçalo', 'a19281@colegiodelamas.com', '0000-00-00', '8234e804a67c8d77ab5ebfa398835fb8c8e58d45deac2294c3ac477a59d2423b9e348e71454661521f56821eb4aff707e87b5ec98b1167c18e523f92f4803e8e', 'user.png', 0, 0, '', '0', 1),
(43, 'Abel Pinho', 'pinho19240@gmail.com', '0000-00-00', 'c874d0702eaa2a1969645de278e780407967c1a967614f8fcdb43f4343f3d6cb3355be49b1afbb71471566188559ea675997ea7e636d0faef7be7b9e8dc7e3a4', 'user.png', 0, 0, '', '0', 1),
(44, 'Joel Ramalho', 'joelramalho17@gmail.com', '0000-00-00', 'e7802e9ea5b9086e29e48e0d1f3377bb854f913292df63c187d375e5e6e86063ffd27dc7c3b9b5603d109a520ec3193f900ed3fdd6096af0494e26ea2fbad0a5', 'user.png', 0, 0, '', '0', 1),
(45, 'Diogo Sá', 'a20615@colegiodelamas.com', '0000-00-00', '7724f096237b209986972abaecdfce849f8585eccb82665e8a2c3c47045b6101fa46849ad233e82d32180cb52ecd265263cfdc75f24451574ef25442ec22b815', 'user.png', 0, 0, '', '0', 1),
(46, 'Pedro silva', 'pedro_silva2000@colegiodelamas.com', '0000-00-00', '40ebd914b89bbefb2d6d098dfee0a19c999ad2ee0ab9cd428b29c16d182ed977354732f284a5279726e6eb090187e5a1d58b1da6caf02a4c65e8cecf36e10b03', '1556544001.jpg', 0, 0, '2IPjsixy69', '0', 0),
(47, 'Bruna silva ', 'a20084@colegiodelamas.com', '0000-00-00', '3d6c2fdf4c59e302d61a422a9cb8e832a12b40fff43ca693436b6f98f7aca55d1335441a1d8a601d921cbdbc4a3d7723de913dbac156d2bb79f1046a9d2889b4', 'user.png', 0, 0, '', '0', 1),
(48, 'Ângela Oliveira ', 'a18404@colegiodelamas.com', '0000-00-00', '3153401c99ca874df224c61a39c61f8d61463598ecfb124e183552f2c0fb744e27eedf2c74ddbc5cc73b1492d299083add90ce118c477120f226f40b366d0d35', 'user.png', 0, 0, 'mrDx0)ZouJ', '0', 0),
(49, 'Pedro ', 'pedro_silva2000@hotmail.com', '0000-00-00', '40ebd914b89bbefb2d6d098dfee0a19c999ad2ee0ab9cd428b29c16d182ed977354732f284a5279726e6eb090187e5a1d58b1da6caf02a4c65e8cecf36e10b03', 'user.png', 0, 0, '', '0', 1),
(50, 'Ana Fernandes', 'a18312@colegiodelamas.com', '0000-00-00', '1cc952d80d0315a974dc92b2425760e0a4299a967b79cd60b0ca2ac83cec1fd43aa8b3bc30de224407ede04f7235c172fb7ceee0a5df456f982f637293caf3d6', 'user.png', 0, 0, '', '0', 1),
(51, 'Alexandre ', 'a18801@colegiodelamas.com', '0000-00-00', '7a440d7346d78b5c4f1b14780239c1e1f483a23ef895a242f9e3d27d206e0529417da4506da2243d6153bf47e46133327361fce2d2448855922f2bba72155d44', 'user.png', 0, 0, 'bqdVSwW490', '0', 0),
(52, 'João rocha', 'jprocha98@gmail.com', '0000-00-00', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2', '1556544887.jpg', 0, 0, '', '0', 1),
(53, 'Filipa', 'filipafelix4@gmail.com', '0000-00-00', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2', 'user.png', 0, 0, '', '0', 1),
(54, '#grelo', 'jorge11magali11filipe@gmail.com', '0000-00-00', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2', '1556544806.jpg', 0, 0, '', '0', 1),
(55, 'Rodrigo Ribeiro', 'coisastroladas@gmail.com', '0000-00-00', '3dd28c5a23f780659d83dd99981e2dcb82bd4c4bdc8d97a7da50ae84c7a7229a6dc0ae8ae4748640a4cc07ccc2d55dbdc023a99b3ef72bc6ce49e30b84253dae', 'user.png', 0, 0, '', '0', 1),
(56, 'João Martins', 'joaop.martins175@gmail.com', '0000-00-00', '8b471553d236cac571218521fcf039513c5dc843563f965689472adc5b2add7d671fd764a8e4cd59b6f68ff77e1cc17c19507120dc47c782c05a3de9e959da42', 'user.png', 0, 0, '', '0', 1),
(57, 'Carlos Magalhães', 'a18553@colegiodelamas.com', '0000-00-00', 'dc29c1154de5a2dea9dfd528485ea8a064ef9a0d63fe2864feec7619d06da4d3377667520aa01fe1cbc3c56541fd6e45613ca2047a803ad121b26ff819570589', 'user.png', 0, 0, '', '0', 1),
(58, 'Gonçalo Silva', 'a20375@colegiodelamas.com', '0000-00-00', 'daca36432dcb77ec99e92736a68049edd2f381bac477fda64cbfbc226211bca620de3296c6745fe61b4aa1a8a5a6a3f4a6c62e55317c1d03adc8663200037ef0', 'user.png', 0, 0, '', '0', 1),
(59, 'Silva', 'a18677@colegiodelamas.com', '0000-00-00', '08d7f331cfa61877a63232d99d93daba6fc5c03b95941bedeeb12a52b7e0f1c4f6f3c5261ffa9fd278e9b80f2311fd6c65f7c00d8ce3b94c0fdef3ebe9f694a6', 'user.png', 0, 0, '', '', 1),
(60, 'leonardo amorim', 'leoy991@gmail.com', '0000-00-00', 'ae796a3ee5ec946b32823db64aa88b1ba3cb7aa586df73cae599bfc475cae91afb481c156227d985e12beb05e971cae7754f61a39e1b52cfe86f08a3edb73da1', 'user.png', 0, 0, '', '0', 1),
(61, 'Joana Ferreira', 'ferreirajoana39@gmail.com', '0000-00-00', '60bd58dd43ff4f67e805146ee3a83c4434d35bef658bab3fbea6c0dc4704b77a125765a9c0e0c5e054935ebc5a94c9afe415fc71239c31ed6ea2defa1cc6f76f', 'user.png', 0, 0, '', '0', 1),
(62, 'Roberto Santos', 'a18619@colegiodelamas.com', '0000-00-00', 'b5075cbed07eca7b93d3a86a27ccae78176789feb04bba7e5d4af3f44e67f0d59e6a1f2d4376096d22a32f8c8fbcf1c6fcb70e6bf107fe86f3170f455a1502cf', 'user.png', 0, 0, '', '0', 1),
(63, 'grilo', 'a19604@colegiodelamas.com', '0000-00-00', '8fd4f83e3b0f42967b14b9345bf8f202e8f066f68847b0ef79e01789d0d7ecf01dd7cbce509c4079b358bc3a374f7de0d0ab7550b10472bc8cd04ce9468326e7', 'user.png', 0, 0, '', '0', 1),
(64, 'Xavier', 'xfvmxavy@live.com.pt', '0000-00-00', 'd07feafe47a65ab16206583dc352ed7c56999f3a55a7d20c33f00545e2f51677e69b2da12409a2591ea8c9d25f9b106b4edfc50a299ae244d9e6c4ed4dc214d9', 'user.png', 0, 0, '', '0', 1),
(65, 'Leandro Campelo', 'a20364@colegiodelamas.com', '0000-00-00', '22b2c699caecfaa560eab74f767ea68f9bdcece0c43fd22ad95864df110f15f5aaa1d6020d78a2e8409e86cc4f517692ed4ff3eba651e065123678f1f4ad1d3e', '1556552261.jpg', 0, 0, '', '0', 1),
(66, 'Jorge Rafael Almeida Pinho', 'a20372@colegiodelamas.com', '0000-00-00', '8e39ab6718b4c7a2c2da06e8dc5eab67e94b6c95e8893d41e1672f80bdab4ddca40d75368c4fff0ef46493ed470ff8dc862f61955460873ce29d7c8935e5d4ec', '1556552531.jpg', 0, 0, '', '0', 1),
(67, 'Vítor Hugo', 'a18562@colegiodelamas.com', '2002-12-27', '26bb0c32a9534d7bcc68fe3a7d8b0754b718329601ef384214c18b62fbab2b0b1f89080bb8bfd3b46239df62e49278e5bdc31ebe5b7a3adbdcde401206c09aa0', '1556552329.jpg', 0, 0, '', '0', 1),
(68, 'Diogo Alves', 'a18549@colegiodelamas.com', '0000-00-00', '2aeb901841e882ab5dda7d0721cef6b3138890d285108b4fab483e5c3b420b9ca29de7e74781ede41e4cba530b053c233adff10fbc6dc9514ae132058b203b27', 'user.png', 0, 0, '', '0', 1),
(69, 'Rui   Rocha', 'a20358@colegiodelamas.com', '0000-00-00', '9feec08ffc91cf489c4fa0d2aeb760e098b8e6a2addb7f30ccbef562e461734a03094394e3682216eec9aa1c8a19426b5da356115c85c1179aa83b4f1aad3bc3', 'user.png', 0, 0, '', '0', 1),
(70, 'Daniel Rodrigues', 'danielitopichi@gmail.com', '0000-00-00', 'b5143654f819bcf8833b12fe8c20937a593153dc93ad5127e155797087c094ef7226f3c3f874cf84eb0d03742ff776e6cbf0cb1dad48e6010e642b1759eb6a63', 'user.png', 0, 0, '', '', 1),
(71, 'onão', 'a18614@colegiodelamas.com', '1500-07-02', '6a6aba4798d918483ec0a5ea37f99196dd0ff7fc20faf15394d17449fb926d88827ae1b1a888f70ab4cee3905625e1b0b9d9b82eba8b48210450aa53f8342464', '1556613569.jpg', 0, 0, '', '0', 1),
(72, 'Diogo', 'dguedes20@hotmail.com', '2000-01-01', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2', 'user.png', 0, 0, '', '0', 1),
(75, 'Gil Almeida', 'gilalmeida11@hotmail.com', '0000-00-00', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2', 'user.png', 0, 0, '', '', 1),
(76, 'Bruno', 'bruno.santos@screen-in.pt', '0000-00-00', '493b55b25a799bec5eb35f33db4fdd41ee5b6dceb708136e6e9a3140843cab7ae3627fcee76c027799226fa0043da723b01ed1a744c8bddaf2b0171b52f39541', 'user.png', 0, 0, 'fNO*)cx0mq', '', 0),
(77, 'Bruno', 'brunosantos.dev00@gmail.com', '0000-00-00', '493b55b25a799bec5eb35f33db4fdd41ee5b6dceb708136e6e9a3140843cab7ae3627fcee76c027799226fa0043da723b01ed1a744c8bddaf2b0171b52f39541', 'user.png', 0, 0, '', '', 1),
(79, 'ENTRElinhas', 'teamaltf4cl@gmail.com', '0000-00-00', '87e3ae8e7b1e864cf48f069b5b1062a0f0370b26ffa42e66e9dbff74d38d3d5e62a40ecae3302c916b2872af450d2f1cc3325efdd4d83ac5f6e05b42c8360e08', '1556884221.jpg', 61, 751, '', '', 1),
(80, 'Carolina Moreira', 'carolmoreira1805@gmail.com', '0000-00-00', '966ced782910fc99ab9b3536ee9d21295c10e2ae49356a1db5cb71ddd112294fcdb981c21960a57c47ee4c06050579acfb3b050218be9eb329001ca9eb05e708', 'user.png', 0, 0, 'K4L3tdxgJe', '', 0),
(85, 'teste', 'teste@teste.teste', '0000-00-00', 'b123e9e19d217169b981a61188920f9d28638709a5132201684d792b9264271b7f09157ed4321b1c097f7a4abecfc0977d40a7ee599c845883bd1074ca23c4af', 'user.png', 0, 0, '3eWaxJfrX2', '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `visualizado`
--

DROP TABLE IF EXISTS `visualizado`;
CREATE TABLE IF NOT EXISTS `visualizado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_publicacao` int(11) NOT NULL,
  `id_utilizador` int(11) NOT NULL,
  `data_visualizada` datetime NOT NULL,
  `visualizado` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=383 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `visualizado`
--

INSERT INTO `visualizado` (`id`, `id_publicacao`, `id_utilizador`, `data_visualizada`, `visualizado`) VALUES
(1, 10, 10, '2019-04-27 03:30:35', 1),
(2, 3, 10, '2019-04-27 03:34:52', 1),
(3, 10, 0, '2019-04-27 10:03:15', 1),
(4, 10, 0, '2019-04-27 10:03:23', 1),
(5, 2, 24, '2019-04-27 10:04:50', 1),
(6, 3, 24, '2019-04-27 10:05:12', 1),
(7, 10, 24, '2019-04-27 10:05:58', 1),
(8, 3, 0, '2019-04-27 11:29:16', 1),
(9, 10, 0, '2019-04-27 11:29:51', 1),
(10, 2, 25, '2019-04-27 11:43:35', 1),
(11, 2, 10, '2019-04-27 11:44:16', 1),
(12, 2, 26, '2019-04-27 12:30:16', 1),
(13, 3, 26, '2019-04-27 12:31:56', 1),
(14, 10, 0, '2019-04-27 21:09:54', 1),
(15, 10, 0, '2019-04-27 21:10:07', 1),
(16, 1, 0, '2019-04-27 22:57:57', 1),
(17, 1, 0, '2019-04-27 22:58:10', 1),
(18, 1, 0, '2019-04-28 08:24:56', 1),
(19, 3, 0, '2019-04-28 13:04:03', 1),
(20, 2, 0, '2019-04-28 13:04:53', 1),
(21, 1, 10, '2019-04-28 18:26:49', 1),
(22, 2, 28, '2019-04-29 08:32:57', 1),
(23, 2, 27, '2019-04-29 08:33:56', 1),
(24, 3, 27, '2019-04-29 08:34:45', 1),
(25, 10, 27, '2019-04-29 08:39:08', 1),
(26, 2, 17, '2019-04-29 08:46:35', 1),
(27, 1, 17, '2019-04-29 08:47:43', 1),
(28, 1, 27, '2019-04-29 09:10:43', 1),
(29, 3, 0, '2019-04-29 09:58:59', 1),
(30, 3, 0, '2019-04-29 10:46:51', 1),
(31, 1, 0, '2019-04-29 12:21:09', 1),
(32, 1, 0, '2019-04-29 13:09:57', 1),
(33, 1, 0, '2019-04-29 13:11:36', 1),
(34, 10, 38, '2019-04-29 13:16:17', 1),
(35, 2, 35, '2019-04-29 13:16:28', 1),
(36, 2, 38, '2019-04-29 13:17:10', 1),
(37, 1, 38, '2019-04-29 13:18:02', 1),
(38, 2, 37, '2019-04-29 13:25:12', 1),
(39, 1, 37, '2019-04-29 13:28:11', 1),
(40, 2, 0, '2019-04-29 13:30:16', 1),
(41, 1, 0, '2019-04-29 13:45:17', 1),
(42, 10, 54, '2019-04-29 14:36:40', 1),
(43, 3, 54, '2019-04-29 14:36:43', 1),
(44, 2, 52, '2019-04-29 14:37:36', 1),
(45, 10, 52, '2019-04-29 14:37:55', 1),
(46, 1, 52, '2019-04-29 14:38:01', 1),
(47, 2, 53, '2019-04-29 14:38:11', 1),
(48, 3, 52, '2019-04-29 14:38:11', 1),
(49, 3, 53, '2019-04-29 14:39:05', 1),
(50, 10, 53, '2019-04-29 14:39:38', 1),
(51, 10, 17, '2019-04-29 14:42:10', 1),
(52, 2, 0, '2019-04-29 14:43:22', 1),
(53, 1, 0, '2019-04-29 15:15:14', 1),
(54, 2, 57, '2019-04-29 16:37:26', 1),
(55, 2, 63, '2019-04-29 16:37:29', 1),
(56, 2, 60, '2019-04-29 16:37:31', 1),
(57, 2, 56, '2019-04-29 16:37:32', 1),
(58, 10, 59, '2019-04-29 16:37:37', 1),
(59, 2, 58, '2019-04-29 16:37:59', 1),
(60, 3, 63, '2019-04-29 16:38:29', 1),
(61, 2, 59, '2019-04-29 16:38:42', 1),
(62, 2, 62, '2019-04-29 16:39:06', 1),
(63, 2, 65, '2019-04-29 16:41:32', 1),
(64, 2, 69, '2019-04-29 16:41:41', 1),
(65, 2, 68, '2019-04-29 16:41:43', 1),
(66, 3, 68, '2019-04-29 16:42:08', 1),
(67, 2, 67, '2019-04-29 16:42:24', 1),
(68, 10, 68, '2019-04-29 16:42:39', 1),
(69, 1, 68, '2019-04-29 16:43:08', 1),
(70, 2, 61, '2019-04-29 16:43:10', 1),
(71, 2, 70, '2019-04-29 16:43:55', 1),
(72, 2, 71, '2019-04-29 16:45:45', 1),
(73, 1, 0, '2019-04-29 20:45:23', 1),
(74, 1, 0, '2019-04-29 20:45:44', 1),
(75, 1, 0, '2019-04-29 20:52:39', 1),
(76, 2, 0, '2019-04-29 22:33:28', 1),
(77, 1, 0, '2019-04-29 23:02:21', 1),
(78, 3, 0, '2019-04-29 23:02:48', 1),
(79, 2, 0, '2019-04-30 06:45:41', 1),
(80, 2, 75, '2019-04-30 08:39:42', 1),
(81, 2, 16, '2019-04-30 08:53:03', 1),
(82, 3, 16, '2019-04-30 08:53:42', 1),
(83, 1, 16, '2019-04-30 08:54:42', 1),
(84, 10, 16, '2019-04-30 08:57:25', 1),
(85, 3, 65, '2019-04-30 09:37:21', 1),
(86, 10, 60, '2019-04-30 09:37:25', 1),
(87, 10, 65, '2019-04-30 09:37:55', 1),
(88, 3, 67, '2019-04-30 09:49:44', 1),
(89, 2, 0, '2019-04-30 13:14:13', 1),
(90, 2, 0, '2019-04-30 13:16:05', 1),
(91, 2, 0, '2019-04-30 13:26:43', 1),
(92, 2, 0, '2019-04-30 13:27:12', 1),
(93, 2, 0, '2019-04-30 13:27:12', 1),
(94, 2, 0, '2019-04-30 13:27:13', 1),
(95, 3, 0, '2019-04-30 15:28:54', 1),
(96, 1, 0, '2019-04-30 17:31:33', 1),
(97, 1, 0, '2019-04-30 17:34:56', 1),
(98, 1, 0, '2019-04-30 20:31:34', 1),
(99, 2, 0, '2019-04-30 20:33:23', 1),
(100, 2, 0, '2019-04-30 20:34:53', 1),
(101, 3, 28, '2019-04-30 20:36:49', 1),
(102, 2, 0, '2019-04-30 22:30:49', 1),
(103, 2, 0, '2019-04-30 23:00:00', 1),
(104, 2, 0, '2019-04-30 23:00:05', 1),
(105, 2, 0, '2019-04-30 23:02:12', 1),
(106, 2, 0, '2019-04-30 23:03:00', 1),
(107, 3, 0, '2019-04-30 23:03:04', 1),
(108, 10, 0, '2019-04-30 23:03:08', 1),
(109, 1, 0, '2019-04-30 23:03:11', 1),
(110, 2, 0, '2019-04-30 23:03:15', 1),
(111, 3, 0, '2019-04-30 23:03:25', 1),
(112, 10, 0, '2019-04-30 23:03:29', 1),
(113, 1, 0, '2019-04-30 23:03:33', 1),
(114, 1, 0, '2019-04-30 23:04:43', 1),
(115, 10, 0, '2019-04-30 23:04:47', 1),
(116, 1, 0, '2019-04-30 23:04:51', 1),
(117, 10, 0, '2019-04-30 23:04:54', 1),
(118, 3, 0, '2019-04-30 23:04:57', 1),
(119, 1, 0, '2019-04-30 23:05:03', 1),
(120, 10, 0, '2019-04-30 23:05:06', 1),
(121, 1, 0, '2019-04-30 23:05:18', 1),
(122, 10, 0, '2019-04-30 23:05:20', 1),
(123, 3, 0, '2019-04-30 23:05:23', 1),
(124, 2, 0, '2019-04-30 23:05:27', 1),
(125, 1, 0, '2019-04-30 23:05:49', 1),
(126, 1, 0, '2019-04-30 23:06:08', 1),
(127, 2, 0, '2019-04-30 23:06:11', 1),
(128, 3, 0, '2019-04-30 23:06:26', 1),
(129, 2, 0, '2019-04-30 23:06:35', 1),
(130, 3, 0, '2019-04-30 23:06:40', 1),
(131, 10, 0, '2019-04-30 23:06:46', 1),
(132, 2, 0, '2019-04-30 23:07:13', 1),
(133, 2, 0, '2019-04-30 23:10:36', 1),
(134, 2, 0, '2019-04-30 23:10:38', 1),
(135, 2, 0, '2019-04-30 23:10:39', 1),
(136, 3, 0, '2019-05-01 01:47:10', 1),
(137, 1, 0, '2019-05-01 08:49:23', 1),
(138, 1, 0, '2019-05-01 10:09:34', 1),
(139, 1, 0, '2019-05-01 10:10:02', 1),
(140, 1, 0, '2019-05-01 10:10:58', 1),
(141, 10, 0, '2019-05-01 10:11:38', 1),
(142, 1, 0, '2019-05-01 10:12:08', 1),
(143, 10, 0, '2019-05-01 10:13:08', 1),
(144, 2, 0, '2019-05-01 12:32:13', 1),
(145, 2, 0, '2019-05-01 16:16:15', 1),
(146, 1, 0, '2019-05-01 16:17:52', 1),
(147, 2, 0, '2019-05-01 19:11:59', 1),
(148, 2, 0, '2019-05-02 00:15:09', 1),
(149, 2, 0, '2019-05-02 08:08:20', 1),
(150, 3, 0, '2019-05-02 08:34:49', 1),
(151, 10, 0, '2019-05-02 11:56:57', 1),
(152, 2, 0, '2019-05-02 12:10:56', 1),
(153, 3, 0, '2019-05-02 12:15:21', 1),
(154, 2, 0, '2019-05-02 12:37:03', 1),
(155, 2, 77, '2019-05-02 13:24:02', 1),
(156, 3, 77, '2019-05-02 13:26:15', 1),
(157, 10, 77, '2019-05-02 13:27:20', 1),
(158, 2, 0, '2019-05-02 14:03:53', 1),
(159, 2, 0, '2019-05-02 22:54:28', 1),
(160, 2, 0, '2019-05-02 22:54:28', 1),
(161, 2, 0, '2019-05-02 23:31:20', 1),
(162, 2, 0, '2019-05-02 23:31:21', 1),
(163, 1, 0, '2019-05-02 23:32:05', 1),
(164, 1, 0, '2019-05-02 23:32:05', 1),
(165, 2, 0, '2019-05-02 23:38:31', 1),
(166, 2, 0, '2019-05-02 23:38:31', 1),
(167, 2, 0, '2019-05-02 23:41:14', 1),
(168, 2, 0, '2019-05-02 23:41:15', 1),
(169, 2, 0, '2019-05-03 09:53:18', 1),
(170, 2, 0, '2019-05-03 09:53:19', 1),
(171, 10, 0, '2019-05-03 10:00:31', 1),
(172, 10, 0, '2019-05-03 10:00:31', 1),
(173, 3, 0, '2019-05-03 10:29:18', 1),
(174, 3, 0, '2019-05-03 10:29:19', 1),
(175, 3, 0, '2019-05-03 10:29:41', 1),
(176, 3, 0, '2019-05-03 10:29:41', 1),
(177, 2, 0, '2019-05-03 10:29:58', 1),
(178, 2, 0, '2019-05-03 10:29:58', 1),
(179, 2, 0, '2019-05-03 10:30:09', 1),
(180, 2, 0, '2019-05-03 10:30:09', 1),
(181, 2, 0, '2019-05-03 10:30:12', 1),
(182, 2, 0, '2019-05-03 10:30:12', 1),
(183, 2, 0, '2019-05-03 10:30:18', 1),
(184, 2, 0, '2019-05-03 10:30:18', 1),
(185, 2, 0, '2019-05-03 10:30:31', 1),
(186, 2, 0, '2019-05-03 10:30:31', 1),
(187, 2, 0, '2019-05-03 10:30:41', 1),
(188, 2, 0, '2019-05-03 10:30:42', 1),
(189, 1, 0, '2019-05-03 10:33:40', 1),
(190, 1, 0, '2019-05-03 10:33:40', 1),
(191, 11, 16, '2019-05-03 10:37:34', 1),
(192, 11, 0, '2019-05-03 10:44:24', 1),
(193, 11, 0, '2019-05-03 10:44:24', 1),
(194, 3, 0, '2019-05-03 10:45:11', 1),
(195, 3, 0, '2019-05-03 10:45:11', 1),
(196, 11, 0, '2019-05-03 10:45:30', 1),
(197, 11, 0, '2019-05-03 10:45:30', 1),
(198, 1, 0, '2019-05-03 10:47:40', 1),
(199, 1, 0, '2019-05-03 10:47:40', 1),
(200, 2, 0, '2019-05-03 11:36:40', 1),
(201, 2, 0, '2019-05-03 11:37:14', 1),
(202, 2, 0, '2019-05-03 11:37:39', 1),
(203, 11, 0, '2019-05-03 12:08:51', 1),
(204, 3, 0, '2019-05-03 12:26:08', 1),
(205, 3, 0, '2019-05-03 12:31:22', 1),
(206, 11, 0, '2019-05-03 12:31:43', 1),
(207, 2, 0, '2019-05-03 12:45:15', 1),
(208, 3, 0, '2019-05-03 12:45:25', 1),
(209, 3, 0, '2019-05-03 12:59:00', 1),
(210, 11, 0, '2019-05-03 12:59:05', 1),
(211, 2, 0, '2019-05-03 12:59:13', 1),
(212, 3, 0, '2019-05-03 12:59:16', 1),
(213, 10, 0, '2019-05-03 12:59:20', 1),
(214, 1, 0, '2019-05-03 12:59:24', 1),
(215, 11, 0, '2019-05-03 13:07:42', 1),
(216, 11, 0, '2019-05-03 13:08:34', 1),
(217, 11, 0, '2019-05-03 13:08:42', 1),
(218, 2, 0, '2019-05-03 14:24:44', 1),
(219, 2, 0, '2019-05-03 14:26:12', 1),
(220, 1, 0, '2019-05-03 14:26:15', 1),
(221, 11, 0, '2019-05-03 14:27:04', 1),
(222, 3, 0, '2019-05-03 14:27:18', 1),
(223, 11, 0, '2019-05-03 14:28:29', 1),
(224, 11, 0, '2019-05-03 14:28:36', 1),
(225, 11, 0, '2019-05-03 14:28:48', 1),
(226, 3, 0, '2019-05-03 14:28:58', 1),
(227, 2, 0, '2019-05-03 14:29:09', 1),
(228, 12, 16, '2019-05-03 14:42:25', 1),
(229, 12, 10, '2019-05-03 14:45:03', 1),
(230, 13, 16, '2019-05-03 15:00:33', 1),
(231, 13, 10, '2019-05-03 15:04:24', 1),
(232, 2, 0, '2019-05-03 15:26:33', 1),
(233, 2, 0, '2019-05-03 15:33:50', 1),
(234, 13, 0, '2019-05-03 15:49:26', 1),
(235, 13, 0, '2019-05-03 15:52:32', 1),
(236, 13, 0, '2019-05-03 15:57:23', 1),
(237, 3, 0, '2019-05-03 16:32:56', 1),
(238, 11, 0, '2019-05-03 16:33:13', 1),
(239, 13, 0, '2019-05-03 17:04:40', 1),
(240, 1, 0, '2019-05-03 17:06:05', 1),
(241, 14, 79, '2019-05-03 18:12:07', 1),
(242, 14, 0, '2019-05-03 18:12:20', 1),
(243, 14, 0, '2019-05-03 18:12:20', 1),
(244, 14, 0, '2019-05-03 18:12:22', 1),
(245, 14, 0, '2019-05-03 18:12:25', 1),
(246, 14, 0, '2019-05-03 18:12:32', 1),
(247, 14, 0, '2019-05-03 18:20:50', 1),
(248, 12, 79, '2019-05-03 18:24:35', 1),
(249, 14, 0, '2019-05-03 18:25:58', 1),
(250, 14, 0, '2019-05-03 18:26:07', 1),
(251, 13, 0, '2019-05-03 18:26:10', 1),
(252, 14, 0, '2019-05-03 18:28:14', 1),
(253, 1, 0, '2019-05-03 19:08:11', 1),
(254, 13, 0, '2019-05-03 19:37:51', 1),
(255, 14, 0, '2019-05-03 20:58:18', 1),
(256, 14, 0, '2019-05-03 20:58:58', 1),
(257, 14, 0, '2019-05-03 20:59:11', 1),
(258, 14, 0, '2019-05-03 20:59:16', 1),
(259, 14, 0, '2019-05-03 20:59:22', 1),
(260, 14, 0, '2019-05-03 20:59:33', 1),
(261, 14, 0, '2019-05-03 21:15:54', 1),
(262, 14, 0, '2019-05-03 22:25:42', 1),
(263, 13, 0, '2019-05-03 22:25:56', 1),
(264, 14, 0, '2019-05-03 22:29:03', 1),
(265, 14, 0, '2019-05-03 22:29:42', 1),
(266, 14, 27, '2019-05-03 22:46:30', 1),
(267, 14, 0, '2019-05-03 22:50:11', 1),
(268, 14, 0, '2019-05-03 22:50:56', 1),
(269, 14, 0, '2019-05-03 22:52:05', 1),
(270, 14, 0, '2019-05-03 22:53:53', 1),
(271, 14, 0, '2019-05-03 22:54:34', 1),
(272, 14, 0, '2019-05-03 23:05:45', 1),
(273, 13, 27, '2019-05-03 23:15:13', 1),
(274, 14, 0, '2019-05-03 23:18:18', 1),
(275, 15, 0, '2019-05-03 23:18:21', 1),
(276, 15, 0, '2019-05-03 23:18:53', 1),
(277, 14, 0, '2019-05-03 23:19:08', 1),
(278, 15, 0, '2019-05-03 23:19:12', 1),
(279, 15, 27, '2019-05-03 23:19:48', 1),
(280, 15, 16, '2019-05-03 23:21:01', 1),
(281, 10, 0, '2019-05-03 23:35:39', 1),
(282, 15, 0, '2019-05-04 00:51:49', 1),
(283, 14, 0, '2019-05-04 00:53:28', 1),
(284, 14, 0, '2019-05-04 00:53:49', 1),
(285, 14, 0, '2019-05-04 00:54:03', 1),
(286, 14, 0, '2019-05-04 00:54:10', 1),
(287, 15, 0, '2019-05-04 00:56:19', 1),
(288, 15, 0, '2019-05-04 00:56:19', 1),
(289, 15, 0, '2019-05-04 00:56:19', 1),
(290, 3, 0, '2019-05-04 01:00:51', 1),
(291, 15, 0, '2019-05-04 01:28:50', 1),
(292, 15, 10, '2019-05-04 01:29:21', 1),
(293, 14, 10, '2019-05-04 01:29:34', 1),
(294, 11, 10, '2019-05-04 01:30:13', 1),
(295, 14, 0, '2019-05-04 02:11:40', 1),
(296, 15, 0, '2019-05-04 07:40:36', 1),
(297, 15, 0, '2019-05-04 10:20:07', 1),
(298, 15, 17, '2019-05-04 10:20:45', 1),
(299, 12, 17, '2019-05-04 10:21:39', 1),
(300, 13, 0, '2019-05-04 11:33:31', 1),
(301, 15, 0, '2019-05-04 12:14:58', 1),
(302, 15, 0, '2019-05-04 12:35:50', 1),
(303, 2, 0, '2019-05-04 13:37:25', 1),
(304, 10, 0, '2019-05-04 13:38:54', 1),
(305, 14, 0, '2019-05-04 13:42:39', 1),
(306, 13, 0, '2019-05-04 13:44:59', 1),
(307, 3, 0, '2019-05-04 13:45:15', 1),
(308, 11, 0, '2019-05-04 14:53:54', 1),
(309, 1, 0, '2019-05-04 16:28:34', 1),
(310, 13, 0, '2019-05-04 16:50:39', 1),
(311, 15, 0, '2019-05-04 17:55:03', 1),
(312, 15, 0, '2019-05-04 18:05:02', 1),
(313, 15, 0, '2019-05-04 18:39:59', 1),
(314, 14, 0, '2019-05-04 19:18:58', 1),
(315, 13, 0, '2019-05-04 19:56:49', 1),
(316, 15, 0, '2019-05-04 20:04:10', 1),
(317, 15, 0, '2019-05-04 21:36:43', 1),
(318, 1, 0, '2019-05-04 21:36:50', 1),
(319, 10, 0, '2019-05-04 21:37:26', 1),
(320, 3, 0, '2019-05-04 21:37:30', 1),
(321, 10, 0, '2019-05-04 21:37:37', 1),
(322, 3, 0, '2019-05-04 21:37:41', 1),
(323, 15, 0, '2019-05-04 21:38:43', 1),
(324, 15, 0, '2019-05-04 22:31:43', 1),
(325, 15, 0, '2019-05-04 23:15:40', 1),
(326, 1, 0, '2019-05-05 10:37:15', 1),
(327, 10, 0, '2019-05-05 12:57:05', 1),
(328, 2, 0, '2019-05-05 17:19:52', 1),
(329, 12, 0, '2019-05-05 19:46:18', 1),
(330, 14, 0, '2019-05-05 19:48:31', 1),
(331, 3, 0, '2019-05-05 21:47:29', 1),
(332, 15, 0, '2019-05-05 21:48:49', 1),
(333, 15, 0, '2019-05-05 21:48:59', 1),
(334, 15, 0, '2019-05-05 21:49:00', 1),
(335, 15, 0, '2019-05-05 21:49:01', 1),
(336, 15, 0, '2019-05-05 21:49:08', 1),
(337, 15, 0, '2019-05-05 21:49:44', 1),
(338, 15, 0, '2019-05-05 22:24:13', 1),
(339, 15, 0, '2019-05-05 22:24:26', 1),
(340, 15, 0, '2019-05-06 01:30:43', 1),
(341, 15, 0, '2019-05-06 01:31:06', 1),
(342, 15, 0, '2019-05-06 01:31:07', 1),
(343, 15, 0, '2019-05-06 01:31:23', 1),
(344, 15, 0, '2019-05-06 01:31:48', 1),
(345, 15, 0, '2019-05-06 01:31:55', 1),
(346, 15, 0, '2019-05-06 01:33:31', 1),
(347, 12, 0, '2019-05-06 07:46:28', 1),
(348, 15, 0, '2019-05-06 13:32:11', 1),
(349, 12, 0, '2019-05-06 13:32:22', 1),
(350, 11, 0, '2019-05-06 13:32:30', 1),
(351, 13, 0, '2019-05-06 13:32:45', 1),
(352, 15, 0, '2019-05-06 13:32:54', 1),
(353, 1, 0, '2019-05-06 13:32:57', 1),
(354, 3, 0, '2019-05-06 13:33:53', 1),
(355, 15, 28, '2019-05-06 13:34:34', 1),
(356, 15, 0, '2019-05-06 13:35:11', 1),
(357, 15, 0, '2019-05-06 13:35:12', 1),
(358, 15, 0, '2019-05-06 14:17:06', 1),
(359, 15, 0, '2019-05-06 18:54:57', 1),
(360, 10, 0, '2019-05-07 01:14:53', 1),
(361, 1, 0, '2019-05-07 04:14:45', 1),
(362, 2, 0, '2019-05-07 04:24:13', 1),
(363, 14, 0, '2019-05-07 04:33:41', 1),
(364, 3, 0, '2019-05-07 04:52:37', 1),
(365, 15, 0, '2019-05-07 05:11:33', 1),
(366, 12, 0, '2019-05-07 12:36:30', 1),
(367, 12, 84, '2019-05-07 23:24:17', 1),
(368, 10, 0, '2019-05-07 23:34:37', 1),
(369, 13, 0, '2019-05-08 09:16:36', 1),
(370, 15, 0, '2019-05-08 11:58:02', 1),
(371, 15, 0, '2019-05-08 12:22:32', 1),
(372, 14, 0, '2019-05-09 13:42:29', 1),
(373, 1, 0, '2019-05-09 15:11:51', 1),
(374, 1, 0, '2019-05-09 15:12:07', 1),
(375, 15, 0, '2019-05-09 16:59:14', 1),
(376, 15, 0, '2019-05-09 18:21:32', 1),
(377, 11, 0, '2019-05-10 04:49:24', 1),
(378, 3, 0, '2019-05-10 16:32:04', 1),
(379, 14, 0, '2019-05-10 20:17:30', 1),
(380, 15, 0, '2019-05-10 20:18:07', 1),
(381, 14, 0, '2019-05-10 20:18:19', 1),
(382, 15, 0, '2019-05-10 20:18:26', 1);

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `publicacoes`
--
ALTER TABLE `publicacoes`
  ADD CONSTRAINT `publicacoes_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id_categoria`),
  ADD CONSTRAINT `publicacoes_ibfk_2` FOREIGN KEY (`preferencia`) REFERENCES `preferencias` (`id_preferencia`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
