<!DOCTYPE html>
<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="assets/img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>ENTRElinhas | Sobre</title>
		<link rel="icon" href="assets/img/logo_escola.png" type="image/png">
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="assets/css/linearicons.css">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/bootstrap.css">
		<link rel="stylesheet" href="assets/css/magnific-popup.css">
		<link rel="stylesheet" href="assets/css/nice-select.css">
		<link rel="stylesheet" href="assets/css/animate.min.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.css">
		<link rel="stylesheet" href="assets/css/jquery-ui.css">
		<link rel="stylesheet" href="assets/css/main.css">
		<link rel="stylesheet" href="assets/css/team.css">

		<style>
			.gray{
				color: #a5a5a5;
			}

			.icon{
				display:flex !important;
				justify-content:center !important;
				align-items:center !important;
				padding:7px;

			}

</style>

	</head>
	<body>
		
	<?php 
	require 'includes/navbar.php'; 
	?>
		
		<div class="site-main-container">
				<!-- Start top-post Area -->
				<section class="top-post-area pt-10">
				<div class="container no-padding">
					<div class="row">
						<div class="col-lg-12">
							<div class="hero-nav-area" style="background-image: url('https://www.way2webworld.com/images/aboutbanner.jpg') !important;">
								<h1 class="text-white">Sobre nós</h1>
								<p class="text-white link-nav"><a href="index.php">Início </a>  <span class="lnr lnr-arrow-right"></span><a href="sobre.php">Sobre nós </a></p>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End top-post Area -->
		</div>
		<!-- Start service Area -->
			<br><br>
		<!-- End service Area -->
		
		<!-- Start info Area -->
		<section class="info-area section-gap relative text-center">
			<div class="container">
				<div class="row align-items-center justify-content-end">
					<div class="col-lg-6 no-padding info-right">
						<h1>
						Quem somos <br>
						AltF4
						</h1>
						<p>
						Somos uma equipa composta por 3 elementos e a coordenadora Prof.Sandra Tavares.
					Estamos a participar no concurso sitestar promovido pela Decojovem. 
					O nosso site é o jornal Entrelinhas em formato digital.
						</p>
						<div class="row no-gutters text-center">
							<div class="single-services col">
								<span class="lnr lnr-laptop"></span>
								<a href="#">
									<h4>Curso</h4>
								</a>
								<p>
								Gestão e Programação de Sistemas Informáticos
								</p>
							</div>
							<div class="single-services col">
								<span class="lnr lnr-book"></span>
								<a href="#">
									<h4>Escola</h4>
								</a>
								<p>
								Colégio de Laamas
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End info Area -->
		<br><br>
		<!--Equipa-->
		<section class="team-area" id="team" >
         <div class="container">
            <div class="row section-title" >
               <div class="col-md-12 text-center">
                  <h2>Nossa Equipa</h2>
                  <p>
                  <h4 class="gray">Conheca os criadores do ENTRElinhas digital!</h4>
               </div>
            </div>
            <div class="row">
            <div class="col-md-3">
                  <div class="single-team">
                  <img src="assets/img/equipa/andre reis.jpg" alt="" class="img-responsive">
                     <div class="team-hover">
                        <div class="team-content">
                           <ul>
                              <li><a href="https://www.facebook.com/andre.gato.5" target="#blank"><i class="icon fa fa-facebook"></i></a></li>
                              <li><a href="https://twitter.com/gato5andre26" target="#blank"><i class="icon fa fa-twitter"></i></a></li>
                              <li><a href="https://www.instagram.com/andre_gato26" target="#blank"><i class="icon fa fa-instagram"></i></a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="teamtext">
                  <h4>André Reis</h4>
					  <p>Master Developer</p>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="single-team">
                  <img src="assets/img/equipa/bruno fernandes.jpg" alt="" class="img-responsive">
                     <div class="team-hover">
                        <div class="team-content">
                           <ul>
                              <li><a href="https://www.facebook.com/bdof2001" target="#blank"><i class="icon fa fa-facebook"></i></a></li>
                              <li><a href="https://twitter.com/bdof2001" target="#blank"><i class="icon fa fa-twitter"></i></a></li>
										<li><a href="https://www.instagram.com/bdof2001" target="#blank"><i class="icon fa fa-instagram"></i></a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="teamtext">
                        <h4>Bruno Fernandes</h4>
                        <p>Developer / Web Designer</p>
                </div>
               </div>
               <div class="col-md-3">
                  <div class="single-team">
                  <img src="assets/img/equipa/joao oliveira.jpg" alt="" class="img-responsive">
                     <div class="team-hover">
                        <div class="team-content">
                           
                           <ul>
                              <li><a href="https://www.facebook.com/profile.php?id=100003721791144" target="#blank"><i class="icon fa fa-facebook"></i></a></li>
                              <li><a href="https://twitter.com/Joao_s_o" target="#blank"><i class="icon fa fa-twitter"></i></a></li>
                              <li><a href="https://www.instagram.com/joao_s_o" target="#blank"><i class="icon fa fa-instagram"></i></a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="teamtext">
                        <h4>João Oliveira</h4>
                        <p>Web Designer</p>
                </div>
               </div>
               <div class="col-md-3">
                  <div class="single-team">
                  <img src="assets/img/equipa/sandra tavares.jpg" alt="" class="img-responsive">
                     <div class="team-hover">
                        <div class="team-content">
                           <ul>
                              <li><a href="https://www.facebook.com/profile.php?id=1582538260" target="#blank"><i class="icon fa fa-facebook"></i></a></li>
                              <li><a href="https://twitter.com/BuZZaZinha" target="#blank"><i class="icon fa fa-twitter"></i></a></li>
                              <li><a href="https://www.instagram.com/sandra_buzza" target="#blank"><i class="icon fa fa-instagram"></i></a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="teamtext">
                <h4>Sandra Tavares</h4>
					  <p>Professora / Coordenadora</p>
                </div>
               </div>
               </div>
            </div>
         </div>
	  </section>
	  
		<!-- Start feedback Area -->
		<section class="feedback-area section-gap" id="feedback">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-12 pb-50 header-text text-center">
						<h1 class="mb-10"><strong>Objetivos a atingir com este projeto</strong></h1>
						<p>
						Aqui encontram-se os nossos principais objetivos com a realização deste projeto.
						</p>
					</div>
				</div>
				<div class="row feedback-contents justify-content-between align-items-center">
				<div class="col-lg-6 feedback-left">
					<div class="mn-accordion" id="accordion">
						<!--Accordion item-->
						<div class="accordion-item">
							<div class="accordion-heading">
								<h3>Inovação</h3>
								<div class="icon">
									<i class="lnr lnr-chevron-right"></i>
								</div>
							</div>
							<div class="accordion-content">
								<p>O nosso principal objetivo com este website é inovação. O <b style="font-weight:900;"> ENTRElinhas </b> primeiramente é um jornal em formato de papel, mas, agora com este trabalho passou a ter também o formato digital.</p>
							</div>
						</div>
						<!--Accordion item-->
						<!--Accordion item-->
						<div class="accordion-item">
							<div class="accordion-heading">
								<h3>Interatividade</h3>
								<div class="icon">
									<i class="lnr lnr-chevron-right"></i>
								</div>
							</div>
							<div class="accordion-content">
								<p>Com o  <b style="font-weight:900;"> ENTRElinhas </b>  digital procuramos uma maior interatividade. Agora os nossos leitores poderão interagir com o jornal, tendo acesso a várias funcionalidades, como:
								<br>
								<br>- Criar conta
								<br>- Dar gosto em publicações 
									<br>- Comentar 
									<br>- Submeter notícias
									<br>- etc . . . 
								</p>
							</div>
						</div>
						<!--Accordion item-->
						<!--Accordion item-->
						<div class="accordion-item">
							<div class="accordion-heading">
								<h3>Apelativo</h3>
								<div class="icon">
									<i class="lnr lnr-chevron-right"></i>
								</div>
							</div>
							<div class="accordion-content">
								<p>Nos dias de hoje grande parte dos leitores não leem jornais em formato de papel, pois, com o aumento da tecnologia passaram a puder ler as notícias em formato digital, o que torna mais fácil o acesso. Devido a isso surgiu a ideia do <b style="font-weight:900;"> ENTRElinhas </b> digital.</p>
							</div>
						</div>
						<!--Accordion item-->
						<!--Accordion item-->
						<div class="accordion-item">
							<div class="accordion-heading">
								<h3>Regularidade</h3>
								<div class="icon">
									<i class="lnr lnr-chevron-right"></i>
								</div>
							</div>
							<div class="accordion-content">
								<p>Um dos problemas do <b style="font-weight:900;"> ENTRElinhas </b> em formato de papel é só ser lançado semestralmente. Agora com este website irá haver uma maior regularidade de notícias.</p>
							</div>
						</div>
						<!--Accordion item-->
					</div>
				</div>
					<div class="col-lg-5 feedback-right relative d-flex justify-content-center align-items-center">
						<div class="overlay overlay-bg"></div>
						<a class="play-btn" href="https://www.youtube.com/watch?v=KLV8YwPyR94"><img class="img-fluid" src="assets/img/play-btn.png" alt=""></a>
					</div>
				</div>
			</div>
		</section>
		<!-- End feedback Area -->
		
		
		<!-- Start brands Area -->
		<section class="brands-area">
		<div class="container no-padding">
			<div class="brand-wrap">
				<div class="row align-items-center active-brand-carusel justify-content-start no-gutters">
					<div class="col single-brand">
						<a href="https://www.colegiodelamas.com" target="_blank"><img class="mx-auto" src="assets/img/colegio.png" alt=""></a>
					</div>
					<div class="col single-brand">
						<a href="https://www.colegiodelamas.com/index.php/blog/atividades/entrelinhas" target="_blank"><img class="mx-auto" src="assets/img/entrelinhas.png" alt=""></a>
					</div>
					<div class="col single-brand">
						<a href="http://office365.colegiodelamas.com/" target="_blank"><img class="mx-auto" src="assets/img/office-365.png" alt=""></a>
					</div>
					<div class="col single-brand">
						<a href="https://complexo.colegiodelamas.com/" target="_blank"><img class="mx-auto" src="assets/img/complexo.png" alt=""></a>
					</div>
					<div class="col single-brand">
						<a href="https://www.colegiodelamas.com/index.php/inovacao/cwm" target="_blank"><img class="mx-auto" src="assets/img/minecraft.png" alt=""></a>
					</div>
				</div>
			</div>
		</div>
		</section>
		<!-- End brands Area -->
	
				<!-- start footer Area -->
				<?php 
				require 'includes/footer.php'; 
				include('includes/botao_topo.php');
				?>

		<!-- End footer Area -->

		<script src="assets/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="assets/js/vendor/bootstrap.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="assets/js/easing.min.js"></script>
		<script src="assets/js/hoverIntent.js"></script>
		<script src="assets/js/superfish.min.js"></script>
		<script src="assets/js/jquery.ajaxchimp.min.js"></script>
		<script src="assets/js/jquery.magnific-popup.min.js"></script>
		<script src="assets/js/mn-accordion.js"></script>
		<script src="assets/js/jquery-ui.js"></script>
		<script src="assets/js/jquery.nice-select.min.js"></script>
		<script src="assets/js/owl.carousel.min.js"></script>
		<script src="assets/js/mail-script.js"></script>
		<script src="assets/js/main.js"></script>
	</body>
</html>