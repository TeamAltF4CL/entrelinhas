<!-- <style>
	#survol{
  padding:0.5em;
  background:#ccc;
  width:15%;
  text-align:center;
  border: 1px solid #1c24ab;
  color:#1c24ab;
  background-color:white;
  cursor:pointer;
}
#survol:hover{
  border: 1px solid white;
  color:white;
  background-color:#1c24ab;
}
#tooltip{
  padding:0.5em;
  background:white;
  border: 1px solid #1c24ab;
  color:#1c24ab !important;
  width:30%;
  opacity:0;
  transition:opacity .5s;
	margin-top:-15px;
	
}

#tooltip.visible{opacity:1}
</style> -->
<!DOCTYPE html>
<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="assets/img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>ENTRElinhas | Partilha Connosco</title>
		<link rel="icon" href="assets/img/logo_escola.png" type="image/png">	
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="assets/css/linearicons.css">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/bootstrap.css">
		<link rel="stylesheet" href="assets/css/magnific-popup.css">
		<link rel="stylesheet" href="assets/css/nice-select.css">
		<link rel="stylesheet" href="assets/css/animate.min.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.css">
		<link rel="stylesheet" href="assets/css/jquery-ui.css">
        <link rel="stylesheet" href="assets/css/main.css">
		<link rel="stylesheet" href="assets/css/imagem_produto.css">
		
		<script src="assets/checkeditor/ckeditor.js"></script>
		<script src="assets/checkeditor/build-config.js"></script>
		

	</head>
	<body>

	<?php 
	    require 'includes/navbar.php'; 
	?>

		<div class="site-main-container">
			<!-- Start top-post Area -->
			<section class="top-post-area pt-10">
				<div class="container no-padding">
					<div class="row">
						<div class="col-lg-12">
							<div class="hero-nav-area" style="background-image: url('https://inbest.solutions/wp-content/uploads/2018/09/banner-correo.jpg') !important;">
								<h1 class="text-white">Partilha Connosco</h1>
								<p class="text-white link-nav"><a href="index.php">Início </a>  <span class="lnr lnr-arrow-right"></span><a href="partilha.php">Partilha </a></p>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End top-post Area -->
			<!-- Start contact-page Area -->
	
			<section class="contact-page-area pt-50 pb-120">
				<div class="container">
					<div class="row contact-wrap">
						
						<div class="col-lg-12 d-flex flex-column">
							
								<?php
										if(!isset($_SESSION['user_id'])){

											?>
											
															<textarea name="artigo" id="editor1" rows="10" cols="80">
															<b>	Registe-se e faça login </b> para poder <b> partilhar connosco </b> os seus artigos.
															</textarea>
											
									<?php
										}else{

											?>
		<!-- <p id="survol">Estrutura da Página</p>
<div id="tooltip">Para que uma publicação se destaque das demais, é necessário: <br>
- um título apelativo;
  <br>
- um assunto inteligível;
   <br>
- um texto coeso e estruturado;
   <br>
- uma imagem relacionada com a publicação.</div> -->
												<form method="POST"	action="includes/processos/proc_submeter.php" enctype="multipart/form-data">
													
												<input class="common-input mb-20 form-control" type="text" name="titulo" placeholder="Título (máx 20 caracteres)" pattern="[a-zA-Z0-9\sàáèéìíòóùú\?\d-_]+" title="Por favor não insira caracteres especiais!" maxlength=20 required>
												<input class="common-input mb-20 form-control" type="text" name = "assunto" placeholder="Assunto (máx 50 caracteres)" pattern="[a-zA-Z0-9\sàáèéìíòóùú\?\d-_]+" title="Por favor não insira caracteres especiais!" maxlength=50 required>
												
												<textarea name="texto" id="editor1" rows="100" cols="80" placeholder="Diga-nos algo.." require>
													</textarea>

												<br><br>
												<div class="imagem_produto">
                                    <div class="col-md-12">
                                    <div class="row">
                                    <div class="col-md-4">
                                        <p class="text-center" style="color:#007bff;font-weight:bold;">Imagem 1</p>
                                                    <!-- Editar Imagem -->
                                                    <div class="preview img-wrapper"></div>
                                                    
                                                      <div class="file-upload-wrapper">
                                                          <input type="file" name="imagem1" id="imagem1" class="file-upload-native" accept="image/*" required/>
                                                          <input type="text" disabled placeholder="Insira a Imagem" class="file-upload-text" />
                                                      </div>
                                                    <!-- / Editar Imagem -->
                                    </div>
                                    <div class="col-md-4">
                                    <p class="text-center" style="color:#007bff;font-weight:bold;">Imagem 2</p>
                                          <!-- Editar Imagem -->
                                          <div class="preview img-wrapper"></div>
                                                      <div class="file-upload-wrapper">
                                                          <input type="file" name="imagem2" id="imagem2" class="file-upload-native" accept="image/*" />
                                                          <input type="text" disabled placeholder="Insira a Imagem" class="file-upload-text" />
                                                      </div>
                                                    <!-- / Editar Imagem -->
                                    </div>

                                    <div class="col-md-4" style="color:#007bff;font-weight:bold;">
                                    <p class="text-center">Imagem 3</p>
                                    <!-- Editar Imagem -->
                                    <div class="preview img-wrapper"></div>
                                                      <div class="file-upload-wrapper">
                                                          <input type="file" name="imagem3" id="imagem3" class="file-upload-native" accept="image/*" />
                                                          <input type="text" disabled placeholder="Insira a Imagem" class="file-upload-text" />
                                                      </div>
                                                    <!-- / Editar Imagem -->
                                    </div>
                                    </div>
                                    </div>
                                    </div>
												<!-- <input type="file" name="imagem1" id="imagem1" placeholder="arquivo" required>
												<input type="file" name="imagem2" id="imagem2" placeholder="arquivo 2">
												<input type="file" name="imagem3" id="imagem3" placeholder="arquivo 3"> -->

											<br>
											
												<div class="col-12 text-center">
																<input type="submit" class="btn" name="submeter" id="submeter" value="Submeter" />
														<div>

														<br>
												</form>


															
									<?php
										}

											?>			
								<script>
									CKEDITOR.replace('editor1', {
										// Define the toolbar groups as it is a more accessible solution.
										toolbarGroups: [{
												"name": "basicstyles",
												"groups": ["basicstyles"]
											},
											{
												"name": "links",
												"groups": ["links"]
											},
											{
												"name": "paragraph",
												"groups": ["list", "blocks"]
											},
											{
												"name": "document",
												"groups": ["mode"]
											},
											{
												"name": "insert",
												"groups": ["insert"]
											},
											{
												"name": "styles",
												"groups": ["styles"]
											}
										],
										// Remove the redundant buttons from toolbar groups defined above.
										removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
									});
								</script>

							</div>


					</div>
				</div>
			</section>
	
			<!-- End contact-page Area -->
		</div>
				<!-- start footer Area -->
<?php 
require 'includes/footer.php'; 
include('includes/botao_topo.php');
?>

<!-- <script>
	var survol = document.getElementById("survol");
var tooltip =document.getElementById("tooltip");

survol.onmouseover = function(){
  //tooltip.style.display="block";
  tooltip.className="visible";
}
  
survol.onmouseout = function(){
 // tooltip.style.display="none";
  tooltip.className="hidden";
}

  
</script> -->
		<!-- Img Input -->
    	<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
		<script  src="assets/js/imagem_produto.js"></script>

		<!-- End footer Area -->
		<script src="assets/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="assets/js/vendor/bootstrap.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="assets/js/easing.min.js"></script>
		<script src="assets/js/hoverIntent.js"></script>
		<script src="assets/js/superfish.min.js"></script>
		<script src="assets/js/jquery.ajaxchimp.min.js"></script>
		<script src="assets/js/jquery.magnific-popup.min.js"></script>
		<script src="assets/js/mn-accordion.js"></script>
		<script src="assets/js/jquery-ui.js"></script>
		<script src="assets/js/jquery.nice-select.min.js"></script>
		<script src="assets/js/owl.carousel.min.js"></script>
		<script src="assets/js/mail-script.js"></script>
		<script src="assets/js/main.js"></script>
	</body>
</html>