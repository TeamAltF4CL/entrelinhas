<!DOCTYPE html>
<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="assets/img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>ENTRElinhas | Arquivo</title>
		<link rel="icon" href="assets/img/logo_escola.png" type="image/png">	
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="assets/css/linearicons.css">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/bootstrap.css">
		<link rel="stylesheet" href="assets/css/magnific-popup.css">
		<link rel="stylesheet" href="assets/css/nice-select.css">
		<link rel="stylesheet" href="assets/css/animate.min.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.css">
		<link rel="stylesheet" href="assets/css/jquery-ui.css">
		<link rel="stylesheet" href="assets/css/main.css">
	</head>
	<body>
	<?php 
	require 'includes/navbar.php'; 
	?>
		<div class="site-main-container">
			<!-- Start top-post Area -->
			<section class="top-post-area pt-10">
				<div class="container no-padding">
					<div class="row">
						<div class="col-lg-12">
							<div class="hero-nav-area" style="background-image: url('assets/img/jornal-bg.jpg') !important;">
								<h1 class="text-white">Arquivo</h1>
								<p class="text-white link-nav"><a href="index.php">Início </a>  <span class="lnr lnr-arrow-right"></span><a href="arquivo.php">Arquivo </a></p>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End top-post Area -->
			<!-- Start contact-page Area -->
			<?php
			

			/* consulta */
 			$sql1="Select * From edicao order by id_edicao DESC"; 

 
			/* executar a consulta */
			$result1 = mysqli_query($conn, $sql1);
				
			

			?>
			<section class="contact-page-area pt-50 pb-120">
				<div class="container"><div class="row contact-wrap">
					<!--mostrar todas as linhas da tabela-->
					<?php while($linha1 = mysqli_fetch_array($result1, MYSQLI_ASSOC)){ ?>
									
									
						<div class="col-lg-6" style="padding:20px;">
							<div style="background-color:#1c24ab;text-align:center;padding:20px;">
                        		<h1 style="color:white;"> <?php echo $linha1['nome_edicao']; ?> </h1>
							</div>

                       		<a target="_blank" href="assets/pdfs/<?php echo $linha1['pdf_edicao']; ?>"><div style="background-color:white;text-align:center;border: 2px solid #1c24ab;">
								<img src="assets/img/<?php echo $linha1['imagem_edicao']; ?>" class="img-fluid">
                        	</div></a>
                        </div>
						
						
					
						<?php    }?>
                      
						</div>

					<br>

					
				</div>
			</section>
			<!-- End contact-page Area -->
		</div>
				<!-- start footer Area -->
<?php 
require 'includes/footer.php'; 

include('includes/botao_topo.php');
?>
		<!-- End footer Area -->
		<script src="assets/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="assets/js/vendor/bootstrap.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="assets/js/easing.min.js"></script>
		<script src="assets/js/hoverIntent.js"></script>
		<script src="assets/js/superfish.min.js"></script>
		<script src="assets/js/jquery.ajaxchimp.min.js"></script>
		<script src="assets/js/jquery.magnific-popup.min.js"></script>
		<script src="assets/js/mn-accordion.js"></script>
		<script src="assets/js/jquery-ui.js"></script>
		<script src="assets/js/jquery.nice-select.min.js"></script>
		<script src="assets/js/owl.carousel.min.js"></script>
		<script src="assets/js/mail-script.js"></script>
		<script src="assets/js/main.js"></script>
	</body>
</html>