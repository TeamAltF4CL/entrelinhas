<?php

    include_once('includes/processos/proc_login.php');

        include_once('includes/processos/conexao.php'); 

?>

<!DOCTYPE html>
<html lang="zxx" class="no-js">
	<head>

<style>

	/*SELECT CSS*/
	.select_categoria {
	margin: 0;
	padding: 0;
	background-color: #004882;
	}

	.select_categoria .box {
	position: absolute;
	margin-bottom:25px;
	}

	.select_categoria .box select {
	background-color: #0563af;
	color: white;
	padding: 12px;
	border: none;
	width: 250px;
	box-shadow: 0 5px 25px rgba(0, 0, 0, 0.2);
	-webkit-appearance: button;
	appearance: button;
	outline: none;
	height:50px;
	border-radius: 5px 5px 5px 5px;
	}

	.select_categoria .box::before {
	content: "\f13a";
	font-family: FontAwesome;
	position: absolute;
	top: 0;
	right: 0;
	width: 20%;
	height: 100%;
	text-align: center;
	font-size: 28px;
	line-height: 45px;
	color: rgba(255, 255, 255, 0.5);
	background-color: rgba(255, 255, 255, 0.1);
	pointer-events: none;
	}

	.select_categoria .box:hover::before {
	color: rgba(255, 255, 255, 0.6);
	background-color: rgba(255, 255, 255, 0.2);
	}

	.select_categoria .box select option {
	padding: 30px;
	}



	/*SEARCH CSS*/


	pesquisar_categoria {
	background: #f2f2f2;
	font-family: 'Open Sans', sans-serif;
	}

	.pesquisar_categoria .search {
	width: auto;
	position: relative;
	display: flex;
	float: right !important;
	margin-bottom: 25px;
	}

	.pesquisar_categoria .searchTerm {
	width: 70%;
	border: 3px solid #0563af;
	border-right: none;
	padding: 5px;
	height: 50px;
	border-radius: 5px 0 0 5px;
	outline: none;
	color: #9DBFAF;
	}

	.pesquisar_categoria .searchTerm:focus{
	color: #0563af;
	}

	.pesquisar_categoria .searchButton {
	width: 40px;
	height: 50px;
	border: 1px solid #0563af;
	background: #0563af;
	text-align: center;
	color: #fff;
	border-radius: 0 5px 5px 0;
	cursor: pointer;
	font-size: 20px;
	}

	/*Resize the wrap to see the search bar change!*/
	/* .pesquisar_categoria .wrap{
	width: 60%;
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
	} */



/*CARD*/
.container2 {
  margin: 75px auto 120px;
  background-color: #fff;
  padding: 0 20px 20px;
  border-radius: 6px;
  -webkit-border-radius: 6px;
  -moz-border-radius: 6px;
  box-shadow: 0 2px 5px rgba(0,0,0,0.075);
  -webkit-box-shadow: 0 2px 5px rgba(0,0,0,0.075);
  -moz-box-shadow: 0 2px 5px rgba(0,0,0,0.075);
  text-align: center;
}
.container2:hover .avatar-flip {
  transform: rotateY(180deg);
  -webkit-transform: rotateY(180deg);
}
.container2:hover .avatar-flip img:first-child {
  opacity: 0;
}
.container2:hover .avatar-flip img:last-child {
  opacity: 1;
}
.avatar-flip {
  border-radius: 100px;
  overflow: hidden;
  height: 150px;
  width: 150px;
  position: relative;
  margin: auto;
  top: -60px;
  transition: all 0.3s ease-in-out;
  -webkit-transition: all 0.3s ease-in-out;
  -moz-transition: all 0.3s ease-in-out;
  box-shadow: 0 0 0 13px #f0f0f0;
  -webkit-box-shadow: 0 0 0 13px #f0f0f0;
  -moz-box-shadow: 0 0 0 13px #f0f0f0;
}
.avatar-flip img {
  position: absolute;
  left: 0;
  top: 0;
  
  transition: all 0.3s ease-in-out;
  -webkit-transition: all 0.3s ease-in-out;
  -moz-transition: all 0.3s ease-in-out;
}
.avatar-flip img:first-child {
  z-index: 1;
}
.avatar-flip img:last-child {
  z-index: 0;
  transform: rotateY(180deg);
  -webkit-transform: rotateY(180deg);
  opacity: 0;
}
body {
  font-family: Arial;

  background-color: #f5f5f5;
  
}
.end { margin-right: 0 !important; }
/* Column Grids End */

.wrapper { width: 980px; margin: 30px auto; position: relative;}
.counter { background-color: #ffffff; padding: 20px 0; border-radius: 5px;}
.count-title { font-size: 40px; font-weight: normal;  margin-top: 10px; margin-bottom: 0; text-align: center; }
.count-text { font-size: 20px; font-weight: normal;  margin-top: 10px; margin-bottom: 0; text-align: center; font-weight: 900; color: #808080;}
.fa-2x { margin: 0 auto; float: center; display: table; color: rgba(0, 105, 243, 0.979); } }

.gray{
	color: #a5a5a5;
}

</style>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="icon" href="assets/img/logo_escola.png" type="image/png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>ENTRElinhas | Descobre</title>
		<link rel="icon" href="assets/img/logo_escola.png" type="image/png">
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="assets/css/linearicons.css">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/bootstrap.css">
		<link rel="stylesheet" href="assets/css/magnific-popup.css">
		<link rel="stylesheet" href="assets/css/nice-select.css">
		<link rel="stylesheet" href="assets/css/animate.min.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.css">
		<link rel="stylesheet" href="assets/css/jquery-ui.css">
		<link rel="stylesheet" href="assets/css/main.css">

		<!--Carregar mais noticias from BD com o botao-->

		<style>
		    html, body {
    max-width: 100%;
    overflow-x: hidden;
}
		</style>
	</head>
	<body>
	
    <?php

				error_reporting(0);
        require 'includes/navbar.php';


    ?>  

		<div class="site-main-container">
			<!-- Start top-post Area -->
			<section class="top-post-area pt-10">
				<div class="container no-padding">
					<div class="row">
						<div class="col-lg-12">
							<div class="hero-nav-area" style="background-image: url('https://thumbs.gfycat.com/NiceTidyDuck-mobile.jpg') !important;">
								<h1 class="text-white">Descobre</h1>
								<p class="text-white link-nav"><a href="index.php">Inicio </a>  <span class="lnr lnr-arrow-right"></span><a href="descobre.php">Descobre</a></p>
							</div>
						</div>
							<?php include('includes/noticias/noticia_ultima_hora.php') ; ?>
					</div>
				</div>
			</section>
            <!-- End top-post Area -->

			<!-- Start service Area -->
		<?php include_once('includes/processos/conexao.php'); 


// Calculo Views

$pesquisa = "SELECT sum(views) as views from publicacoes";

	$result = mysqli_query($conn, $pesquisa);

		$views = mysqli_fetch_assoc($result);

 // Calculo publicações

 $pesquisa2 = "SELECT count(id) as publicacoes from publicacoes";

 	$result2 = mysqli_query($conn, $pesquisa2);

 		$publicacoes = mysqli_fetch_assoc($result2);

 

 

// Calculo likes

$pesquisa3 = "SELECT sum(likes) as likes from publicacoes";

	$result3 = mysqli_query($conn, $pesquisa3);

		$likes = mysqli_fetch_assoc($result3);


?>
<section class="service-area section-gap">
<div class="container">
<div class="row">
	<div class="col-lg-4">
	<div class="counter col_third" style="text-align: center;">
	<i class="fas fa-eye fa-2x"></i>
	<h2 class="timer count-title count-number" data-to="<?php echo $views['views']  ;?>" data-speed="1500"></h2>
	 <p class="count-text ">Número de Visualizações</p>
  </div>
	</div>
	<div class="col-lg-4">
	<div class="counter col_third"  style="text-align: center;">
	<i class="far fa-file-alt fa-2x"></i>
	<h2 class="timer count-title count-number" data-to="<?php echo $publicacoes['publicacoes'];?>" data-speed="1500"></h2>
	<p class="count-text ">Número de Publicações</p>
  </div>
	</div>
	<div class="col-lg-4">
	<div class="counter col_third"  style="text-align: center;">
	<i class="far fa-thumbs-up fa-2x"></i>
	<h2 class="timer count-title count-number" data-to="<?php echo $likes['likes'];?>" data-speed="1500"></h2>
	<p class="count-text ">Número de Likes</p>
  </div>
	</div>
</div>
</div>
</section>
<!-- End service Area -->
            
			<!-- Titulo -->
			<section class="latest-post-area pb-120">
				<div class="container no-padding">
				<div class="row">
					<div class="col-sm-12">
							<h1>Conheça o nosso top 9!</h1>
							<br>
					</div>
				</div>

	<!-- Select e pesquisa -->

						<?php
						
						// fazer a procura
						if($_POST[bt_search]=='search')
						{
						$designacao_temp=utf8_decode($_POST[srch_term]);
						/* procura */
						}
						else{

						}
						if(isset($_GET[categ])){
							$categ=$_GET[categ];

							$users_ordenar="SELECT * From utilizadores WHERE estado = 1 AND nome LIKE '%".$designacao_temp."%' ORDER BY $categ DESC LIMIT 9";
						}
							else
						{
							/* consulta */
							$users_ordenar="SELECT * From utilizadores WHERE estado = 1 AND nome LIKE '%".$designacao_temp."%' ORDER BY views_tot DESC LIMIT 9";
						}

						/* executar a consulta */
						$result_users_ordenar = mysqli_query($conn, $users_ordenar);

						/* contar o número de linhas da comsulta */
						$nr_linhas = mysqli_num_rows($result_users_ordenar);

						?>

				<div class="container">

				<form class="" method="post">
				<div class="row" id="custom-search-input" style="flex-wrap:nowrap;">


				<div class="col-sm-6">

				<div class="select_categoria">

					<div class="box">

						<select name="categ" onChange="javascript:location.href = this.value;">
						<option value="" >Selecione um filtro</option>
						<option value="descobre.php" >Todos</option>
						<option value= "descobre.php?categ=likes_tot">Likes</option>
						<option value= "descobre.php?categ=views_tot">Views</option>
						</select>
					</div>
				</div>
				</div>

				<?php 
					//Detetar o que pesquisou
						if($designacao_temp ==  ""){
								$default_pesquisa = "";
										}else{
											$default_pesquisa = $designacao_temp;
											}
						?>
						

				<div class="col-sm-6 pesquisar_categoria" style="padding-right:0;">
						<div class="search" style="margin-right: -40px;">
							<input type="text" class="searchTerm" name="srch_term" id="srch_term" placeholder="Pesquisar.." value="<?php echo $default_pesquisa;?>" pattern="[a-zA-Z0-9\sàáèéìíòóùú\?]+" title="Por favor não insira caracteres especiais!">
							<button class="searchButton" type="submit"  id="bt_search" name="bt_search" value="search" >		
							<i class="fa fa-search"></i>
							</button>
					</div>
				</div>


				</div>	

				</form>
				</div>

	<!-- Fim da Select e pesquisa -->	


				<div class="row">
	
									

										<?php 
										
										if($nr_linhas<>0){


                            //Buscar os users todos e apresentar o principal nas suas respetivas cartas de perfil
   
                       	   while($linha_users_ordenar = mysqli_fetch_array($result_users_ordenar, MYSQLI_ASSOC)){
																							
															// Calculo publicações
																	$pesquisa2 = "SELECT count(id) as publicacoes from publicacoes WHERE dados = '". $linha_users_ordenar['id'] ."' ";
																			$result2 = mysqli_query($conn, $pesquisa2);
																					$publicacoes = mysqli_fetch_assoc($result2);
																									
																							
                    ?>

				<!-- Começo das cartas de perfil -->  
				<div class="col-lg-4 post-list">
					 <div class="container2">
						<div class="avatar-flip">
						<a href="ver-perfil.php?id=<?php echo $linha_users_ordenar['id'];?>&nome=<?php echo $linha_users_ordenar['nome'];?>"><img src="assets/fotos_user/<?php echo $linha_users_ordenar['nome_imagem'] ;?>" height="150" width="150">
							<img src="assets/fotos_user/<?php echo $linha_users_ordenar['nome_imagem'] ;?>" height="150" width="150"></a>
						</div>
						<a href="ver-perfil.php?id=<?php echo $linha_users_ordenar['id'];?>&nome=<?php echo $linha_users_ordenar['nome'];?>"><h2><?php echo $linha_users_ordenar['nome'] ;?></h2></a>
						<p></p>
						<div class="row">
							<div class="col-lg-4">
							<p>Publicacoes: <b>  <h5 style="color:#1c24ab"><?php echo $publicacoes['publicacoes'] ;?>  </b> </h5></p></div>
							<div class="col-lg-4">
							<p>Views: <b> <h5 style="color:#1c24ab"><?php echo $linha_users_ordenar['views_tot'] ;?> </b></h5> </p></div>
							<div class="col-lg-4">
							<p>Likes:  <b> <h5 style="color:#1c24ab"><?php echo $linha_users_ordenar['likes_tot'] ;?> </b></h5> </p></div>
							</div>
					</div>
				</div>
				  <!-- Fim das cartas de perfil -->   



                <?php
											
										} //Fim do ciclo While

									}else{ //se o numero de linhas for diferente de 0

										$mensagem_pra_0_resutlados = "Nao foram encnontrados resultados para <b><i> '$designacao_temp' </i></b>";
									
                ?>

    	<!-- Nao ha pesquisas -->
			<br><br>
			<section class="col-lg-12 post-list">
				<div class="container">
				<div class="row">
					<div class="col-sm-12" align="center">
							<p class="excert"><?php echo $mensagem_pra_0_resutlados ;?> </p>
					</div>
				</div>

						 <?php
											
										} //Fim do else


									
                ?>
					</div>
				</div>
			</section>
			<!-- End latest-post Area -->
		</div>
		
        <?php
				include('includes/footer.php');
				
				include('includes/botao_topo.php');
        ?>	
        
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">

		<script src="assets/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="assets/js/vendor/bootstrap.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="assets/js/easing.min.js"></script>
		<script src="assets/js/hoverIntent.js"></script>
		<script src="assets/js/superfish.min.js"></script>
		<script src="assets/js/jquery.ajaxchimp.min.js"></script>
		<script src="assets/js/jquery.magnific-popup.min.js"></script>
		<script src="assets/js/mn-accordion.js"></script>
		<script src="assets/js/jquery-ui.js"></script>
		<script src="assets/js/jquery.nice-select.min.js"></script>
		<script src="assets/js/owl.carousel.min.js"></script>
		<script src="assets/js/mail-script.js"></script>
		<script src="assets/js/main.js"></script>
		
		<script  src="assets/js/acerca.js"></script>

	</body>
</html>

