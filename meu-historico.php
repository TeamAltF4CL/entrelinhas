<?php

    include_once('includes/processos/proc_login.php');

        include_once('includes/processos/conexao.php'); 


				if (!isset($_SESSION["user_id"])) //se não existe login 
				{
				header('Location: index.php');
					exit;
				}
				else //se existe login
				{


?>

<!DOCTYPE html>
<html lang="zxx" class="no-js">
	<head>

<style>

.container2 {
  margin: 75px auto 120px;
  background-color: #fff;
  padding: 0 20px 20px;
  border-radius: 6px;
  -webkit-border-radius: 6px;
  -moz-border-radius: 6px;
  box-shadow: 0 2px 5px rgba(0,0,0,0.075);
  -webkit-box-shadow: 0 2px 5px rgba(0,0,0,0.075);
  -moz-box-shadow: 0 2px 5px rgba(0,0,0,0.075);
  text-align: center;
}
.container2:hover .avatar-flip {
  transform: rotateY(180deg);
  -webkit-transform: rotateY(180deg);
}
.container2:hover .avatar-flip img:first-child {
  opacity: 0;
}
.container2:hover .avatar-flip img:last-child {
  opacity: 1;
}
.avatar-flip {
  border-radius: 100px;
  overflow: hidden;
  height: 150px;
  width: 150px;
  position: relative;
  margin: auto;
  top: -60px;
  transition: all 0.3s ease-in-out;
  -webkit-transition: all 0.3s ease-in-out;
  -moz-transition: all 0.3s ease-in-out;
  box-shadow: 0 0 0 13px #f0f0f0;
  -webkit-box-shadow: 0 0 0 13px #f0f0f0;
  -moz-box-shadow: 0 0 0 13px #f0f0f0;
}
.avatar-flip img {
  position: absolute;
  left: 0;
  top: 0;
  
  transition: all 0.3s ease-in-out;
  -webkit-transition: all 0.3s ease-in-out;
  -moz-transition: all 0.3s ease-in-out;
}
.avatar-flip img:first-child {
  z-index: 1;
}
.avatar-flip img:last-child {
  z-index: 0;
  transform: rotateY(180deg);
  -webkit-transform: rotateY(180deg);
  opacity: 0;
}

</style>

		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="icon" href="assets/img/logo_escola.png" type="image/png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>ENTRElinhas | Notícias</title>
		<link rel="icon" href="assets/img/logo_escola.png" type="image/png">
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="assets/css/linearicons.css">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/bootstrap.css">
		<link rel="stylesheet" href="assets/css/magnific-popup.css">
		<link rel="stylesheet" href="assets/css/nice-select.css">
		<link rel="stylesheet" href="assets/css/animate.min.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.css">
		<link rel="stylesheet" href="assets/css/jquery-ui.css">
		<link rel="stylesheet" href="assets/css/main.css">

		<!--Carregar mais noticias from BD com o botao-->

		
	</head>
	<body>
	
    <?php

        require 'includes/navbar.php';


        $sql="SELECT * From utilizadores where id= '" .$_SESSION['user_id']. "' ";
        $result = mysqli_query($conn, $sql);

		$linha = mysqli_fetch_array($result, MYSQLI_ASSOC);
		
						// Calculo Views
						$pesquisa = "SELECT sum(views) as views from publicacoes WHERE dados = '" .$_SESSION['user_id']. "' ";
						$result = mysqli_query($conn, $pesquisa);
							$views = mysqli_fetch_assoc($result);
	
	
					// Calculo publicações
					$pesquisa2 = "SELECT count(id) as publicacoes from publicacoes WHERE dados = '" .$_SESSION['user_id']. "'";
						$result2 = mysqli_query($conn, $pesquisa2);
							$publicacoes = mysqli_fetch_assoc($result2);
					
	
					// Calculo likes
					$pesquisa3 = "SELECT sum(likes) as likes from publicacoes WHERE dados = '" .$_SESSION['user_id']. "'";
						$result3 = mysqli_query($conn, $pesquisa3);
							$likes = mysqli_fetch_assoc($result3);

    ?>  

		<div class="site-main-container">
			<!-- Start top-post Area -->
			<section class="top-post-area pt-10">
				<div class="container no-padding">
					<div class="row">
						<div class="col-lg-12">
							<div class="hero-nav-area" style="background-image: url('https://images.unsplash.com/photo-1516382799247-87df95d790b7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80') !important;">
								<h1 class="text-white">Histórico</h1>
								<p class="text-white link-nav"><a href="index.php">Inicio </a>  <span class="lnr lnr-arrow-right"></span><a href="perfil.php"><?php echo $linha['nome'] ;?></a></p>
							</div>
						</div>
							<?php include('includes/noticias/noticia_ultima_hora.php') ; ?>
					</div>
				</div>
			</section>
            <!-- End top-post Area -->



			<!-- Start latest-post Area -->
			<section class="latest-post-area pb-120">

				<div class="container no-padding">
					<div class="row">

<!-- Carta perfil -->
				<div class="col-lg-4 post-list">
					 <div class="container2">
						<div class="avatar-flip">
							<img src="assets/fotos_user/<?php echo $linha['nome_imagem'] ;?>" height="150" width="150">
							<img src="assets/fotos_user/<?php echo $linha['nome_imagem'] ;?>" height="150" width="150">
						</div>
						<h2><?php echo $linha['nome'] ;?></h2>
						<p></p>
						<div class="row">
							<div class="col-lg-4">
							<p>Publicacoes: <b>  <h5 style="color:#1c24ab"><?php echo $publicacoes['publicacoes'] ;?>  </b> </h5></p></div>
							<div class="col-lg-4">
							<p>Views: <b> <h5 style="color:#1c24ab"><?php echo $linha['views_tot'] ;?> </b></h5> </p></div>
							<div class="col-lg-4">
							<p>Likes:  <b> <h5 style="color:#1c24ab"><?php echo $linha['likes_tot'] ;?> </b></h5> </p></div>
							</div>
					</div>
				</div>
				<!-- Fim da carta do perfil -->

						<div class="col-lg-8 post-list" >
                            
                        <?php
                        /* Todas as noticias (vars: $linha) */
                        include('includes/noticias/noticias_visualizado_perfil.php');
                        ?>

                        </div>
                       
					</div>
				</div>
			</section>
			<!-- End latest-post Area -->
		</div>
		
        <?php
				include('includes/footer.php');
				
				include('includes/botao_topo.php');
        ?>	
        
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">

		<script src="assets/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="assets/js/vendor/bootstrap.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="assets/js/easing.min.js"></script>
		<script src="assets/js/hoverIntent.js"></script>
		<script src="assets/js/superfish.min.js"></script>
		<script src="assets/js/jquery.ajaxchimp.min.js"></script>
		<script src="assets/js/jquery.magnific-popup.min.js"></script>
		<script src="assets/js/mn-accordion.js"></script>
		<script src="assets/js/jquery-ui.js"></script>
		<script src="assets/js/jquery.nice-select.min.js"></script>
		<script src="assets/js/owl.carousel.min.js"></script>
		<script src="assets/js/mail-script.js"></script>
		<script src="assets/js/main.js"></script>

	</body>
</html>

<?php
				}
				?>