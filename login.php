<?php
    include_once("includes/processos/proc_login.php");
    ?>

    <?php
            if (isset($_SESSION["admin_id"] )) //se existe login
                     {
    					header('Location: admin/inicio.php');
    								}
    
    
    				elseif (isset($_SESSION["user_id"] ))
    				{
    			    	header('Location: index.php');
    
    				}else{  //se nao existe login
    				
      ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>ENTRElinhas | Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" href="assets/img/logo_escola.png" type="image/png">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/login_registo/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/login_registo/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/login_registo/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/login_registo/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="assets/login_registo/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/login_registo/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/login_registo/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="assets/login_registo/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/login_registo/css/util.css">
	<link rel="stylesheet" type="text/css" href="assets/login_registo/css/main.css">
<!--===============================================================================================-->



<!-- Mobile Specific Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="assets/img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title> ENTRElinhas | Login </title>
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
<!--
CSS
		============================================= -->
		<link rel="stylesheet" href="assets/css/linearicons.css">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/bootstrap.css">
		<link rel="stylesheet" href="assets/css/magnific-popup.css">
		<link rel="stylesheet" href="assets/css/nice-select.css">
		<link rel="stylesheet" href="assets/css/animate.min.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.css">
		<link rel="stylesheet" href="assets/css/jquery-ui.css">
		<link rel="stylesheet" href="assets/css/main.css">

<style>

.botao_login{
color: blue !important;
border: 2px solid blue !important;
}

.botao_login:hover{
background-color: transparent !important;
color: white !important;
border: 2px solid white !important;
}
</style>
		
</head>
<body>

	<div class="limiter">
		<div class="container-login100" style="background-image: url('assets/login_registo/images/colegio.jpg');">
			<div class="wrap-login100">
			<a href="index.php"><div style="background-color:white;border-radius: 50%;width:35px;height:35px;margin-top:-40px;margin-left:-40px">
			<span style="display:flex;justify-content:center;align-items:center;padding:7px;color:#1c24ab;font-weight:999;font-size:20px;"class="lnr lnr-arrow-left"></span>
			</div></a>
				<form class="login100-form validate-form" method="POST" action="includes/processos/proc_login.php"> 
					<span class="login100-form-logo">
						<img src="assets/login_registo/images/logo_escola.png" style="width:70%;">
					</span>

					<span class="login100-form-title p-b-34 p-t-27">
						Login
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Insira o Email">
						<input class="input100" type="email" name="email" id="email" placeholder="Email" pattern="[^@]+@[^@]+.[a-zA-Z\d-_]{2,6}">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Insira a Password" style="margin-bottom: 10px;">
						<input class="input100" type="password" name="password" id="password" placeholder="Password" pattern="[a-zA-Z0-9\sàáèéìíòóùú\?\d-_]+" title="Por favor não insira caracteres especiais!">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>

					<div>
							<a href="reset-password.php?token=wBqvayJh2n2RVbcC28we9fweew29fwikfwo"><p style="font-family:Arial Black;color:white;font-style: italic;font-size:13px;float:right;margin-top:0;"> Esqueceu-se da Password? </p></a>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn botao_login" id="login" name="login">
							Login
						</button>


					</div>

					<div>
					<a href="registo.php"> <p style="color:white;text-align:center;margin-top:20px;font-size:13px;"> Se não tiver conta  <b style="font-weight: bold;font-size:16px;color:lime;">registe-se! </b> </p></a>
					</div>
				</form>


			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="assets/login_registo/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/login_registo/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/login_registo/vendor/bootstrap/js/popper.js"></script>
	<script src="assets/login_registo/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/login_registo/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/login_registo/vendor/daterangepicker/moment.min.js"></script>
	<script src="assets/login_registo/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="assets/login_registo/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="assets/login_registo/js/main.js"></script>
	<!--<script src="includes/scripts/login.js"></script>-->

</body>
</html>

<?php
		}
?>
