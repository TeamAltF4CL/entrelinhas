<!DOCTYPE html>
<html lang="pt" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="assets/img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Site Title -->
		<title>ENTRElinhas | Noticía</title>
		<link rel="icon" href="assets/img/logo_escola.png" type="image/png">
		<!--Likes-->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
			
	<!--Fim dos Likes-->

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="assets/css/linearicons.css">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/bootstrap.css">
		<link rel="stylesheet" href="assets/css/magnific-popup.css">
		<link rel="stylesheet" href="assets/css/nice-select.css">
		<link rel="stylesheet" href="assets/css/animate.min.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.css">
		<link rel="stylesheet" href="assets/css/jquery-ui.css">
		<link rel="stylesheet" href="assets/css/main.css">
		<link rel="stylesheet" href="assets/css/button-like.css">

		<!--MyScroll-->
		<script type="text/javascript" src="js/redips-scroll.js"></script>

		<style>
		.nomeutilizador{
			font-weight:600;
			color:#1c24ab;
			font-size:17px;
		}
		</style>
	</head>
	<body>
   
   <?php

			error_reporting(0);

        require 'includes/navbar.php';

        $id_pub = $_GET['id'];
				$titulo_pub = $_GET['titulo'];
				$user_id_para_o_visualizado =	$_SESSION['user_id'];

        if($id_pub == 0){
            header('location:index.php');
        }else{

			try{
				$pdo =new PDO('mysql:host=localhost;dbname=entrelin_entrelinhas','root','');
			} catch(PDOException $e){
				echo $e->getMessage();
			}
			
			$selecionar = $pdo->prepare("SELECT views FROM publicacoes WHERE id=$id_pub");
			$selecionar->execute();
			
			$resultados=$selecionar->fetchAll(PDO::FETCH_ASSOC);
			
			foreach($resultados as $results);
				$contar=$results['views'] + 1;
				$update=$pdo->prepare("UPDATE publicacoes SET views=:views WHERE id=$id_pub");
				$update->bindValue(':views',$contar);
				$update->execute();
			
				date_default_timezone_set('Europe/Lisbon');
				$data_hora_currente = date('Y-m-d H:i:s');
				

									//Add view ao user na tabela utilizadores
									try{
										$pdo2 =new PDO('mysql:host=localhost;dbname=entrelin_entrelinhas','root','');
									} catch(PDOException $e){
										echo $e->getMessage();
									}
									
									$encontrar_user = $pdo2->prepare("SELECT dados FROM publicacoes WHERE id=$id_pub");
									$encontrar_user->execute();
									$linha_encontrar_user = $encontrar_user->fetch();


									$selecionar_user = $pdo2->prepare("SELECT views_tot FROM utilizadores WHERE id= '". $linha_encontrar_user['dados'] ."' ");
									$selecionar_user->execute();
									
									$resultados_user=$selecionar_user->fetchAll(PDO::FETCH_ASSOC);
									
									foreach($resultados_user as $results_user);
										$contar_user=$results_user['views_tot'] + 1;
										$update_user=$pdo2->prepare("UPDATE utilizadores SET views_tot=:views_tot WHERE id='" .$linha_encontrar_user['dados']. "'");
										$update_user->bindValue(':views_tot',$contar_user);
										$update_user->execute();


    //Pesquisa
         $noticia= "SELECT * FROM publicacoes,categorias WHERE publicacoes.id_categoria = categorias.id_categoria AND publicacoes.id= $id_pub AND publicacoes.titulo= '". $titulo_pub ."' LIMIT 1";
            $resultados_noticia= mysqli_query($conn, $noticia);
                while($linha = mysqli_fetch_array($resultados_noticia, MYSQLI_ASSOC)){

					$user_infs="SELECT * From utilizadores WHERE id = '" .$linha['dados']. "' ";
					$result_para_o_user = mysqli_query($conn, $user_infs);
					$user = mysqli_fetch_array($result_para_o_user, MYSQLI_ASSOC);


					//O user ja viu a publicacao?
					$visualizado_ou_nao = "SELECT * FROM visualizado WHERE id_publicacao = $id_pub AND id_utilizador= $user_id_para_o_visualizado ";
					
					$result_visualizado_ou_nao = mysqli_query($conn, $visualizado_ou_nao);

					$nr_linha_visualizado = mysqli_num_rows($result_visualizado_ou_nao);

						if($nr_linha_visualizado == 0){
							$visualizado_insert = "INSERT INTO visualizado (id_publicacao,id_utilizador,data_visualizada, visualizado) VALUES ('$id_pub', '$user_id_para_o_visualizado', '$data_hora_currente', '1')";
							$icon_visto = "";

							$result_visualizado_insert = mysqli_query($conn, $visualizado_insert);
						}else{
							$icon_visto = "<li><a><span class='lnr lnr-checkmark-circle'></span>Já visualizada por si </a></li>";

						}
	?>  


		<div class="site-main-container">
			<!-- Start top-post Area -->
			<section class="top-post-area pt-10">
				<div class="container no-padding">
					<div class="row">
						<div class="col-lg-12">
							<div class="hero-nav-area" style="background-image: url('https://previews.123rf.com/images/sergiobarrios/sergiobarrios1504/sergiobarrios150400072/38517914-diarios-del-mundo-detalle-de-peri%C3%B3dicos-con-noticias-informaci%C3%B3n-y-lectura.jpg') !important;">
								<h1 class="text-white"><?php echo $linha['titulo'] ;?></h1>
								<p class="text-white link-nav"><a href="index.php">Ínicio </a>  <span class="lnr lnr-arrow-right"></span><a href="noticias.php?categ=<?php echo $linha['id_categoria'];?>"><?php echo $linha['categoria'] ;?> </a><span class="lnr lnr-arrow-right"></span><a href="#">Notícia </a></p>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End top-post Area -->
			<!-- Start latest-post Area -->
			<section class="latest-post-area pb-120">
				<div class="container no-padding">
					<div class="row">
						<div class="col-lg-8 post-list">
							<!-- Start single-post Area -->
							<div class="single-post-wrap">
							<div class="feature-img-thumb relative">

							<?php

								//Conteudo
								$imagem1 = $linha['imgnome'];
								$imagem2 = $linha['imgnome2'];
								$imagem3 = $linha['imgnome3'];
				
									//Aray do conteudo
									$imagens_todas = array($imagem1, $imagem2, $imagem3);	

									//Conta o que tem dentro do array
									$count_conteudo = 0;

						
														if($imagem2 == '' and $imagem3 == '' ){
															
											?>

											
				
									
									<img class="img-fluid" src="assets/img/noticias/<?php echo $linha['imgnome'] ;?>" alt="">
				
											
											
									<?php
															}elseif($imagem3 == ''){
									?>
									<div class="active-gallery-carusel">
									<div class="single-img relative">
										
											<img class="img-fluid item" src="assets/img/noticias/<?php echo $linha['imgnome'] ;?>" alt="">
										</div>
										<div class="single-img relative">
										
											<img class="img-fluid item" src="assets/img/noticias/<?php echo $linha['imgnome2'] ;?>" alt="">
										</div>
									</div>

										<?php
															}elseif($imagem2 == ''){
											
												
										?>

									<div class="active-gallery-carusel">
									<div class="single-img relative">
											
											<img class="img-fluid item" src="assets/img/noticias/<?php echo $linha['imgnome'] ;?>" alt="">
										</div>
										<div class="single-img relative">
										
											<img class="img-fluid item" src="assets/img/noticias/<?php echo $linha['imgnome3'] ;?>" alt="">
										</div>
									</div>
							
									<?php
															}else{
												
										?>

								<div class="active-gallery-carusel">
									<div class="single-img relative">
											
											<img class="img-fluid item" src="assets/img/noticias/<?php echo $linha['imgnome'] ;?>" alt="">
										</div>
										<div class="single-img relative">
											
											<img class="img-fluid item" src="assets/img/noticias/<?php echo $linha['imgnome2'] ;?>" alt="">
										</div>
										<div class="single-img relative">
										
											<img class="img-fluid item" src="assets/img/noticias/<?php echo $linha['imgnome3'] ;?>" alt="">
										</div>
									</div>

									<?php
															}
												
										?>

								</div>
								<div class="content-wrap">
									<ul class="tags mt-10">
										<li><a href="noticias.php?categ=<?php echo $linha['id_categoria'];?>"><?php echo $linha['categoria'] ;?></a></li>
									</ul>
									<a>
										<h3><?php echo $linha['titulo'] ;?></h3>
									</a>
									<ul class="meta pb-20">
										<li><a href="ver-perfil.php?id=<?php echo $user['id'];?>&nome=<?php echo $user['nome'];?>"><span class="lnr lnr-user"></span><?php echo $user['nome'] ;?></a></li>
										<li><a><span class="lnr lnr-calendar-full"></span><?php $datanormal=$linha['data']; $dataformatada=date("d-m-Y H:i",strtotime($datanormal)); echo $dataformatada;?></a></li>
                                        <li><a><span class="lnr lnr-heart"></span><?php echo $linha['likes'] ;?> </a></li>
                                        <li><a><span class="lnr lnr-eye"></span><?php echo $linha['views'] ;?> </a></li>
																				<?php echo $icon_visto ;?>
                                    </ul>
                                    <blockquote><?php echo $linha['assunto'] ;?></blockquote>
									<p>
                                            <?php echo $linha['texto'] ;?>
									</p>
									<br id="aqui">

									
									<!--LIKES-->
									<?php
										if($linha['likes'] == 1){
											$pessoas_ou_pessoa = "pessoa";
											$gostaram_ou_gostou = "gostou";
										}else{
												$pessoas_ou_pessoa = "pessoas";
												$gostaram_ou_gostou = "gostaram";
											}
											
									?>

								
									<?php
										if(!isset($_SESSION['user_id'])){
											

									?>
										<button class="button button-like" id="likes" rel="like"  data-toggle="tooltip" data-placement="right" title="Faça login para poder dar like"><i class="fa fa-heart"></i><span>Like</span></button>
										<br>
										<a data-target="#pessoasquegostaram" data-toggle="modal" class="MainNavText"  href="#pessoasquegostaram"> <small> <b> <?php echo $linha['likes'] ;?> <?php echo $pessoas_ou_pessoa ;?></b> <?php echo $gostaram_ou_gostou ;?> disto</small> </a>
										<br> <a href="login.php" style="color=red;"><small><b>Faça login</b> para poder deixar o seu <b>like</b> !</small></a>
									

										

									<?php

										}else{
											$publicacao_id = $id_pub; //Publicacoes id
											$user_id = $_SESSION['user_id']; //Utilizador id
											$se_ja_deu_like=mysqli_query($conn,"SELECT * FROM likes WHERE id_utilizador='$user_id' and id_publicacao='$publicacao_id' ");
											
											if(mysqli_num_rows($se_ja_deu_like)==0){ //Verifica se o suer ja deu like
						 
											?>

											<form method="POST" action="includes/processos/proc_like.php?user_id=<?php echo $user_id ;?>&id_publicacao=<?php echo $id_pub ;?>&titulo=<?php echo $titulo_pub ;?>">

												<button class="button button-like" type="submit" name="estado" value="like" title="Like" rel="Like" id="likes"><i class="fa fa-heart"></i><span>Like</span></button>
												<br>
												<a data-target="#pessoasquegostaram" data-toggle="modal" class="MainNavText"  href="#pessoasquegostaram"> <small> <b> <?php echo $linha['likes'] ;?> <?php echo $pessoas_ou_pessoa ;?></b> <?php echo $gostaram_ou_gostou ;?> disto</small> </a>
											
											<?php
												}else{
											 ?>
											 
										<form method="POST" action="includes/processos/proc_like.php?user_id=<?php echo $user_id ;?>&id_publicacao=<?php echo $id_pub ;?>&titulo=<?php echo $titulo_pub ;?>">

									<button class="button button-like liked" type="submit" name ="estado" value="unlike" title="Unlike" rel="Unlike" id="likes" onClick="my_scroll('noticia.php?id=<?php echo $id_pub;?>&titulo=<?php echo $titulo_pub; ?>'); return false"><i class="fa fa-heart"></i><span>Like</span></button>
									<br>
									<a data-target="#pessoasquegostaram" data-toggle="modal" class="MainNavText"  href="#pessoasquegostaram"> <small> <b> <?php echo $linha['likes'] ;?> <?php echo $pessoas_ou_pessoa ;?></b> <?php echo $gostaram_ou_gostou ;?> disto</small> </a>

								<?php
										}
									}
								 ?> 
								</form>

				<?php

					//Data e hora da noticia atual e id
					$id_pub_atual = $_GET['id'];
					 $data_hora_noticia_atual = $linha['data'];

					//Noticia seguinte
         $noticia_seguinte= "SELECT * FROM publicacoes,categorias WHERE publicacoes.id_categoria = categorias.id_categoria AND publicacoes.data >= '$data_hora_noticia_atual' AND publicacoes.id <> $id_pub_atual ORDER BY data ASC LIMIT 1";
            $resultados_noticia_seguinte= mysqli_query($conn, $noticia_seguinte);
							
						$nr_linhas_noticia_seguinte = mysqli_num_rows($resultados_noticia_seguinte);

							$linha_noticia_seguinte = mysqli_fetch_array($resultados_noticia_seguinte, MYSQLI_ASSOC);

								$id_pub_seguinte =  $linha_noticia_seguinte['id'];
								$titulo_pub_seguinte =  $linha_noticia_seguinte['titulo'];

					//Noticia anterior
         $noticia_aterior= "SELECT * FROM publicacoes,categorias WHERE publicacoes.id_categoria = categorias.id_categoria AND publicacoes.data <= '$data_hora_noticia_atual' AND publicacoes.id <> $id_pub_atual ORDER BY data DESC LIMIT 1";
						$resultados_noticia_aterior= mysqli_query($conn, $noticia_aterior);
						
						$nr_linhas_noticia_anterior = mysqli_num_rows($resultados_noticia_aterior);
						
								$linha_noticia_anterior = mysqli_fetch_array($resultados_noticia_aterior, MYSQLI_ASSOC);

								$id_pub_aterior =  $linha_noticia_anterior['id'];
								$titulo_pub_aterior =  $linha_noticia_anterior['titulo'];


									//Verificar se existe noticia anterior e seguinte
									if($nr_linhas_noticia_seguinte == 0 and $nr_linhas_noticia_anterior == 0){
										
						?>
		
								<?php
									}elseif($nr_linhas_noticia_seguinte == 0){

										?>

									<div class="navigation-wrap justify-content-between d-flex">
									<a class="prev" href="noticia.php?id=<?php echo $id_pub_aterior;?>&titulo=<?php echo $titulo_pub_aterior;?>"><span class="lnr lnr-arrow-left"></span>Noticia Anterior</a>
								</div>

								<?php
										}elseif($nr_linhas_noticia_anterior == 0){

											?>
											
									<div class="navigation-wrap justify-content-between d-flex">
										<a class="next" href="noticia.php?id=<?php echo $id_pub_seguinte;?>&titulo=<?php echo $titulo_pub_seguinte;?>">Noticia Seguinte<span class="lnr lnr-arrow-right"></span></a>
									</div>

									<?php		
										}else{
									?>

								<div class="navigation-wrap justify-content-between d-flex">
									<a class="prev" href="noticia.php?id=<?php echo $id_pub_aterior;?>&titulo=<?php echo $titulo_pub_aterior;?>"><span class="lnr lnr-arrow-left"></span>Noticia Anterior</a>
									<a class="next" href="noticia.php?id=<?php echo $id_pub_seguinte;?>&titulo=<?php echo $titulo_pub_seguinte;?>">Noticia Seguinte<span class="lnr lnr-arrow-right"></span></a>
								</div>

								<?php	
										}
									?>

								<div class="comment-sec-area">
									<div class="container">
											
										<?php
											/* consulta */
											$comentario="SELECT * FROM comentarios WHERE id_publicacao = $id_pub AND estado = 1 order by data_hora DESC" ;

											/* executar a consulta */
												$resultados_comentario = mysqli_query($conn, $comentario);

											/* contar o número de linhas da comsulta */
												$nr_linhas_comentarios = mysqli_num_rows($resultados_comentario);

												if($nr_linhas_comentarios == 0){
													$comentario_ou_comentarios = "Comentários";
												}elseif($nr_linhas_comentarios == 1){
													$comentario_ou_comentarios = "Comentário";
												}else{
													$comentario_ou_comentarios = "Comentários";
												}
										?>

										<div class="row flex-column">
											<!-- COmetarios-->	
											<h6><?php echo $nr_linhas_comentarios;?> <?php echo $comentario_ou_comentarios;?> </h6>

											<?php

													if($nr_linhas_comentarios == 0){

												?> 

											<div class="comment-list">
												<div class="single-comment justify-content-between d-flex">
													<div class="user justify-content-between d-flex">

														<div class="desc">
															<h5><a href="#">Sem comentários..</a></h5>
															<p class="comment">
																Ainda não existem comentários para esta publicação...não quer ser o primeiro?
															</p>
														</div>
													</div>

												</div>
											</div>
											<?php
													}else{
													
													while($linha_comentarios = mysqli_fetch_array($resultados_comentario, MYSQLI_ASSOC)){

															$user_infs_comentarios="SELECT * From utilizadores WHERE id = '" .$linha_comentarios['id_utilizador']. "' ";

															$result_para_o_user = mysqli_query($conn, $user_infs_comentarios);

															$user_comentarios = mysqli_fetch_array($result_para_o_user, MYSQLI_ASSOC);

															

											?> 
 

											<div class="comment-list" id="comentarios">
												<div class="single-comment justify-content-between d-flex">
													<div class="user justify-content-between d-flex">
														<div class="thumb">
														<a href="ver-perfil.php?id=<?php echo $user_comentarios['id'];?>&nome=<?php echo $user_comentarios['nome'];?>"><img style="width:60px; height:60px;" src="assets/fotos_user/<?php echo $user_comentarios['nome_imagem'];?>" alt=""></a>
														</div>
														<div class="desc">
															<h5><a href="ver-perfil.php?id=<?php echo $user_comentarios['id'];?>&nome=<?php echo $user_comentarios['nome'];?>"><?php echo $user_comentarios['nome'];?></a></h5>
															<p class="date"><?php echo $linha_comentarios['data_hora'];?></p>
															<p class="comment">
																<?php echo utf8_decode($linha_comentarios['comentario']);?>
															</p>
														</div>
													</div>
												</div>
											</div>
											
											<?php
													}

												}	
								 			?> 

										</div>
											
										</div>
									
									</div>
									
								</div>

								<?php
								
									if(isset($_SESSION['user_id'])) {

								?> 

							<div class="comment-form">
								<h4>Escreva o seu comentário </h4> <small>Os comentários são todos submetidos para revisão de admistradores! </small>
								
								<form method = "POST" action = "includes/processos/proc_comentar.php">
									<div class="form-group">
										<textarea style="resize:none;" class="form-control mb-10" rows="5" maxlength="180" name="comentario" placeholder="Comente aqui...(180 caracteres max.)" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Comente algo antes de submeter o comentário!'" required=""></textarea>
									</div>
									<button class="primary-btn text-uppercase" type="submit" name="comentar">Comentar </button>

									<input type="hidden" name="id_pub" value="<?php echo $id_pub;?>">
									<input type="hidden" name="user_id" value="<?php echo $user_id;?>">
									<input type="hidden" name="titulo_pub" value="<?php echo $titulo_pub;?>">
								</form>

							
							</div>

							<?php
								
									}else{

							?> 

							<div class="comment-form">
								<h4>Escreva o seu comentário </h4>  <small>Os comentários são todos submetidos para revisão de admistradores! </small>
								
									<div class="form-group">
										<textarea style="resize:none;" class="form-control mb-10" rows="5" maxlength="180" name="comentario" placeholder="Registe-se e faça login para poder comentar!" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Certifique-se que está registado e logado na sua conta para poder comentar!'" required=""></textarea>
									</div>
									<button class="primary-btn text-uppercase" type="submit" name="comentar">Comentar </button>

							
							</div>

							<?php
										
											}
							?> 
						</div>
						<!-- End single-post Area -->
					</div>
                    
                        <?php

                            include('includes/noticias/noticias_laterais.php');

                        ?>

				</div>
			</div>
		</section>
		<!-- End latest-post Area -->
	</div>
	
    <?php
        include('includes/footer.php');
		include('includes/botao_topo.php');
    ?>
    
	<!-- Quem deu like -->
	<div class="modal fade" id="pessoasquegostaram">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><span class="fa fa-heart" style="color:red;">&nbsp;</span><?php echo $linha['likes'] ;?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
		  <?php 
		  
				//Buscar os users que derao like na publicacao
				$likes2="SELECT * From likes WHERE id_publicacao = $id_pub";   
				$result_likes2 = mysqli_query($conn, $likes2);

				$nr_linhas_likes2= mysqli_num_rows($result_likes2);

				if($nr_linhas_likes2 == 0){

			?>
				Ainda ninguém deu like nesta publicação...deseja ser o primeiro?
			<?php

				}else{

				while($linha_likes2= mysqli_fetch_array($result_likes2, MYSQLI_ASSOC)){

				$id_user_na_linha_likes2 = $linha_likes2['id_utilizador'];


					$user_na_linha_likes2="SELECT * From utilizadores WHERE id = $id_user_na_linha_likes2";
					$result_user_na_linha_likes2 = mysqli_query($conn, $user_na_linha_likes2);
				
					$linha_user_na_linha_likes2 = mysqli_fetch_array($result_user_na_linha_likes2, MYSQLI_ASSOC);
					
					$id_user_na_linha_likes2 = $linha_user_na_linha_likes2['id'];
									
		 ?>

			<div class="thumb">
				<a href="ver-perfil.php?id=<?php echo $id_user_na_linha_likes2;?>&nome=<?php echo $linha_user_na_linha_likes2['nome'];?>"><img style="width:40px; height:40px;border-radius: 50%" src="assets/fotos_user/<?php echo $linha_user_na_linha_likes2['nome_imagem'];?>" alt=""></a>
				<a class="nomeutilizador" href ="ver-perfil.php?id=<?php echo $id_user_na_linha_likes2;?>&nome=<?php echo $linha_user_na_linha_likes2['nome'];?>">&nbsp;<?php echo $linha_user_na_linha_likes2['nome'];?></a>
			<hr>		
		 <?php 
		 }
				}
		 ?>
        </div>
        
      </div>
    </div>
  </div>
<!-- Fim do quem deu like -->

	<!--Likes-->

	
	<!-- End footer Area -->
	<script src="assets/js/vendor/jquery-2.2.4.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="assets/js/vendor/bootstrap.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="assets/js/easing.min.js"></script>
	<script src="assets/js/hoverIntent.js"></script>
	<script src="assets/js/superfish.min.js"></script>
	<script src="assets/js/jquery.ajaxchimp.min.js"></script>
	<script src="assets/js/jquery.magnific-popup.min.js"></script>
	<script src="assets/js/mn-accordion.js"></script>
	<script src="assets/js/jquery-ui.js"></script>
	<script src="assets/js/jquery.nice-select.min.js"></script>
	<script src="assets/js/owl.carousel.min.js"></script>
	<script src="assets/js/mail-script.js"></script>
    <script src="assets/js/main.js"></script>
    
    <?php
        }

    }
    ?> 
</body>
</html>